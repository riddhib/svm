module.exports.projectSchema = { 
                                projectId: {
                                    type: Number,
                                    unique: true,
                                    required: true,
                                    index: true
                                },
                                name: { 
                                    type: String,
                                    required: true,
                                    index: true
                                },
                                description: { 
                                    type: String
                                },
                                phone: { 
                                    type: Number
                                },
                                email: { 
                                    type: String
                                },
                                contactPerson: {
                                    type: String
                                },
                                details: {
                                    type: String
                                },
                                createdBy: { 
                                    type: Number
                                },
                                logo: { 
                                    type: String
                                },
                                categories: { 
                                    type: String
                                },
                                type: { 
                                    type: String
                                },
                                status: {
                                    type: String
                                },
                                resources: {
                                    type: [Number]
                                },
                                members: {
                                    type: [Number],
                                },
                                projectFolderId: {
                                    type: Number
                                },
                                following: [{
                                    userId: {type: Number},
                                    userName: {type: String}
                                }],
                                rating: [{
                                    rating: {type: Number},
                                    ratingId: {type: [Number]},
                                    userId: {type: [Number]}
                                }],
                                avgRating: {
                                    type: Number
                                },
                                totalRating: {
                                    type: Number
                                },
                                myRating: [{
                                    userId: {type: Number},
                                    rating: {type: Number}
                                }],
                                organisationId: { 
                                    type: Number
                                },
                                discussions:[],
                                requests: [],
                                type: {
                                    type: String
                                },
                                sector: {
                                    type: [String]
                                },
                                startDate: {
                                    type: Number
                                },
                                isActive: {
                                    type: Boolean
                                },
                                endDate: {
                                    type: Number
                                },
                                createdDate: {
                                    type: Number,
                                    index: true
                                },
                                updatedDate: {
                                    type: Number,
                                    index: true
                                }
};