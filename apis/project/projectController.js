var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var VALIDATE = utils.CONSTANTS.VALIDATE;
var validate = utils.validate;

var Project = function() {
    return {
        projectId: 0,
        name: null,
        description: null,
        phone: 0,
        email: null,
        contactPerson: null,
        details: null,
        createdBy: 0,
        logo: null,
        categories: null,
        type: null,
        status: null,
        resources: [],
        members: [],
        projectFolderId: 0,
        following: [],
        rating: [],
        discussions: [],
        avgRating: 0,
        totalRating: 0,
        myRating: [],
        organisationId: 0,
        type: null,
        sector: [],
        requests: [],
        startDate: 0,
        endDate: 0,
        isActive: true,
        createdDate: 0,
        updatedDate: 0
    }
};

function ProjectAPI(projectRecord) {
    var project = new Project();
    project.getProjectId = function() {
        return this.projectId;
    };
    project.setProjectId = function(projectId) {
        if (projectId) {
            if (validate.isInteger(projectId + '')) {
                this.projectId = projectId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, projectId, 'projectId')
                };
            }
        }
    };
    project.getName = function() {
        return this.name;
    };
    project.setName = function(name) {
         if (name) {
            if (name.length <= 250) {
                this.name = name;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, name, 'name')
                };
            }
        }
    };
    project.getDescription = function() {
        return this.description;
    };
    project.setDescription = function(description) {
        if (description) {
            this.description = description;
        }
    };
    project.getPhone = function() {
        return this.phone;
    };
    project.setPhone = function(phone) {
        if (phone) {
            if (validate.isInteger(phone + '')) {
                this.phone = phone;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, phone, 'phone')
                };
            }
        }
    };
    project.getEmail = function() {
        return this.email;
    };
    project.setEmail = function(email) {
        if (email) {
            this.email = email;
        }
    };
    project.getContactPerson = function() {
        return this.contactPerson;
    };
    project.setContactPerson = function(contactPerson) {
        if (contactPerson) {
            this.contactPerson = contactPerson;
        }
    };
    project.getDetails = function() {
        return this.details;
    };
    project.setDetails = function(details) {
        if (details) {
            this.details = details;
        }
    };
    project.getCreatedBy = function() {
        return this.createdBy;
    };
    project.setCreatedBy = function(createdBy) {
        if (createdBy) {
            if (validate.isInteger(createdBy + '')) {
                this.createdBy = createdBy;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdBy, 'createdBy')
                };
            }
        }
    };
    project.getLogo = function() {
        return this.logo;
    };
    project.setLogo = function(logo) {
        if (logo) {
            this.logo = logo;
        }
    };
    project.getCategories = function() {
        return this.categories;
    };
    project.setCategories = function(categories) {
        if (categories) {
            if (categories.length <= 500) {
                this.categories = categories;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, categories, 'categories')
                };
            }
        }
    };
    project.getType = function() {
        return this.type;
    };
    project.setType = function(type) {
        if (type) {
            if (type.length <= 500) {
                this.type = type;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, type, 'type')
                };
            }
        }
    };
    project.getStatus = function() {
        return this.status;
    };
    project.setStatus = function(status) {
        if (status) {
            if (status.length <= 50) {
                this.status = status;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, status, 'status')
                };
            }
        }
    };
    project.getResources = function() {
        return this.resources;
    };
    project.setResources = function(resources) {
        if (resources) {
            this.resources = resources;           
        }
    };
    project.getDiscussions = function() {
        return this.discussions;
    };
    project.setDiscussions = function(discussions) {
        if (discussions) {
            this.discussions = discussions;           
        }
    };
    project.getMembers = function() {
        return this.members;
    };
    project.setMembers = function(members) {
        if (members) {
            this.members = members;           
        }
    };
    project.getIsActive = function() {
        return this.isActive;
    };
    project.setIsActive = function(isActive) {
        this.isActive = isActive;
    };
    project.getProjectFolderId = function() {
        return this.projectFolderId;
    };
    project.setProjectFolderId = function(projectFolderId) {
        if (projectFolderId) {
            if (validate.isInteger(projectFolderId + '')) {
                this.projectFolderId = projectFolderId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, projectFolderId, 'projectFolderId')
                };
            }
        }
    };
    project.getFollowing = function() {
        return this.following;
    };
    project.setFollowing = function(following) {
        if (following) {
            this.following = following;           
        }
    };
    project.getRating = function() {
        return this.rating;
    };
    project.setRating = function(rating) {
        if (rating) {
            this.rating = rating;           
        }
    };
    project.getAvgRating = function() {
        return this.avgRating;
    };
    project.setAvgRating = function(avgRating) {
        if (avgRating) {
            this.avgRating = avgRating;           
        }
    };
    project.getTotalRating = function() {
        return this.totalRating;
    };
    project.setTotalRating = function(totalRating) {
        if (totalRating) {
            this.totalRating = totalRating;           
        }
    };
    project.getMyRating = function() {
        return this.myRating;
    };
    project.setMyRating = function(myRating) {
        if (myRating) {
            this.myRating = myRating;           
        }
    };
    project.getOrganisationId = function() {
        return this.organisationId;
    };
    project.setOrganisationId = function(organisationId) {
        if (organisationId) {
            this.organisationId = organisationId;
        }
    };
    project.getRequests = function() {
        return this.requests;
    };
    project.setRequests = function(requests) {
        if (requests) {
            this.requests = requests;
        }
    };
    project.getType = function() {
        return this.type;
    };
    project.setType = function(type) {
        if (type) {
            if (type.length <= 50) {
                this.type = type;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, type, 'type')
                };
            }
        }
    };
    project.getSector = function() {
        return this.sector;
    };
    project.setSector = function(sector) {
        if (sector) {
            if (sector.length <= 500) {
                this.sector = sector;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, sector, 'sector')
                };
            }
        }
    };
    project.getStartDate = function() {
        return this.startDate;
    };
    project.setStartDate = function(startDate) {
        if (startDate) {
            if (validate.isInteger(startDate + '')) {
                this.startDate = startDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, startDate, 'startDate')
                };
            }
        }
    };
    project.getEndDate = function() {
        return this.endDate;
    };
    project.setEndDate = function(endDate) {
        if (endDate) {
            if (validate.isInteger(endDate + '')) {
                this.endDate = endDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, endDate, 'endDate')
                };
            }
        }
    };
    project.getCreatedDate = function() {
        return this.createdDate;
    };
    project.setCreatedDate = function(createdDate) {
        if (createdDate) {
            if (validate.isInteger(createdDate + '')) {
                this.createdDate = createdDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdDate, 'createdDate')
                };
            }
        }
    };
    project.getUpdatedDate = function() {
        return this.updatedDate;
    };
    project.setUpdatedDate = function(updatedDate) {
        if (updatedDate) {
            if (validate.isInteger(updatedDate + '')) {
                this.updatedDate = updatedDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, updatedDate, 'updatedDate')
                };
            }
        }
    };

    if (projectRecord) {
        var errorList = [];
        try {
            project.setProjectId(projectRecord.projectId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setName(projectRecord.name);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setDescription(projectRecord.description);
        } catch (e) {
           errorList.push(e);
        }
        try {
            project.setPhone(projectRecord.phone);
        } catch (e) {
           errorList.push(e);
        }
        try {
            project.setEmail(projectRecord.email);
        } catch (e) {
           errorList.push(e);
        }
        try {
            project.setContactPerson(projectRecord.contactPerson);
        } catch (e) {
           errorList.push(e);
        }
        try {
            project.setDetails(projectRecord.details);
        } catch (e) {
           errorList.push(e);
        }
        try {
            project.setCreatedBy(projectRecord.createdBy);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setLogo(projectRecord.logo);
        } catch (e) {
             errorList.push(e);
        }
        try {
            project.setCategories(projectRecord.categories);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setType(projectRecord.type);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setStatus(projectRecord.status);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setResources(projectRecord.resources);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setMembers(projectRecord.members);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setProjectFolderId(projectRecord.projectFolderId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setDiscussions(projectRecord.discussions);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setFollowing(projectRecord.following);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setRating(projectRecord.rating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setAvgRating(projectRecord.avgRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setTotalRating(projectRecord.totalRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setMyRating(projectRecord.myRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setOrganisationId(projectRecord.organisationId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setType(projectRecord.type);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setSector(projectRecord.sector);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setStartDate(projectRecord.startDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setEndDate(projectRecord.endDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setCreatedDate(projectRecord.createdDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setUpdatedDate(projectRecord.updatedDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setIsActive(projectRecord.isActive);
        } catch (e) {
            errorList.push(e);
        }
        try {
            project.setRequests(projectRecord.requests);
        } catch (e) {
            errorList.push(e);
        }
        if (errorList.length) {
            throw {
                status: REQUEST_CODES.FAIL,
                error: errorList
            };
        }
    }
    return project;
}

module.exports.ProjectAPI = ProjectAPI;