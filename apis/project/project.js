module.exports = function(app){
	app.post('/ui/project',function(req, res){
        try{
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            create(req.body, function(response){
               res.json(response);
        	});
         
        }catch(e){
            res.json(e);
        }
    });
	app.put('/ui/project', function(req, res) {
		try {
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			update(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
    app.get('/ui/project/:projectId', function(req, res) {
		try {
			getDetails(req.params.projectId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/getProject/:projectId', function(req, res) {
		try {
			getProject(req.params.projectId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/query/project', function(req, res) {
		try {
			getList(req.query, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.post('/ui/project/uploads/:type',function(req, res) {
        try {
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            upload(req.body, function(response) {
        	    res.json(response);
            });
        } catch(e){
            res.json(e);
        }
    });
    app.put('/ui/project/requests',function(req, res) {
        try {
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            requests(req.body, function(response) {
        	    res.json(response);
            });
        } catch(e){
            res.json(e);
        }
    });
    app.put('/ui/project/requestsResponse',function(req, res) {
        try {
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            requestsResponse(req.body, function(response) {
        	    res.json(response);
            });
        } catch(e){
            res.json(e);
        }
    });
    app.put('/ui/project/deleteRequest',function(req, res) {
        try {
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            deleteRequest(req.body, function(response) {
        	    res.json(response);
            });
        } catch(e){
            res.json(e);
        }
    });
}

var mongoose = require('mongoose')
	Schema = mongoose.Schema;
var ProjectSchema = new Schema(require('./projectSchema').projectSchema, {collection: 'project'});
var ProjectModel = mongoose.model('project', ProjectSchema);
var ProjectController = require('./projectController');
var Discussion = require('../discussions/discussion');
var UserLog = require('../userLog/userLog');
var utils = require('../../assets/utils').utils;
var MailHelper = require('../mailHelper/mailHelper');
var File = require('../file/file');
var Research = require('../research/research');
var ProjectFolder = require('../projectFolder/projectFolder');
var mongoUtils = utils.mongoUtils;
var CONSTANTS = utils.CONSTANTS;
var VALIDATE = CONSTANTS.VALIDATE;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var PROJECTS = CONSTANTS.PROJECTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var _ = require('lodash');

function create(project, callback) {
	var projectAPI = ProjectController.ProjectAPI(project);
    var errorList = [];
    if (!projectAPI.getName()) {
       	var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'name')
		};
		errorList.push(e);
	}	
   	if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var projectModel = new ProjectModel(projectAPI);
		projectModel.isActive = true;
		mongoUtils.getNextSequence('projectId', function(oSeq) {
			projectModel.projectId = oSeq;
			projectModel.createdDate = new Date().getTime();
			var newDiscussion = {
				"discussionName": projectModel.name,
				"body": projectModel.description || "",
				"category": "Projects",
				"parentId": projectModel.solutionId,
				"userId": project.userId,
				"userName": project.userName,
				"userEmail": project.userEmail,
				"url": project.url,
				"method": project.method,
				"apiRequest": {
					"module": "Discussions-Create",
					"name": project.name,
					"id": projectModel.projectId
				},
				"apiResponse": "SUCCESS"
			};
			Discussion.create(newDiscussion, function(response){
				if(response.error){
					callback({
						status: REQUEST_CODES.ERROR,
						error: "Failed to create Project"
					});
				} else {
					var discussionId = response.result[0].discussionId;
					projectModel.discussions.push(discussionId);
					projectModel.save(function(error) {
						if (error) {
							callback({
								status: DB_CODES.FAIL,
								error: error
							});
							return;
						} else {
							let logObj = {
								"userId": project.userId,
								"userName": project.userName,
								"email": project.userEmail,
								"requestUrl": project.url,
								"requestMethod": project.method,
								"apiRequest": {
									"module": "Research-Create",
									"name": projectModel.name,
									"id": projectModel.projectId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: utils.formatText(PROJECTS.CREATE_SUCCESS, projectModel.projectId)
									});
									return;
								}
							});				
						}
					});
				}
			});
		});
	}
}

function getDetails(projectId, callback) {
	ProjectModel.find({"projectId": projectId}, function(error, projectRecords) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			projectRecords = projectRecords.map(function(projectRecord) {
				return new ProjectController.ProjectAPI(projectRecord);
			});			
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: projectRecords
			});
			return;		
		}
	});	
}

function getList(query, callback) {
	ProjectModel.find(query, {details: 0, requests: 0}, function(error, projectRecords) {		
		if (error) {
			callback({
			    status: DB_CODES.FAIL,
			    error: error
			});
			return;
		} else {		
			projectRecords = projectRecords.map(function(projectRecord) {
				return new ProjectController.ProjectAPI(projectRecord);
			});
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: projectRecords
			});
			return;	
		}
	}).sort({createdDate: -1});
}

function getProjects(query, projection, callback) {
	ProjectModel.find(query, projection, function(error, projectRecords) {		
		if (error) {
			callback({
			    status: DB_CODES.FAIL,
			    error: error
			});
			return;
		} else {
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: projectRecords
			});
			return;	
		}
	});
}

function update(project, callback) {
	getDetails(project.projectId, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result) {
				var projectDetails = response.result[0];
				project.updatedDate = new Date().getTime();
				ProjectModel.updateOne({"projectId": project.projectId}, {$set: project}, function(error, effectedRows) {
					if (error) {
						callback({
							status: DB_CODES.FAIL,
							error: error
						});
						return;
					} else {
						if (!effectedRows.nModified) {
							callback({
								status: REQUEST_CODES.FAIL,
								error: utils.formatText(PROJECTS.UPDATE_FAIL, project.projectId)
							});
							return;
						} else {
							let logObj = {
								"userId": project.userId,
								"userName": project.userName,
								"email": project.userEmail,
								"requestUrl": project.url,
								"requestMethod": project.method,
								"apiRequest": {
									"module": "Research-Create",
									"name": projectDetails.name,
									"id": projectDetails.projectId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: utils.formatText(PROJECTS.UPDATE_SUCCESS, project.projectId)
									});
									return;
								}
							});
						}
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No Project Found"
				});
				return;
			}
		}
	});	 
}

function upload(file, callback) {
	File.create({
		fileName: file.advanceDetails.fileName,
		description: file.advanceDetails.description,
		uploadBy: file.advanceDetails.uploadBy,
		originalName: file.file.originalname,
		filePath: '/uploads/'+file.advanceDetails.category+'/'+file.file.filename,
		userId: file.advanceDetails.userId,
		category: file.advanceDetails.category,
		mimeType: file.file.mimetype
	}, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var fileResponse = response.file;
			getDetails(file.advanceDetails.projectId, function(response) {
				if(response.error) {
					callback(response);
				} else {
					var projectRecord = response.result[0];
					var resources = projectRecord.resources;
					resources.push(fileResponse.fileId);
					var project = {
						"projectId": file.advanceDetails.projectId,
						"resources": resources,
						"userId": file.userId,
						"userName": file.userName,
						"userEmail": file.userEmail,
						"url": file.url,
						"method": file.method,
						"apiRequest": {
							"module": "Project-Update",
							"name": projectRecord.name,
							"id": file.advanceDetails.projectId
						},
						"apiResponse": "SUCCESS"
					}
					update(project, function(response){
						if(response.error){
							callback(response);
						} else {
							file.category = _.capitalize(file.category);
							if(file.advanceDetails.category == 'academicResearch'){
								var researchDetails = {
									"researchName": file.advanceDetails.fileName,
									"description": file.advanceDetails.description,
									"sector": projectRecord.sector,
									"type": projectRecord.type,
									"researchUrl": file.advanceDetails.url,
									"keyword": file.advanceDetails.keyWords,
									"author": file.advanceDetails.author,
									"userId": file.userId,
									"userName": file.userName,
									"email": file.userEmail,
									"requestUrl": file.url,
									"requestMethod": file.method,
									"apiRequest": {
										"module": "Research-Create",
										"name": projectRecord.name,
										"id": projectRecord.projectId
									},
									"apiResponse": "SUCCESS",
									"fileDetails": {
										"fileId": fileResponse.fileId,
										"fileName": file.advanceDetails.fileName,
										"description": file.advanceDetails.description,
										"uploadBy": file.advanceDetails.uploadBy,
										"originalName": file.file.originalname,
										"filePath": '/uploads/'+file.advanceDetails.category+'/'+file.file.filename,
										"userId": file.advanceDetails.userId,
										"category": "research",
										"mimeType": file.file.mimetype
									}
								};
								Research.createResearchFromProject(researchDetails, function(response){
									if(response.error){
										callback({
											status: REQUEST_CODES.ERROR,
											error: response.error
										});
									} else {
										callback({
											status: REQUEST_CODES.SUCCESS,
											result: "Project "+file.category+" uploaded" 
										});
										return;
									}
								});
							} else {
								callback({
									status: REQUEST_CODES.SUCCESS,
									result: "Project "+file.category+" uploaded" 
								});
								return;
							}
						}
					});
				}
			});
		}
	});
}

async function requests(request, callback){
	getDetails(request.projectId, async function(response) {
		if(response.error) {
			callback(response);
		} else {
			var projectRecord = response.result[0];
			var requestsObject = await new Promise(resolve=> {
				mongoUtils.getNextSequence('requestsId', function(oSeq) {
					let obj = {
						"requestId": oSeq,
						"requestedBy": {
							"userId": request.userId,
							"userName": request.userName
						},
						"message": request.message,
						"responses": [],
						"createdDate": new Date().getTime()
					}
					resolve(obj);
				});
			});
			projectRecord.requests.push(requestsObject);
			var project = {
				"projectId": request.projectId,
				"requests": projectRecord.requests,
				"userId": request.userId,
				"userName": request.userName,
				"userEmail": request.userEmail,
				"url": request.url,
				"method": request.method,
				"apiRequest": {
					"module": "Project-Update",
					"name": projectRecord.name,
					"id": request.projectId
				},
				"apiResponse": "SUCCESS"
			}
			update(project, function(response){
				if(response.error){
					callback(response);
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: "Project request submitted succesfully" 
					});
					return;
				}
			});
		}
	});
}

function deleteRequest(request, callback){
	getDetails(request.projectId, async function(response) {
		if(response.error) {
			callback(response);
		} else {
			var projectRecord = response.result[0];
			let requests = projectRecord.requests.filter(req=> req.requestId != request.requestId);
			var project = {
				"projectId": request.projectId,
				"requests": requests,
				"userId": request.userId,
				"userName": request.userName,
				"userEmail": request.userEmail,
				"url": request.url,
				"method": request.method,
				"apiRequest": {
					"module": "Project-Update",
					"name": projectRecord.name,
					"id": request.projectId
				},
				"apiResponse": "SUCCESS"
			}
			update(project, function(response){
				if(response.error){
					callback(response);
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: "Project request deleted succesfully" 
					});
					return;
				}
			});
		}
	});
}

async function requestsResponse(request, callback){
	if(request.advanceDetails || request.file){
		File.create({
			fileName: request.advanceDetails.fileName,
			description: '',
			uploadBy: request.advanceDetails.uploadBy,
			originalName: request.file.originalname,
			filePath: '/uploads/'+request.advanceDetails.category+'/'+request.file.filename,
			userId: request.advanceDetails.userId,
			category: request.advanceDetails.category,
			mimeType: request.file.mimetype
		}, async function(response) {
			if(response.error) {
				callback(response);
			} else {
				var fileResponse = response.file;
				getDetails(request.advanceDetails.projectId, async function(response) {
					if(response.error) {
						callback(response);
					} else {
						var projectRecord = response.result[0];
						let requests = [];
						let resp = projectRecord.requests.map(req=> {
							return new Promise(resolve=>{
								if(req.requestId == request.advanceDetails.requestId){
									req.responses.push({
										"userId": request.userId,
										"userName": request.userName,
										"message": request.advanceDetails.message,
										"file": {
											"fileId": fileResponse.fileId,
											"fileName": fileResponse.fileName,
											"filePath": fileResponse.filePath
										},
										"createdDate": new Date().getTime()
									});
								}
								requests.push(req);
								resolve();
							});
						});
						await Promise.all(resp).then(result => result);
						var project = {
							"projectId": request.advanceDetails.projectId,
							"requests": requests,
							"userId": request.userId,
							"userName": request.userName,
							"userEmail": request.userEmail,
							"url": request.url,
							"method": request.method,
							"apiRequest": {
								"module": "Project-Update",
								"name": projectRecord.name,
								"id": request.advanceDetails.projectId
							},
							"apiResponse": "SUCCESS"
						}
						update(project, function(response){
							if(response.error){
								callback(response);
							} else {
								callback({
									status: REQUEST_CODES.SUCCESS,
									result: "Project response submitted succesfully" 
								});
								return;
							}
						});
					}
				});
			}
		});
	} else {
		getDetails(request.projectId, async function(response) {
			if(response.error) {
				callback(response);
			} else {
				var projectRecord = response.result[0];
				let requests = [];
				let resp = projectRecord.requests.map(req=> {
					return new Promise(resolve=>{
						if(req.requestId == request.requestId){
							req.responses.push({
								"userId": request.userId,
								"userName": request.userName,
								"message": request.message,
								"createdDate": new Date().getTime()
							});
						}
						requests.push(req);
						resolve();
					});
				});
				await Promise.all(resp).then(result => result);
				var project = {
					"projectId": request.projectId,
					"requests": requests,
					"userId": request.userId,
					"userName": request.userName,
					"userEmail": request.userEmail,
					"url": request.url,
					"method": request.method,
					"apiRequest": {
						"module": "Project-Update",
						"name": projectRecord.name,
						"id": request.projectId
					},
					"apiResponse": "SUCCESS"
				}
				update(project, function(response){
					if(response.error){
						callback(response);
					} else {
						callback({
							status: REQUEST_CODES.SUCCESS,
							result: "Project response submitted succesfully" 
						});
						return;
					}
				});
			}
		});
	}
}

function getProject(projectId, callback) {
	getDetails(projectId, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var projectRecord = response.result[0];
			ProjectFolder.getList({projectFolderId: projectRecord.projectFolderId}, function(response) {
				if(response.error) {
					callback(response);
				} else {
					var projectFolder = response.result[0];
					projectRecord.people = projectFolder.users;
					File.getList({fileId: {$in: projectRecord.resources}}, function(response) {
						if(response.error) {
							callback(response);
						} else {
							var fileRecords = response.result;
							callback({
								status: REQUEST_CODES.SUCCESS,
								result: {project: projectRecord, files: fileRecords}
							});
						}
					});
				}
			});
		}
	});
}

module.exports.create = create;
module.exports.getDetails = getDetails;
module.exports.getList = getList;
module.exports.update = update;
module.exports.getProjects = getProjects;
