module.exports = function(app){
	app.post('/ui/collaborate',function(req, res){
        try{
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            create(req.body, function(response){
               res.json(response);
        	});
        } catch(e){
            res.json(e);
        }
    });
	app.put('/ui/collaborate', function(req, res) {
		try {
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			update(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
    app.get('/ui/collaborate/:collaborateId', function(req, res) {
		try {
			getDetails(req.params.collaborateId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/query/collaborate', function(req, res) {
		try {
			getList(req.query, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});  
}

var mongoose = require('mongoose')
	Schema = mongoose.Schema;
var CollaborateSchema = new Schema(require('./collaborateSchema').collaborateSchema, {collection: 'collaborate'});
var CollaborateModel = mongoose.model('collaborate', CollaborateSchema);
var CollaborateController = require('./collaborateController');
var UserLog = require('../userLog/userLog');
var utils = require('../../assets/utils').utils;
var mongoUtils = utils.mongoUtils;
var CONSTANTS = utils.CONSTANTS;
var VALIDATE = CONSTANTS.VALIDATE;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var COLLABORATE_CODES = CONSTANTS.COLLABORATE_CODES;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;

function create(collaborate, callback) {
	var collaborateAPI = CollaborateController.CollaborateAPI(collaborate);
    var errorList = [];
    if (!collaborateAPI.getName()) {
       	var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'name')
		};
		errorList.push(e);
	}	
   	if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var collaborateModel = new CollaborateModel(collaborateAPI);
		collaborateModel.isActive = true;
		mongoUtils.getNextSequence('collaborateId', function(oSeq) {
			collaborateModel.collaborateId = oSeq;
			collaborateModel.createdDate = new Date().getTime();
			collaborateModel.save(function(error) {
				if (error) {
					callback({
						status: DB_CODES.FAIL,
						error: error
					});
					return;
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: utils.formatText(COLLABORATE_CODES.CREATE_SUCCESS, collaborateModel.collaborateId)
					});
					return;							
				}
			});
		});
	}
}

function getDetails(collaborateId, callback) {
	CollaborateModel.find({"collaborateId": collaborateId}, function(error, collaborateRecords) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			collaborateRecords = collaborateRecords.map(function(collaborateRecord) {
				return new CollaborateController.CollaborateAPI(collaborateRecord);
			});			
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: collaborateRecords
			});
			return;		
		}
	});	
}

function getList(query, callback) {
	CollaborateModel.find(query, function(error, collaborateRecords) {	
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			collaborateRecords = collaborateRecords.map(function(collaborateRecord) {
				return new CollaborateController.CollaborateAPI(collaborateRecord);
			});
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: collaborateRecords
			});
			return;		
		}
	}).sort({createdDate: -1});
}

function update(collaborate, callback) {
	getDetails(collaborate.collaborateId, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result) {
				collaborate.updatedDate = new Date().getTime();
				CollaborateModel.updateOne({"collaborateId": collaborate.collaborateId}, {$set: collaborate}, function(error, effectedRows) {
					if (error) {
						callback({
							status: DB_CODES.FAIL,
							error: error
						});
						return;
					} else {
						if (!effectedRows.nModified) {
							callback({
								status: REQUEST_CODES.FAIL,
								error: utils.formatText(COLLABORATE_CODES.UPDATE_FAIL, collaborate.collaborateId)
							});
							return;
						} else {
							let logObj = {
								"userId": collaborate.userId,
								"userName": collaborate.userName,
								"email": collaborate.userEmail,
								"requestUrl": collaborate.url,
								"requestMethod": collaborate.method,
								"apiRequest": {
									"module": "Collaborate-Update",
									"name": '',
									"id": collaborate.collaborateId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: utils.formatText(COLLABORATE_CODES.UPDATE_SUCCESS, collaborate.collaborateId)
									});
									return;
								}
							});
						}
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No Collaborate Found"
				});
				return;
			}
		}
	});	 
}

module.exports.create = create;
module.exports.getDetails = getDetails;
module.exports.getList = getList;
module.exports.update = update;
