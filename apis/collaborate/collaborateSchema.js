module.exports.collaborateSchema = { 
                                collaborateId: {
                                    type: Number,
                                    unique: true,
                                    required: true,
                                    index: true
                                },
                                name: { 
                                    type: String,
                                    required: true
                                },
                                about: {
                                    type: String
                                },
                                type: { 
                                    type: String
                                },
                                location: {
                                    type: String
                                },
                                sector: {
                                    type: []
                                },
                                keyword: {
                                    type: [String]
                                },
                                following: [{
                                    userId: {type: Number},
                                    userName: {type: String}
                                }],
                                rating: [{
                                    rating: {type: Number},
                                    ratingId: {type: [Number]},
                                    userId: {type: [Number]}
                                }],
                                avgRating: {
                                    type: Number
                                },
                                totalRating: {
                                    type: Number
                                },
                                myRating: [{
                                    userId: {type: Number},
                                    rating: {type: Number}
                                }],
                                logo: {
                                    type: String
                                },
                                website: {
                                    type: String
                                },
                                description: {
                                    type: String
                                },
                                interests: {
                                    type: String
                                },
                                previousSV: {
                                    type: String
                                },
                                projects: {
                                    type: Number
                                },
                                resources: {
                                    type: Number
                                },
                                valueProposition: {
                                    type: String
                                }, 
                                annualTurnover: {
                                    type: String
                                },
                                fundingRaised: {
                                    type: String
                                },
                                tags: {
                                    type: String
                                },
                                poc: {
                                    type: String
                                }, 
                                email: {
                                    type: String
                                },   
                                contact: {
                                    type: Number
                                },
                                linkedin: {
                                    type: String
                                },
                                isActive : {
                                    type: Boolean
                                },
                                createdDate: { 
                                    type: Number,
                                    index: true
                                },
                                updatedDate: { 
                                    type: Number,
                                    index: true
                                }

};