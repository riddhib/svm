var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var VALIDATE = utils.CONSTANTS.VALIDATE;
var validate = utils.validate;

var Collaborate = function() {
    return {
        collaborateId: 0,
        name: null,
        about: null,
        type: null,
        location: null,
        sector: [],
        keyword: [],
        following: [],
        rating: [],
        avgRating: 0,
        totalRating: 0,
        myRating: [],
        logo: null,
        website: null,
        description: null,
        interests: null,
        previousSV: null,
        projects: 0,
        resources: 0,
        valueProposition: null, 
        annualTurnover: 0,
        fundingRaised: null,
        poc: null,
        tags: null,
        email: null,   
        contact: 0,
        linkedin: null,
        isActive: true,
        createdDate: 0,
        updatedDate: 0
    };
};

function CollaborateAPI(collaborateRecord) {
    var collaborate = new Collaborate();
    collaborate.getCollaborateId = function() {
        return this.collaborateId;
    };
    collaborate.setCollaborateId = function(collaborateId) {
        if (collaborateId) {
            if (validate.isInteger(collaborateId + '')) {
                    this.collaborateId = collaborateId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, collaborateId, 'collaborateId')
                };
            }
        }
    };
    collaborate.getName = function() {
        return this.name;
    };
    collaborate.setName = function(name) {
         if (name) {
            if (name.length <= 250) {
                this.name = name;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, name, 'name')
                };
            }
        }
    };
    collaborate.getAbout = function() {
        return this.about;
    };
    collaborate.setAbout = function(about) {
         if (about) {
            this.about = about;
        }
    };
    collaborate.getType = function() {
        return this.type;
    };
    collaborate.setType = function(type) {
        if (type) {
            this.type = type;
        }
    };
    collaborate.getIsActive = function() {
        return this.isActive;
    };
    collaborate.setIsActive = function(isActive) {
        this.isActive = isActive;
    };
    collaborate.getLocation = function() {
        return this.location;
    };
    collaborate.setLocation = function(location) {
        if (location) {
            this.location = location;
        }
    };
    collaborate.getSector = function() {
        return this.sector;
    };
    collaborate.setSector = function(sector) {
        if (sector) {
            this.sector = sector;
        }
    };
    collaborate.getKeyword = function() {
        return this.keyword;
    };
    collaborate.setKeyword = function(keyword) {
        if (keyword) {
            this.keyword = keyword;
        }
    };
    collaborate.getFollowing = function() {
        return this.following;
    };
    collaborate.setFollowing = function(following) {
        if (following) {
            this.following = following;
        }
    };
    collaborate.getRating = function() {
        return this.rating;
    };
    collaborate.setRating = function(rating) {
        if (rating) {
            this.rating = rating;
        }
    };
    collaborate.getAvgRating = function() {
        return this.avgRating;
    };
    collaborate.setAvgRating = function(avgRating) {
        if (avgRating) {
            this.avgRating = avgRating;
        }
    };
    collaborate.getTotalRating = function() {
        return this.totalRating;
    };
    collaborate.setTotalRating = function(totalRating) {
        if (totalRating) {
            this.totalRating = totalRating;
        }
    };
    collaborate.getMyRating = function() {
        return this.myRating;
    };
    collaborate.setMyRating = function(myRating) {
        if (myRating) {
            this.myRating = myRating;
        }
    };
    collaborate.getLogo = function() {
        return this.logo;
    };
    collaborate.setLogo = function(logo) {
        if (logo) {
            this.logo = logo;
        }
    };
    collaborate.getWebsite = function() {
        return this.website;
    };
    collaborate.setWebsite = function(website) {
        if (website) {
            this.website = website;
        }
    };
    collaborate.getDescription = function() {
        return this.description;
    };
    collaborate.setDescription = function(description) {
        if (description) {
            this.description = description;
        }
    };
    collaborate.getInterests = function() {
        return this.interests;
    };
    collaborate.setInterests = function(interests) {
        if (interests) {
            this.interests = interests;
        }
    };
    collaborate.getPreviousSV = function() {
        return this.previousSV;
    };
    collaborate.setPreviousSV = function(previousSV) {
        if (previousSV) {
            this.previousSV = previousSV;
        }
    };
    collaborate.getProjects = function() {
        return this.projects;
    };
    collaborate.setProjects = function(projects) {
        if (projects) {
            if (validate.isInteger(projects + '')) {
                    this.projects = projects;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, projects, 'projects')
                };
            }
        }
    };
    collaborate.getResources = function() {
        return this.resources;
    };
    collaborate.setResources = function(resources) {
        if (resources) {
            if (validate.isInteger(resources + '')) {
                    this.resources = resources;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, resources, 'resources')
                };
            }
        }
    };
    collaborate.getValueProposition = function() {
        return this.valueProposition;
    };
    collaborate.setValueProposition = function(valueProposition) {
        if (valueProposition) {
            this.valueProposition = valueProposition;
        }
    };
    collaborate.getAnnualTurnover = function() {
        return this.annualTurnover;
    };
    collaborate.setAnnualTurnover = function(annualTurnover) {
        if (annualTurnover) {
            if (validate.isInteger(annualTurnover + '')) {
                    this.annualTurnover = annualTurnover;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, annualTurnover, 'annualTurnover')
                };
            }
        }
    };
    collaborate.getFundingRaised = function() {
        return this.fundingRaised;
    };
    collaborate.setFundingRaised = function(fundingRaised) {
        if (fundingRaised) {
            this.fundingRaised = fundingRaised;
        }
    };
    collaborate.getTags = function() {
        return this.tags;
    };
    collaborate.setTags = function(tags) {
        if (tags) {
            this.tags = tags;
        }
    };
    collaborate.getPoc = function() {
        return this.poc;
    };
    collaborate.setPoc = function(poc) {
        if (poc) {
            this.poc = poc;
        }
    };
    collaborate.getEmail = function() {
        return this.email;
    };
    collaborate.setEmail= function(email) {
        if (email) {
            if (utils.validate.isEmail(email)) {
                this.email = email;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_AN_EMAIL, email, 'email')
                };
            }
        }
    };
    collaborate.getContact = function() {
        return this.contact;
    };
    collaborate.setContact = function(contact) {
        if (contact) {
            if (validate.isInteger(contact + '')) {
                    this.contact = contact;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, contact, 'contact')
                };
            }
        }
    };
    collaborate.getLinkedin = function() {
        return this.linkedin;
    };
    collaborate.setLinkedin = function(linkedin) {
        if (linkedin) {
            this.linkedin = linkedin;
        }
    };
    collaborate.getCreatedDate = function() {
        return this.createdDate;
    };
    collaborate.setCreatedDate = function(createdDate) {
        if (createdDate) {
            if (validate.isInteger(createdDate + '')) {
                    this.createdDate = createdDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdDate, 'createdDate')
                };
            }
        }
    };
    collaborate.getUpdatedDate = function() {
        return this.updatedDate;
    };
    collaborate.setUpdatedDate = function(updatedDate) {
        if (updatedDate) {
            if (validate.isInteger(updatedDate + '')) {
                    this.updatedDate = updatedDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, updatedDate, 'updatedDate')
                };
            }
        }
    };
   
    if (collaborateRecord) {
        var errorList = [];
        try {
            collaborate.setCollaborateId(collaborateRecord.collaborateId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setName(collaborateRecord.name);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setAbout(collaborateRecord.about);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setType(collaborateRecord.type);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setLocation(collaborateRecord.location);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setSector(collaborateRecord.sector);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setKeyword(collaborateRecord.keyword);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setFollowing(collaborateRecord.following);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setRating(collaborateRecord.rating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setAvgRating(collaborateRecord.avgRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setTotalRating(collaborateRecord.totalRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setMyRating(collaborateRecord.myRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setLogo(collaborateRecord.logo);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setWebsite(collaborateRecord.website);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setDescription(collaborateRecord.description);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setInterests(collaborateRecord.interests);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setPreviousSV(collaborateRecord.previousSV);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setProjects(collaborateRecord.projects);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setResources(collaborateRecord.resources);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setValueProposition(collaborateRecord.valueProposition);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setPoc(collaborateRecord.poc);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setAnnualTurnover(collaborateRecord.annualTurnover);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setFundingRaised(collaborateRecord.fundingRaised);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setTags(collaborateRecord.tags);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setEmail(collaborateRecord.email);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setContact(collaborateRecord.contact);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setLinkedin(collaborateRecord.linkedin);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setCreatedDate(collaborateRecord.createdDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setUpdatedDate(collaborateRecord.updatedDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            collaborate.setIsActive(collaborateRecord.isActive);
        } catch (e) {
            errorList.push(e);
        }
        if (errorList.length) {
            throw {
                status: REQUEST_CODES.FAIL,
                error: errorList
            };
        }
    }
    return collaborate;
}

module.exports.CollaborateAPI = CollaborateAPI;