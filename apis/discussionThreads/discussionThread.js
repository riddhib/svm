module.exports = function(app){
	app.post('/ui/discussionThread',function(req, res){
        try{
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            create(req.body, function(response){
               res.json(response);
        	});
         
        }catch(e){
            res.json(e);
        }
    });

    app.get('/ui/discussionThread/:discussionThreadId', function(req, res) {
		try {
			getTimeLine(req.params.discussionThreadId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});

	app.put('/ui/replyToDiscussion',function(req, res){
        try{
            createThreadReplies(req.body, function(response){
               res.json(response);
        	});
         
        }catch(e){
            res.json(e);
        }
    });

    app.put('/ui/voteComments',function(req, res){
        try{
            voteComments(req.body, function(response){
               res.json(response);
        	});
         
        }catch(e){
            res.json(e);
        }
    });

    app.put('/ui/updateMessages',function(req, res){
        try{
            updateMessages(req.body, function(response){
               res.json(response);
        	});
         
        }catch(e){
            res.json(e);
        }
    });
	    
}

var mongoose = require('mongoose')
	Schema = mongoose.Schema;
var DiscussionThreadSchema = new Schema(require('./discussionThreadSchema').discussionThreadSchema, {collection: 'discussionThread'});
var DiscussionThreadModel = mongoose.model('discussionThread', DiscussionThreadSchema);
var DiscussionThreadController = require('./discussionThreadController');
var User = require('../user/user');
var Discussion = require('../discussions/discussion');
var utils = require('../../assets/utils').utils;
var mongoUtils = utils.mongoUtils;
var CONSTANTS = utils.CONSTANTS;
var VALIDATE = CONSTANTS.VALIDATE;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var DISCUSSION_CODES = CONSTANTS.DISCUSSION_CODES;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;

function create(discussionThread, callback) {	
	if(discussionThread.discussionThreadId) {
		getList({"discussionThreadId": discussionThread.discussionThreadId}, function(response){
			if(response.error){
				callback({
					status: REQUEST_CODES.ERROR,
					error: response.error
				});
			} else {
				var discussionThreadResponse = response.result[0];
				var messageId = 0;
				mongoUtils.getNextSequence('threadMessageId', function(oSeq) {
					messageId = oSeq;
				});
				User.getDetails(discussionThread.createdBy, function(response){
					if(response.error){
						callback({
							status: REQUEST_CODES.ERROR,
							error: response.error
						});
					} else if(response.result.length>0){
						var user = response.result[0];
						var threadMessage = {
							"messageId" : messageId,
							"title": discussionThread.title,
							"message": discussionThread.message,
							"postedBy": {
								"userId": discussionThread.createdBy,
								"userName": user.firstName+' '+user.lastName
							},
							"upvotes": [],
							"downvotes": [],
							"replies": [],
							"createdDate": new Date().getTime()
						};
						discussionThreadResponse.message.push(threadMessage);
						var isParticipant = discussionThreadResponse.participantIds.filter(participant=>participant.userId == discussionThread.createdBy)
						if(isParticipant.length < 1){
							discussionThreadResponse.participantIds.push({"userId": discussionThread.createdBy,"userName": user.firstName+' '+user.lastName, "logo": user.logo, "isOnline": true});
						}
						update(discussionThreadResponse, function(response){
							if(!response.error){
								callback({
									status: REQUEST_CODES.SUCCESS,
									result: [discussionThreadResponse]
								});
								return;	
							} else {
								callback({
									status: REQUEST_CODES.ERROR,
									result: response.error
								});
								return;	
							}
						});	
					}
				});
			}
		});
	} else {
		var discussionThreadAPI = DiscussionThreadController.DiscussionThreadAPI(discussionThread);
	    var errorList = [];
	    if (!discussionThreadAPI.getDiscussionId()) {
	       	var e = {
				status: VALIDATE.FAIL,
				error: utils.formatText(VALIDATE.REQUIRED, 'discussionId')
			};
			errorList.push(e);
		}
	    
	   	if (errorList.length) {
			throw {
			    status: REQUEST_CODES.FAIL,
			    error: errorList
			};
		}  else {
			User.getDetails(discussionThread.createdBy, function(response){
				if(response.error){
					callback({
						status: REQUEST_CODES.ERROR,
						error: response.error
					});
				} else if(response.result.length>0){
					var messageId = 0;
					mongoUtils.getNextSequence('threadMessageId', function(oSeq) {
						messageId = oSeq;
					});
					var user = response.result[0];
					var discussionThreadModel = new DiscussionThreadModel(discussionThreadAPI);
					mongoUtils.getNextSequence('discussionThreadId', function(oSeq) {
						discussionThreadModel.discussionThreadId = oSeq;
						discussionThreadModel.message = [];
						var threadMessage = {
							"messageId" : messageId,
							"title": discussionThread.title,
							"postedBy": {
								"userId": discussionThread.createdBy,
								"userName": user.firstName + ' ' +user.lastName
							},
							"message": discussionThread.message,
							"upvotes": [],
							"downvotes": [],
							"replies": [],
							"createdDate": new Date().getTime()
						};
						discussionThreadModel.message.push(threadMessage); 
						discussionThreadModel.participantIds.push({"userId": discussionThread.createdBy,"userName": user.firstName+' '+user.lastName, "logo": user.logo, "isOnline": true});
						discussionThreadModel.createdDate = new Date().getTime();
						discussionThreadModel.save(function(error) {
							if (error) {
								callback({
									status: DB_CODES.FAIL,
									error: error
								});
								return;
							} else {
								let logObj = {
									"discussionId": discussionThread.discussionId, 
									"discussionThreadId": discussionThreadModel.discussionThreadId, 
									"participantIds": discussionThreadModel.participantIds,
									"userId": discussionThread.userId,
									"userName": discussionThread.userName,
									"userEmail": discussionThread.userEmail,
									"url": discussionThread.url,
									"method": discussionThread.method,
									"apiRequest": {
										"module": "Discussion-Update",
										"name": '',
										"id": discussionThread.discussionId
									},
									"apiResponse": "SUCCESS"
								}
								Discussion.update(logObj, function(response){
									if(!response.error){
										callback({
											status: REQUEST_CODES.SUCCESS,
											result: [discussionThreadModel]
										});
										return;	
									} else {
										callback({
											status: REQUEST_CODES.ERROR,
											result: response.error
										});
										return;	
									}
								});						
							}
						});
					});
				}
			});
		}
	}
		
}

function createThreadReplies(discussionThreadMessage, callback){
	getDetails(discussionThreadMessage.discussionThreadId, function(response){
		if(response.error){
			callback({
				status: REQUEST_CODES.ERROR,
				error: response.error
			});
			return;
		} else if(response.result.length>0){
			var discussionThreadResponse = response.result[0];
			User.getList({$or: [{"userId":discussionThreadMessage.repliedBy},{"userId": discussionThreadMessage.repliedTo}]}, function(response){
				if(response.error){
					callback({
						status: REQUEST_CODES.ERROR,
						error: response.error
					});
				} else if(response.result.length>0){
					var repliedByName = response.result.filter(user=>{
						if(user.userId == discussionThreadMessage.repliedBy) {
							return user;
						}
					});
					var repliedToName = response.result.filter(user=>{
						if(user.userId == discussionThreadMessage.repliedTo) {
							return user;
						}
					});
					var threadReplies = {
						"repliedBy": {
							"userId": discussionThreadMessage.repliedBy,
							"userName": repliedByName[0].firstName+' '+repliedByName[0].lastName
						},
						"repliedTo": {
							"uId": discussionThreadMessage.repliedTo,
							"userName": repliedToName[0].firstName+' '+repliedToName[0].lastName
						},
						"message": discussionThreadMessage.message,
						"upvotes": [],
						"downvotes": [],
						"createdDate": new Date().getTime()
					};
					var threadMessages = discussionThreadResponse.message.map(message=> {
						if(message.messageId == discussionThreadMessage.messageId){
							message.replies.push(threadReplies);
							return message;
						} else {
							return message;
						};
					});
					discussionThreadResponse.message = threadMessages;
					update(discussionThreadResponse, function(response){
						if(response.error){
							callback({
								status: REQUEST_CODES.ERROR,
								error: response.error
							});
							return;
						} else {
							callback({
								status: REQUEST_CODES.SUCCESS,
								result: response.result
							});
							return;
						}
					});
				}
			});
		}
	});
}

function getTimeLine(discussionThreadId, callback){
	getDetails(discussionThreadId, function(response){
		if(!response.error){
			var threadResponse = response.result[0];
			Discussion.getDetails(threadResponse.discussionId, function(response){
				if(!response.error){
					threadResponse.discussionName = response.result[0].discussionName;
					let newMessageResponseObject = threadResponse.message.sort(GetSortOrderDesc("createdDate"));
					threadResponse.message = newMessageResponseObject;
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: [threadResponse]
					});
					return;
				}
			});
		} else {
			callback({
				status: REQUEST_CODES.ERROR,
				error: response.error
			})
		}
	})
}

function voteComments(query, callback){
	getDetails(query.discussionThreadId, function(response){
		if(response.error){
			callback({
				status: REQUEST_CODES.ERROR,
				error: response.error
			});
			return;
		} else {
			var discussionThread = response.result[0];
			var threadMessages = discussionThread.message.map(message=> {
				if(message.messageId == query.messageId){
					if(query.upVotes && query.upVotes.length > 0){
						if(!message.upvotes.includes(query.upVotes[0])){
							message.upvotes.push(query.upVotes[0]);
							if(message.downvotes.includes(query.upVotes[0])) {
								message.downvotes = message.downvotes.filter(vote=> vote != query.upVotes[0]);
							}
						}
					} 
					if(query.downVotes && query.downVotes.length > 0){
						if(!message.downvotes.includes(query.downVotes[0])){
							message.downvotes.push(query.downVotes[0]);
							if(message.upvotes.includes(query.downVotes[0])) {
								message.upvotes = message.upvotes.filter(vote=> vote != query.downVotes[0]);
							}
						}
					}
					return message;
				} else {
					return message;
				};
			});
			discussionThread.message = threadMessages;
			update(discussionThread, function(response){
				if(response.error){
					callback({
						status: REQUEST_CODES.ERROR,
						error: response.error
					});
					return;
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: [discussionThread]
					});
					return;
				}
			});
		}
	});
}

function getDetails(discussionThreadId, callback) {
	DiscussionThreadModel.find({"discussionThreadId": discussionThreadId}, function(error, discussionThreadRecords) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			discussionThreadRecords = discussionThreadRecords.map(function(discussionThreadRecord) {
				return new DiscussionThreadController.DiscussionThreadAPI(discussionThreadRecord);
			});			
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: discussionThreadRecords
			});
			return;		
		}
	});	
}

function getList(query, callback) {
	DiscussionThreadModel.find(query, function(error, discussionThreadRecords) {		
		if (error) {
			callback({
			    status: DB_CODES.FAIL,
			    error: error
			});
			return;
		} else {					
			discussionThreadRecords.map(function(discussionThreadRecord) {
				return new DiscussionThreadController.DiscussionThreadAPI(discussionThreadRecord);
			});
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: discussionThreadRecords
			});
			return;	
		}
	});
}

function updateMessages(discussionThread, callback) {
	getDetails(discussionThread.discussionThreadId, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result) {
				var discussionMessage = response.result[0].message.map(msg=> {
					if(msg.messageId == discussionThread.message[0].messageId){
						msg = discussionThread.message[0];
					}
					return msg;
				});
				DiscussionThreadModel.updateOne({"discussionThreadId": discussionThread.discussionThreadId}, {$set: {"message": discussionMessage}}, function(error, effectedRows) {
					if (error) {
						callback({
							status: DB_CODES.FAIL,
							error: error
						});
						return;
					} else {
						if (!effectedRows.nModified) {
							callback({
								status: REQUEST_CODES.FAIL,
								error: utils.formatText(DISCUSSION_CODES.UPDATE_FAIL, discussionThread.discussionThreadId)
							});
							return;
						} else {
							callback({
								status: REQUEST_CODES.SUCCESS,
								result: utils.formatText(DISCUSSION_CODES.UPDATE_SUCCESS, discussionThread.discussionThreadId)
							});
							return;
						}
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No Discussion Found"
				});
				return;
			}
		}
	});	 
}

function update(discussionThread, callback) {
	getDetails(discussionThread.discussionThreadId, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result) {
				discussionThread.updatedDate = new Date().getTime();
				DiscussionThreadModel.updateOne({"discussionThreadId": discussionThread.discussionThreadId}, {$set: discussionThread}, function(error, effectedRows) {
					if (error) {
						callback({
							status: DB_CODES.FAIL,
							error: error
						});
						return;
					} else {
						if (!effectedRows.nModified) {
							callback({
								status: REQUEST_CODES.FAIL,
								error: utils.formatText(DISCUSSION_CODES.UPDATE_FAIL, discussionThread.discussionThreadId)
							});
							return;
						} else {
							callback({
								status: REQUEST_CODES.SUCCESS,
								result: utils.formatText(DISCUSSION_CODES.UPDATE_SUCCESS, discussionThread.discussionThreadId)
							});
							return;
						}
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No Discussion Found"
				});
				return;
			}
		}
	});	 
}

function GetSortOrderDesc(prop) {  
    return function(a, b) {  
        if (a[prop] < b[prop]) {  
            return 1;  
        } else if (a[prop] > b[prop]) {  
            return -1;  
        }  
        return 0;  
    }  
}

module.exports.create = create;
module.exports.createThreadReplies = createThreadReplies;
module.exports.getDetails = getDetails;
module.exports.getList = getList;
module.exports.update = update;