var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var VALIDATE = utils.CONSTANTS.VALIDATE;
var validate = utils.validate;

var DiscussionThread = function() {
    return {
        discussionThreadId : 0,
		message : [],
        category: null,
        parentId: 0,
		discussionId : 0,
		participantIds : [],
		createdBy : 0,
		createdDate : 0,
		status : null
   }
};

function DiscussionThreadAPI(discussionThreadRecord) {
    var discussionThread = new DiscussionThread();

    discussionThread.getDiscussionThreadId = function() {
        return this.discussionThreadId;
    };
    discussionThread.setDiscussionThreadId = function(discussionThreadId) {
        if (discussionThreadId) {
            if (validate.isInteger(discussionThreadId + '')) {
                this.discussionThreadId = discussionThreadId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, discussionThreadId, 'discussionThreadId')
                };
            }
        }
    };

    discussionThread.getMessage = function() {
        return this.message;
    };
    discussionThread.setMessage = function(message) {       
        if (message) {
            this.message = message;
        }
    };  
    discussionThread.getCategory = function() {
        return this.category;
    };
    discussionThread.setCategory = function(category) {       
        if (category) {
            this.category = category;
        }
    };  
    discussionThread.getParentId = function() {
        return this.parentId;
    };
    discussionThread.setParentId = function(parentId) {
        if (parentId) {
            if (validate.isInteger(parentId + '')) {
                this.parentId = parentId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, parentId, 'parentId')
                };
            }
        }
    }; 
    discussionThread.getDiscussionId = function() {
        return this.discussionId;
    };
    discussionThread.setDiscussionId = function(discussionId) {
        if (discussionId) {
            if (validate.isInteger(discussionId + '')) {
                this.discussionId = discussionId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, discussionId, 'discussionId')
                };
            }
        }
    };
    discussionThread.getParticipantIds = function() {
        return this.participantIds;
    }
    discussionThread.setParticipantIds = function(participantIds) {
        if (participantIds) {
            this.participantIds = participantIds;
        }
    }
    discussionThread.getCreatedBy = function() {
        return this.createdBy;
    };
    discussionThread.setCreatedBy = function(createdBy) {
         if (createdBy) {
            if (validate.isInteger(createdBy + '')) {
                this.createdBy = createdBy;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdBy, 'createdBy')
                };
            }
        }
    };
    discussionThread.getCreatedDate = function() {
        return this.createdDate;
    };
    discussionThread.setCreatedDate = function(createdDate) {
        if (createdDate) {
            if (validate.isInteger(createdDate + '')) {
                this.createdDate = createdDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdDate, 'createdDate')
                };
            }
        }
    };
   
    if (discussionThreadRecord) {
        var errorList = [];
		try {
            discussionThread.setDiscussionThreadId(discussionThreadRecord.discussionThreadId);
        } catch (e) {
			errorList.push(e);
        }
        try {
            discussionThread.setMessage(discussionThreadRecord.message);
        } catch (e) {
			 errorList.push(e);
        }
        try {
            discussionThread.setCategory(discussionThreadRecord.category);            
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussionThread.setParentId(discussionThreadRecord.parentId);            
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussionThread.setDiscussionId(discussionThreadRecord.discussionId);            
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussionThread.setParticipantIds(discussionThreadRecord.participantIds);
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussionThread.setCreatedBy(discussionThreadRecord.createdBy);
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussionThread.setCreatedDate(discussionThreadRecord.createdDate);
        } catch (e) {
           errorList.push(e);
        }
        if (errorList.length) {
            throw {
                status: REQUEST_CODES.FAIL,
                error: errorList
            };
        }
    }
	return discussionThread;
}
module.exports.DiscussionThreadAPI = DiscussionThreadAPI;