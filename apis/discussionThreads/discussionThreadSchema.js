module.exports.discussionThreadSchema = {
						discussionThreadId: {
							type: Number,
                                    	unique: true,
                                    	required: true,
                                    	index: true
                                    },
                                    message: [],
                                    discussionId: {
                                    	type: Number,
	                                    required: true
                                    },
                                    category: {
                                          type: String
                                    },
                                    parentId: {
                                          type: Number
                                    },
                                    participantIds: [],
                                    createdBy: {
                                    	type: Number,
                                    	required: true
                                    },
                                    createdDate: {
                                    	type: Number,
	                                    required: true,
                                          index: true
                                    }
} 