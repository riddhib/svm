var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var VALIDATE = utils.CONSTANTS.VALIDATE;
var validate = utils.validate;

var Rating = function() {
    return {
        ratingId: 0,
        collectionName: null,
        collectionId: 0,
        rating: 0,
        ratingBy: null,
        ratingById: 0,
        ratingComments: null,
        createdDate: 0,
        updatedDate: 0
    };
};

function RatingAPI(ratingRecord) {
    var rating = new Rating();
    rating.getRatingId = function() {
        return this.ratingId;
    };
    rating.setRatingId = function(ratingId) {
        if (ratingId) {
            if (validate.isInteger(ratingId + '')) {
                    this.ratingId = ratingId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, ratingId, 'ratingId')
                };
            }
        }
    };
    rating.getCollectionName = function() {
        return this.collectionName;
    };
    rating.setCollectionName = function(collectionName) {
         if (collectionName) {
            if (collectionName.length <= 50) {
                this.collectionName = collectionName;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, collectionName, 'collectionName')
                };
            }
        }
    };
    rating.getCollectionId = function() {
        return this.collectionId;
    };
    rating.setCollectionId = function(collectionId) {
        if (collectionId) {
            if (validate.isInteger(collectionId + '')) {
                    this.collectionId = collectionId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, collectionId, 'collectionId')
                };
            }
        }
    };
    rating.getRating = function() {
        return this.rating;
    };
    rating.setRating = function(rating) {
        if (rating) {
            if (validate.isInteger(rating + '')) {
                    this.rating = rating;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, rating, 'rating')
                };
            }
        }
    };
    rating.getRatingBy = function() {
        return this.ratingBy;
    };
    rating.setRatingBy = function(ratingBy) {
         if (ratingBy) {
            if (ratingBy.length <= 50) {
                this.ratingBy = ratingBy;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, ratingBy, 'ratingBy')
                };
            }
        }
    };
    rating.getRatingById = function() {
        return this.ratingById;
    };
    rating.setRatingById = function(ratingById) {
        if (ratingById) {
            if (validate.isInteger(ratingById + '')) {
                    this.ratingById = ratingById;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, ratingById, 'ratingById')
                };
            }
        }
    };
    rating.getRatingComments = function() {
        return this.ratingComments;
    };
    rating.setRatingComments = function(ratingComments) {
        if (ratingComments) {
            this.ratingComments = ratingComments;
        }
    };
    rating.getCreatedDate = function() {
        return this.createdDate;
    };
    rating.setCreatedDate = function(createdDate) {
        if (createdDate) {
            if (validate.isInteger(createdDate + '')) {
                    this.createdDate = createdDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdDate, 'createdDate')
                };
            }
        }
    };
    rating.getUpdatedDate = function() {
        return this.updatedDate;
    };
    rating.setUpdatedDate = function(updatedDate) {
        if (updatedDate) {
            if (validate.isInteger(updatedDate + '')) {
                    this.updatedDate = updatedDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, updatedDate, 'updatedDate')
                };
            }
        }
    };
   
    if (ratingRecord) {
        var errorList = [];
        try {
            rating.setRatingId(ratingRecord.ratingId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            rating.setCollectionName(ratingRecord.collectionName);
        } catch (e) {
            errorList.push(e);
        }
        try {
            rating.setCollectionId(ratingRecord.collectionId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            rating.setRating(ratingRecord.rating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            rating.setRatingBy(ratingRecord.ratingBy);
        } catch (e) {
            errorList.push(e);
        }
        try {
            rating.setRatingById(ratingRecord.ratingById);
        } catch (e) {
            errorList.push(e);
        }
        try {
            rating.setRatingComments(ratingRecord.ratingComments);
        } catch (e) {
            errorList.push(e);
        }
        try {
            rating.setCreatedDate(ratingRecord.createdDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            rating.setUpdatedDate(ratingRecord.updatedDate);
        } catch (e) {
            errorList.push(e);
        }
        
        if (errorList.length) {
            throw {
                status: REQUEST_CODES.FAIL,
                error: errorList
            };
        }
    }
    return rating;
}

module.exports.RatingAPI = RatingAPI;