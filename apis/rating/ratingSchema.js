module.exports.ratingSchema = { 
                               ratingId: {
                                    type: Number,
                                    unique: true,
                                    required: true,
                                    index: true
                                },
                                collectionName: { 
                                    type: String
                                },
                                collectionId: {
                                    type: Number
                                },
                                rating: {
                                    type: Number
                                },
                                ratingBy: {
                                    type: String
                                },
                                ratingById: {
                                    type: Number
                                },
                                ratingComments: {
                                    type: String
                                },
                                createdDate: { 
                                    type: Number
                                },
                                updatedDate: { 
                                    type: Number
                                }

};