module.exports = function(app){
	app.post('/ui/rating',function(req, res){
        try{
            create(req.body, function(response){
               res.json(response);
        	});
         
        }catch(e){
            res.json(e);
        }
    });
	app.put('/ui/rating', function(req, res) {
		try {
			update(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
    app.get('/ui/rating/:ratingId', function(req, res) {
		try {
			getDetails(req.params.ratingId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/query/rating', function(req, res) {
		try {
			getList(req.query, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
}

var mongoose = require('mongoose')
	Schema = mongoose.Schema;
var RatingSchema = new Schema(require('./ratingSchema').ratingSchema, {collection: 'rating'});
var RatingModel = mongoose.model('rating', RatingSchema);
var RatingController = require('./ratingController');
var utils = require('../../assets/utils').utils;
var MailHelper = require('../mailHelper/mailHelper');
var mongoUtils = utils.mongoUtils;
var CONSTANTS = utils.CONSTANTS;
var VALIDATE = CONSTANTS.VALIDATE;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var RATE_CODES = CONSTANTS.RATE_CODES;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;

function create(rating, callback) {
	var ratingAPI = RatingController.RatingAPI(rating);
    var errorList = [];
    if (!ratingAPI.getCollectionName()) {
       	var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'collectionName')
		};
		errorList.push(e);
	}	
   	if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var ratingModel = new RatingModel(ratingAPI);
		mongoUtils.getNextSequence('ratingId', function(oSeq) {
			ratingModel.ratingId = oSeq;
			ratingModel.createdDate = new Date().getTime();
			ratingModel.save(function(error) {
				if (error) {
					callback({
						status: DB_CODES.FAIL,
						error: error
					});
					return;
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: utils.formatText(RATE_CODES.CREATE_SUCCESS, ratingModel.ratingId),
						ratingId: ratingModel.ratingId
					});
					return;							
				}
			});
		});
	}
}

function getDetails(ratingId, callback) {
	RatingModel.find({"ratingId": ratingId}, function(error, ratingRecords) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			ratingRecords = ratingRecords.map(function(ratingRecord) {
				return new RatingController.RatingAPI(ratingRecord);
			});			
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: ratingRecords
			});
			return;		
		}
	});	
}

function getList(query, callback) {
	RatingModel.find(query, function(error, ratingRecords) {		
		if (error) {
			callback({
			    status: DB_CODES.FAIL,
			    error: error
			});
			return;
		} else {		
			ratingRecords = ratingRecords.map(function(ratingRecord) {
				return new RatingController.RatingAPI(ratingRecord);
			});
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: ratingRecords
			});
			return;	
		}
	});
}

function update(rating, callback) {
	getDetails(rating.ratingId, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result) {
				rating.updatedDate = new Date().getTime();
				RatingModel.updateOne({"ratingId": rating.ratingId}, {$set: rating}, function(error, effectedRows) {
					if (error) {
						callback({
							status: DB_CODES.FAIL,
							error: error
						});
						return;
					} else {
						if (!effectedRows.nModified) {
							callback({
								status: REQUEST_CODES.FAIL,
								error: utils.formatText(RATE_CODES.UPDATE_FAIL, rating.ratingId)
							});
							return;
						} else {
							callback({
								status: REQUEST_CODES.SUCCESS,
								result: utils.formatText(RATE_CODES.UPDATE_SUCCESS, rating.ratingId)
							});
							return;
						}
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No Rating Found"
				});
				return;
			}
		}
	});	 
}

module.exports.create = create;
module.exports.getDetails = getDetails;
module.exports.getList = getList;
module.exports.update = update;
