module.exports = function(app){
	app.post('/ui/stakeholder',function(req, res){
        try{
            create(req.body, function(response){
               res.json(response);
        	});
         
        }catch(e){
            res.json(e);
        }
    });
	app.put('/ui/stakeholder', function(req, res) {
		try {
			update(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
    app.get('/ui/stakeholder/:stakeholderId', function(req, res) {
		try {
			getDetails(req.params.stakeholderId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/query/stakeholder', function(req, res) {
		try {
			getList(req.query, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
}

var mongoose = require('mongoose')
	Schema = mongoose.Schema;
var StakeholderSchema = new Schema(require('./stakeholderSchema').stakeholderSchema, {collection: 'stakeholder'});
var StakeholderModel = mongoose.model('stakeholder', StakeholderSchema);
var StakeholderController = require('./stakeholderController');
var utils = require('../../assets/utils').utils;
var MailHelper = require('../mailHelper/mailHelper');
var mongoUtils = utils.mongoUtils;
var CONSTANTS = utils.CONSTANTS;
var VALIDATE = CONSTANTS.VALIDATE;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var STACKEHOLDER_CODES = CONSTANTS.STACKEHOLDER_CODES;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;

function create(stakeholder, callback) {
	var stakeholderAPI = StakeholderController.StakeholderAPI(stakeholder);
    var errorList = [];
    if (!stakeholderAPI.getName()) {
       	var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'name')
		};
		errorList.push(e);
	}	
   	if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var stakeholderModel = new StakeholderModel(stakeholderAPI);
		mongoUtils.getNextSequence('stakeholderId', function(oSeq) {
			stakeholderModel.stakeholderId = oSeq;
			stakeholderModel.createdDate = new Date().getTime();
			stakeholderModel.save(function(error) {
				if (error) {
					callback({
						status: DB_CODES.FAIL,
						error: error
					});
					return;
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: utils.formatText(STACKEHOLDER_CODES.CREATE_SUCCESS, stakeholderModel.stakeholderId)
					});
					return;							
				}
			});
		});
	}
}

function getDetails(stakeholderId, callback) {
	StakeholderModel.find({"stakeholderId": stakeholderId}, function(error, stakeholderRecords) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			stakeholderRecords = stakeholderRecords.map(function(stakeholderRecord) {
				return new StakeholderController.StakeholderAPI(stakeholderRecord);
			});			
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: stakeholderRecords
			});
			return;		
		}
	});	
}

function getList(query, callback) {
	StakeholderModel.find(query, function(error, stakeholderRecords) {		
		if (error) {
			callback({
			    status: DB_CODES.FAIL,
			    error: error
			});
			return;
		} else {		
			stakeholderRecords = stakeholderRecords.map(function(stakeholderRecord) {
				return new StakeholderController.StakeholderAPI(stakeholderRecord);
			});
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: stakeholderRecords
			});
			return;	
		}
	});
}

function update(stakeholder, callback) {
	getDetails(stakeholder.stakeholderId, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result) {
				stakeholder.updatedDate = new Date().getTime();
				StakeholderModel.update({"stakeholderId": stakeholder.stakeholderId}, {$set: stakeholder}, function(error, effectedRows) {
					if (error) {
						callback({
							status: DB_CODES.FAIL,
							error: error
						});
						return;
					} else {
						if (!effectedRows.nModified) {
							callback({
								status: REQUEST_CODES.FAIL,
								error: utils.formatText(STACKEHOLDER_CODES.UPDATE_FAIL, stakeholder.stakeholderId)
							});
							return;
						} else {
							callback({
								status: REQUEST_CODES.SUCCESS,
								result: utils.formatText(STACKEHOLDER_CODES.UPDATE_SUCCESS, stakeholder.stakeholderId)
							});
							return;
						}
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No Stakeholder Found"
				});
				return;
			}
		}
	});	 
}

module.exports.create = create;
module.exports.getDetails = getDetails;
module.exports.getList = getList;
module.exports.update = update;
