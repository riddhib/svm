var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var VALIDATE = utils.CONSTANTS.VALIDATE;
var validate = utils.validate;

var Stakeholder = function() {
    return {
        stakeholderId: 0,
        name: null,
        location: null,
        type: null,
        interests: null,
        logo: null,
        description: null,
        previousSV: null,
        website: null,
        resources: 0,
        projects: 0,
        expectations: null,
        poc: null,
        email: null,
        contact: 0,
        linkedin: null,
        createdDate: 0,
        updatedDate: 0
    };
};

function StakeholderAPI(stakeholderRecord) {
    var stakeholder = new Stakeholder();
    stakeholder.getStakeholderId = function() {
        return this.stakeholderId;
    };
    stakeholder.setStakeholderId = function(stakeholderId) {
        if (stakeholderId) {
            if (validate.isInteger(stakeholderId + '')) {
                    this.stakeholderId = stakeholderId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, stakeholderId, 'stakeholderId')
                };
            }
        }
    };
    stakeholder.getName = function() {
        return this.name;
    };
    stakeholder.setName = function(name) {
         if (name) {
            if (name.length <= 50) {
                this.name = name;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, name, 'name')
                };
            }
        }
    };
    stakeholder.getLocation = function() {
        return this.location;
    };
    stakeholder.setLocation = function(location) {
         if (location) {
            if (location.length <= 50) {
                this.location = location;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, location, 'location')
                };
            }
        }
    };
    stakeholder.getType = function() {
        return this.type;
    };
    stakeholder.setType = function(type) {
        if (type) {
            this.type = type;
        }
    };
    stakeholder.getInterests = function() {
        return this.interests;
    };
    stakeholder.setInterests = function(interests) {
        if (interests) {
            this.interests = interests;
        }
    };
    stakeholder.getLogo = function() {
        return this.logo;
    };
    stakeholder.setLogo = function(logo) {
        if (logo) {
            this.logo = logo;
        }
    };
    stakeholder.getDescription = function() {
        return this.description;
    };
    stakeholder.setDescription = function(description) {
        if (description) {
            this.description = description;
        }
    };
    stakeholder.getPreviousSV = function() {
        return this.previousSV;
    };
    stakeholder.setPreviousSV = function(previousSV) {
        if (previousSV) {
            this.previousSV = previousSV;
        }
    };
    stakeholder.getWebsite = function() {
        return this.website;
    };
    stakeholder.setWebsite = function(website) {
        if (website) {
            this.website = website;
        }
    };
    stakeholder.getResources = function() {
        return this.resources;
    };
    stakeholder.setResources = function(resources) {
        if (resources) {
            if (validate.isInteger(resources + '')) {
                    this.resources = resources;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, resources, 'resources')
                };
            }
        }
    };
    stakeholder.getProjects = function() {
        return this.projects;
    };
    stakeholder.setProjects = function(projects) {
        if (projects) {
            if (validate.isInteger(projects + '')) {
                    this.projects = projects;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, projects, 'projects')
                };
            }
        }
    };
    stakeholder.getExpectations = function() {
        return this.expectations;
    };
    stakeholder.setExpectations = function(expectations) {
        if (expectations) {
            this.expectations = expectations;
        }
    };
    stakeholder.getPoc = function() {
        return this.poc;
    };
    stakeholder.setPoc = function(poc) {
        if (poc) {
            this.poc = poc;
        }
    };
    stakeholder.getEmail = function() {
        return this.email;
    };
    stakeholder.setEmail= function(email) {
        if (email) {
            if (utils.validate.isEmail(email)) {
                this.email = email;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_AN_EMAIL, email, 'email')
                };
            }
        }
    };
    stakeholder.getContact = function() {
        return this.contact;
    };
    stakeholder.setContact = function(contact) {
        if (contact) {
            if (validate.isInteger(contact + '')) {
                    this.contact = contact;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, contact, 'contact')
                };
            }
        }
    };
    stakeholder.getLinkedin = function() {
        return this.linkedin;
    };
    stakeholder.setLinkedin = function(linkedin) {
        if (linkedin) {
            this.linkedin = linkedin;
        }
    };
    stakeholder.getCreatedDate = function() {
        return this.createdDate;
    };
    stakeholder.setCreatedDate = function(createdDate) {
        if (createdDate) {
            if (validate.isInteger(createdDate + '')) {
                    this.createdDate = createdDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdDate, 'createdDate')
                };
            }
        }
    };
    stakeholder.getUpdatedDate = function() {
        return this.updatedDate;
    };
    stakeholder.setUpdatedDate = function(updatedDate) {
        if (updatedDate) {
            if (validate.isInteger(updatedDate + '')) {
                    this.updatedDate = updatedDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, updatedDate, 'updatedDate')
                };
            }
        }
    };
   
    if (stakeholderRecord) {
        var errorList = [];
        try {
            stakeholder.setStakeholderId(stakeholderRecord.stakeholderId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setName(stakeholderRecord.name);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setLocation(stakeholderRecord.location);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setType(stakeholderRecord.type);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setInterests(stakeholderRecord.interests);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setLogo(stakeholderRecord.logo);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setDescription(stakeholderRecord.description);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setPreviousSV(stakeholderRecord.previousSV);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setWebsite(stakeholderRecord.website);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setResources(stakeholderRecord.resources);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setProjects(stakeholderRecord.projects);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setExpectations(stakeholderRecord.expectations);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setPoc(stakeholderRecord.poc);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setEmail(stakeholderRecord.email);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setContact(stakeholderRecord.contact);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setLinkedin(stakeholderRecord.linkedin);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setCreatedDate(stakeholderRecord.createdDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            stakeholder.setUpdatedDate(stakeholderRecord.updatedDate);
        } catch (e) {
            errorList.push(e);
        }
        
        if (errorList.length) {
            throw {
                status: REQUEST_CODES.FAIL,
                error: errorList
            };
        }
    }
    return stakeholder;
}

module.exports.StakeholderAPI = StakeholderAPI;