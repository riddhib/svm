module.exports.stakeholderSchema = { 
                               stakeholderId: {
                                    type: Number,
                                    unique: true,
                                    required: true,
                                    index: true
                                },
                                name: { 
                                    type: String
                                },
                                location: { 
                                    type: String
                                },
                                type: {
                                    type: String
                                },
                                interests: {
                                    type: String
                                },
                                logo: {
                                    type: String
                                },
                                description: {
                                    type: String
                                },
                                previousSV: {
                                    type: String
                                },
                                website: {
                                    type: String
                                },
                                resources: {
                                    type: Number
                                },
                                projects: {
                                    type: Number
                                },
                                expectations: {
                                    type: String
                                },
                                poc: {
                                    type: String
                                },
                                email: { 
                                    type: String
                                },
                                contact: {
                                    type: Number
                                },
                                linkedin: {
                                    type: String
                                },
                                createdDate: { 
                                    type: Number
                                },
                                updatedDate: { 
                                    type: Number
                                }

};