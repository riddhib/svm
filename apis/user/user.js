module.exports = function(app){
	app.post('/ui/user',function(req, res){
        try{
            create(req.body, function(response){
               res.json(response);
        	});
         
        }catch(e){
            res.json(e);
        }
    });
	app.put('/ui/user', function(req, res) {
		try {
			req.body.uId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			update(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
	app.post('/ui/resetPassword',function(req, res) {
		try{
			req.body.url = req.url;
			req.body.method = req.method;
			resetPassword(req.body, function(response){
			res.json(response);
		});
		}catch(e){
		 res.json(e);
		}
	});
    app.post('/ui/user/forgotPassword',function(req, res) {
        try{
        	req.body.url = req.url;
			req.body.method = req.method;
         	forgotPassword(req.body, function(response) {
            	res.json(response);
            });
        }catch(e) {
             res.json(e);
        }
    });
	app.put('/ui/following', function(req, res) {
		try {
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			following(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
	app.put('/ui/userRating', function(req, res) {
		try {
			req.body.uId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			userRating(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
    app.get('/ui/user/:userId', function(req, res) {
		try {
			getDetails(req.params.userId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/editProfile/:userId', function(req, res) {
		try {
			editProfile(req.params.userId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/query/user', function(req, res) {
		try {
			getList(req.query, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/activeUser/:userId', function(req, res) {
		try {
			activelink(req.params, function(response) {
				res.redirect(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.put('/ui/changepassword',function(req, res) {
        try {
        	req.body.uId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            changePassword(req.body, function(response) {
        	    res.json(response);
            });
        } catch(e){
            res.json(e);
        }
	});
	app.post('/ui/profileUpload',function(req, res) {
        try {
        	req.body.uId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            profileUpload(req.body, function(response) {
        	    res.json(response);
            });
        } catch(e){
            res.json(e);
        }
    });
    app.post('/ui/uploadResearchDocuments',function(req, res) {
        try {
        	req.body.uId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            uploadResearchDocuments(req.body, function(response) {
        	    res.json(response);
            });
        } catch(e){
            res.json(e);
        }
	});
	app.get('/ui/iamFollowing/:userId', function(req, res) {
		try {
			iamFollowing(req.params.userId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
}

var mongoose = require('mongoose')
	Schema = mongoose.Schema;
var UserSchema = new Schema(require('./userSchema').userSchema, {collection: 'user'});
var UserModel = mongoose.model('user', UserSchema);
var UserController = require('./userController');
var utils = require('../../assets/utils').utils;
var File = require('../file/file');
var Challenge = require('../challenge/challenge');
var Solution = require('../solution/solution');
var Collaborate = require('../collaborate/collaborate');
var Research = require('../research/research');
var Project = require('../project/project');
var Rating = require('../rating/rating');
var Discussion = require('../discussions/discussion');
var MailHelper = require('../mailHelper/mailHelper');
var UserLog = require('../userLog/userLog');
var mongoUtils = utils.mongoUtils;
var CONSTANTS = utils.CONSTANTS;
var VALIDATE = CONSTANTS.VALIDATE;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var USER_CODES = CONSTANTS.USER_CODES;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var FORGOTPASSWORD = CONSTANTS.FORGOT_PASSWORD;
var _ = require('underscore');

function create(user, callback) {
	var userAPI = UserController.UserAPI(user);
	//var randomNumber = utils.getRandomNumber();
	userAPI.setIsSuperUser(false);
	var password;
	if (user.password) {
		password = user.password;
	} else {
		password = 123456;
	}
	if (user.isAdmin === undefined || user.isAdmin != true) {
		userAPI.setIsAdmin(false);
	}
	if (!user.status || user.status === undefined) {
		userAPI.setStatus('Active');
	}
	userAPI.setPassword(utils.encryptText(password));
    var errorList = [];
    if (!userAPI.getFirstName()) {
       	var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'firstName')
		};
		errorList.push(e);
	}
	if (!userAPI.getLastName()) {
		var e = {
			status: VALIDATE.FAIL,
		 	error: utils.formatText(VALIDATE.REQUIRED, 'lastName')
	 	};
	 	errorList.push(e);
 	}	
	if (!userAPI.getEmail()) {
        var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'email')
		};
		console.log(e);
		errorList.push(e);
    }
    if (!userAPI.getPassword()) {
        var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'password')
     	};
		errorList.push(e);
    }
    //I made this change
    if (!userAPI.getState()) {
        var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'state')
     	};
		errorList.push(e);
    }
   	if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var userModel = new UserModel(userAPI);
		getList({email: userModel.email}, function(response) {
			if(response.error) {
				callback(response);
			} else if (response.result.length > 0){
				callback({
					status: REQUEST_CODES.FAIL,
					error: USER_CODES.ALREADY_EXIST
				});
			} else {
				mongoUtils.getNextSequence('userId', function(oSeq) {
					userModel.userId = oSeq;
					if(userModel.userId == 1) {
						userModel.isSuperUser = true;
					}
					userModel.status = "Inactive";
					userModel.createdDate = new Date().getTime();
                    userModel.state = userAPI.getUserState();
					userModel.resources = [
						{
							fileId: [],
							category: "Agriculture"
						},
						{
							fileId: [],
							category: "Healthcare"
						},
						{
							fileId: [],
							category: "Transportation"
						},
						{
							fileId: [],
							category: "Water"
						},
						{
							fileId: [],
							category: "Connectivity"
						},
						{
							fileId: [],
							category: "Education"
						},
						{
							fileId: [],
							category: "Energy"
						},
						{
							fileId: [],
							category: "Livelihood"
						}						
					];
					userModel.save(function(error) {
						if (error) {
							callback({
								status: DB_CODES.FAIL,
								error: error
							});
							return;
						} else {
							var collaborate = {
								name: userModel.firstName+ ' ' +userModel.lastName,
								sector: userModel.areaOfInterest,
								type: userModel.type
							};
							Collaborate.create(collaborate, function(response) {
								if(response.error) {
									callback(response);
								} else {
									var subject = '';
									if (userModel.isAdmin == true || userModel.isAdmin == "true") {
										subject = 'Organization Registered'
									} else {
										subject = 'Account Created';
									}
									var template =  {
										body: '<p> Dear <b>' + utils.getFullName(user) + '</b>,</p><p>Thanks for signing up with <a href='+CONSTANTS.BASE_URL_UI+'>Smart Village Movement</a>.</p><p>Please <a href='+CONSTANTS.BASE_URL_API+'/ui/activeUser/'+oSeq+'>Click Here</a> to activate and sign in to your SVM account.</p><p style="margin-top: 100px;">Thanks & Regards</p><p>Support Team</p><p>Smart Village Movement</p><p><b style="font-family: Roboto, Helvetica Neue, Arial, sans-serif;letter-spacing: -2px;font-weight: 700;color:#1b84e7;font-size: 30px;">SVM</b></p>',
										recipients: userModel.email,
										from: 'SVM',
										subject: subject
									};
									MailHelper.sendMail(template, function(response) {
										if (response.result) {
											callback({
												status: REQUEST_CODES.SUCCESS,
												result: USER_CODES.CREATE_MAIL_SENT
											});
											return;
										} else {
											callback({
												status: REQUEST_CODES.SUCCESS,
												result: [utils.formatText('User Created successfully, But failed to send email')]
											});
											return;
										}						
									});	
								}
							});						
						}
					});
				});
			}
		});
	}
}

function getDetails(userId, callback) {
	UserModel.find({"userId": userId}, function(error, userRecords) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			userRecords = userRecords.map(function(userRecord) {
				return new UserController.UserAPI(userRecord);
			});			
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: userRecords
			});
			return;		
		}
	});	
}

function editProfile(userId, callback) {
	getDetails(userId, function(response) {
		if(response.error) {
			callback(response);
		} else {
			if(response.result.length < 1) {
				callback({
					status: REQUEST_CODES.SUCCESS,
					result: {user: [], files: [], profile: []}
				});
			} else {
				var userRecord = response.result[0];
				var fileIds = _.flatten(_.pluck(userRecord.resources, 'fileId'));
				File.getList({fileId: {$in: fileIds}}, function(response) {
					if(response.error) {
						callback(response);
					} else {
						var fileRecords = response.result;
						File.getList({fileId: userRecord.profileId}, function(response) {
							if(response.error) {
								callback(response);
							} else {
								var profileRecord = response.result;
								callback({
									status: REQUEST_CODES.SUCCESS,
									result: {user: userRecord, files: fileRecords, profile: profileRecord}
								});
							}
						});
					}
				});
			}
		}
	});
}

function getList(query, callback) {
	UserModel.find(query, function(error, userRecords) {		
		if (error) {
			callback({
			    status: DB_CODES.FAIL,
			    error: error
			});
			return;
		} else {		
			userRecords = userRecords.map(function(userRecord) {
				return new UserController.UserAPI(userRecord);   
			});
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: userRecords
			});
			return;	
		}
	}).sort({createdDate: -1});
}

function update(user, callback) {
	if(user.email) {
		callback({
			status: REQUEST_CODES.FAIL,
			result: "We can't change Email"
		});
		return;
	} else {
		getDetails(user.userId, function(response) {
			if (response.error) {
				callback(response);
				return;
			} else {
				if (response.result) {
					var userDetails = response.result[0];
					if (user.password) {
						user.password = utils.encryptText(user.password);
					}
					user.updatedDate = new Date().getTime();
					UserModel.updateOne({"userId": user.userId}, {$set: user}, function(error, effectedRows) {
						if (error) {
							callback({
								status: DB_CODES.FAIL,
								error: error
							});
							return;
						} else {
							if (!effectedRows.nModified) {
								callback({
									status: REQUEST_CODES.FAIL,
									error: utils.formatText(USER_CODES.UPDATE_FAIL, user.userId)
								});
								return;
							} else {
								let logObj = {
									"userId": user.uId || user.userId,
									"userName": user.userName,
									"email": user.userEmail,
									"requestUrl": user.url,
									"requestMethod": user.method,
									"apiRequest": user.apiRequest || {"module": "User-Update","name": userDetails.firstName+' '+userDetails.lastName,"id": user.userId},
									"apiResponse": "SUCCESS"
								}
								UserLog.create(logObj, function(response){
									if(!response.error){
										callback({
											status: REQUEST_CODES.SUCCESS,
											result: utils.formatText(USER_CODES.UPDATE_SUCCESS, user.userId)
										});
										return;
									}
								});
							}
						}
					});
				} else {
					callback({
						status: REQUEST_CODES.FAIL,
						result: "No User Found"
					});
					return;
				}
			}
		});	 
	}
}

function forgotPassword(reqBody, callback) {
	getList({email: reqBody.email}, function(response) {
		if (response.error) {
			callback(error);
		} else {
			var user = response.result[0] || [];
			if(user.userId) {
				var template =  {
					body: '<p> Dear <b>' + utils.getFullName(user) + '</b>,</p><p>Please click the below link to reset password</p><p><a href='+CONSTANTS.BASE_URL_UI+'/changepassword::'+user.userId+'>Reset Password</a></p><p style="margin-top: 100px;">Thanks & Regards </p><p>Support Team</p><p>Smart Village Movement</p><p><b style="font-family: Roboto, Helvetica Neue, Arial, sans-serif;letter-spacing: -2px;font-weight: 700;color:#1b84e7;font-size: 30px;">SVM</p>',
					recipients: [user.email],
					from: 'SVM',
					subject: 'Reset Password'
				};
				MailHelper.sendMail(template, function(response) {
					if (response.result) {
						callback({
							status: REQUEST_CODES.SUCCESS,
							result: "Password reset link sent to your mail successfully"
						});
						return;
					} else {
						callback({
							status: REQUEST_CODES.SUCCESS,
							result: [utils.formatText('Password updated successfully, but unable to send the email to user whose id is ', user.userId)]
						});
						return;
					}						
				});
			} else{
				callback({
					status: REQUEST_CODES.FAIL,
					error: FORGOTPASSWORD.INVALID_EMAIL
				});
				return;
			}
		}
	});
}

function resetPassword(user, callback) {
	getList({userId: user.userId}, function(response) {
		if (response.error) {
			callback(error);
		} else if (response.result.length < 1) {
			callback({
			    status: REQUEST_CODES.FAIL,
				error: FORGOTPASSWORD.ACCOUNT_NOT_FOUND
			});
			return;
		} else {
			var userRecord = response.result[0];
			user.userId = userRecord.userId;
			user.uId = userRecord.userId;
			user.userName = userRecord.firstName+' '+userRecord.lastName;
			user.userEmail = userRecord.email;
			user.url = user.url;
			user.method = user.method;
			user.apiRequest = {
				"module": "User-ResetPassword",
				"name": userRecord.firstName+' '+userRecord.lastName,
				"id": userRecord.userId
			};
			user.apiResponse = "SUCCESS";
			update(user, function(response) {
				if (response.error) {
					callback(response);
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: "Password reset successfully"
					});
					return;
				}
			});	
		}
	});
}

function activelink(params, callback) {
	getDetails(params.userId, function(response) {
		if (response.error) {
			callback(CONSTANTS.BASE_URL_UI+'/'+CONSTANTS.ERROR_PAGE);			
		} else {
			if (response.result.length < 1) {
				callback(CONSTANTS.BASE_URL_UI+'/'+CONSTANTS.ERROR_PAGE);
			} else {
				var user = response.result[0];
				if(user.status == "Active") {
					callback(CONSTANTS.BASE_URL_UI+'/'+CONSTANTS.LINK_EXPIRED);
				} else {
					var userRecord = {
						"userId": params.userId,
						"uId": params.userId,
						"status": "Active",
						"userName": user.firstName+' '+user.lastName,
						"userEmail": user.email,
						"url": '/ui/activeUser/',
						"method": 'GET',
						"apiRequest": {
							"module": "User-Activation",
							"name": user.firstName+' '+user.lastName,
							"id": params.userId
						},
						"apiResponse": "SUCCESS"
					};
					update(userRecord, function(response) {
						if (response.error) {
							callback(CONSTANTS.BASE_URL_UI);
						} else {
							callback(CONSTANTS.BASE_URL_UI+'/'+CONSTANTS.ACTIVATE_USER);
						}
					});
				}
			}
		}
	});
}

function changePassword(user, callback) {
	var errorList = [];
	if (!user.userId) {
		errorList.push({
			status: VALIDATE.FAIL,
			error: utils.formatText(CONSTANTS.VALIDATE.REQUIRED, 'userId')
		});
	} 
	if (!user.password) {
		errorList.push({
			status: VALIDATE.FAIL,
			error: utils.formatText(CONSTANTS.VALIDATE.REQUIRED, 'password')
		});
	} 
	if (!user.newPassword) {
		errorList.push({
			status: VALIDATE.FAIL,
			error: utils.formatText(CONSTANTS.VALIDATE.REQUIRED, 'newPassword')
		});
	}
	if (errorList.length) {
		throw {
			status: REQUEST_CODES.FAIL,
			error: VALIDATE.MANDATORY,
			errorList: errorList
		};
	} else {
		getDetails(user.userId, function(response) {
			if (response.error) {
				callback(response);
			} else if (response.result.length < 1) {
				callback({
					status: REQUEST_CODES.FAIL,
					error: FORGOTPASSWORD.ACCOUNT_NOT_FOUND
				});
				return;
			} else {
				var userObj = response.result[0];
				if (utils.encryptText(user.password) == userObj.password) {
					var userRecord = {
						'userId': userObj.userId,
						'password': user.newPassword,
						"uId": user.uId,
						"userName": user.userName,
						"userEmail": user.userEmail,
						"url": user.url,
						"method": user.method,
						"apiRequest": {
							"module": "User-UpdatePassword",
							"name": userObj.firstName+' '+userObj.lastName,
							"id": user.userId
						},
						"apiResponse": "SUCCESS"
					};
					update(userRecord, function(response) {
						if (response.error) {
							callback(response);
						} else {
							callback(response);
						}
					});				
				} else {
					callback({
					    status: REQUEST_CODES.FAIL,
						error: FORGOTPASSWORD.WRONG_PASSWORD
					});
					return;
				}
			}
		});
	}
}

function profileUpload(file, callback) {
	File.create({fileName: file.advanceDetails.fileName, originalName: file.file.originalname, filePath: '/uploads/profiles/'+file.file.filename, userId: file.advanceDetails.userId, category: "profile", mimeType: file.file.mimetype}, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var fileResponse = response.file;
			var user = {
				"userId": file.advanceDetails.userId,
				"uId": file.uId,
				"profileId": fileResponse.fileId,
				"userName": file.userName,
				"userEmail": file.userEmail,
				"url": file.url,
				"method": file.method,
				"apiRequest": {
					"module": "User-Update",
					"name": file.userName,
					"id": file.advanceDetails.userId
				},
				"apiResponse": "SUCCESS"
			}
			update(user, function(response){
				if(response.error){
					callback(response);
				} else {
					let logObj = {
						"userId": file.uId,
						"userName": file.userName,
						"email": file.userEmail,
						"requestUrl": file.url,
						"requestMethod": file.method,
						"apiRequest": {
							"module": "User-ProfileUpload",
							"name": file.userName,
							"id": file.advanceDetails.userId
						},
						"apiResponse": "SUCCESS"
					}
					UserLog.create(logObj, function(response){
						if(!response.error){
							callback({
								status: REQUEST_CODES.SUCCESS,
								result: "Profile uploaded"
							});
							return;
						}
					});
				}
			});
		}
	});
}

function uploadResearchDocuments(file, callback) {
	File.create({fileName: file.advanceDetails.fileName, originalName: file.file.originalname, filePath: '/uploads/researchDocuments/'+file.file.filename, userId: file.advanceDetails.userId, category: "researchDocument", subCategory: file.advanceDetails.subCategory, mimeType: file.file.mimetype}, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var fileResponse = response.file;
			getDetails(file.advanceDetails.userId, function(response) {
				if(response.error) {
					callback(response);
				} else {
					var userRecord = response.result[0];
					var events = _.filter(userRecord.resources, function(list){ 
						if(list.category == file.advanceDetails.subCategory) {
							list.fileId.push(fileResponse.fileId);
						}
						return list; 
					});
					var user = {
						"userId": file.advanceDetails.userId,
						"uId": file.uId,
						"resources": events,
						"userName": file.userName,
						"userEmail": file.userEmail,
						"url": file.url,
						"method": file.method,
						"apiRequest": {
							"module": "User-Update",
							"name": file.userName,
							"id": file.advanceDetails.userId
						},
						"apiResponse": "SUCCESS"
					}
					update(user, function(response){
						if(response.error){
							callback(response);
						} else {
							let logObj = {
								"userId": file.uId,
								"userName": file.userName,
								"email": file.userEmail,
								"requestUrl": file.url,
								"requestMethod": file.method,
								"apiRequest": {
									"module": "User-ResearchDocumentUpload",
									"name": file.userName,
									"id": file.advanceDetails.userId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: "Resource Doccument uploaded"
									});
									return;
								}
							});
						}
					});
				}
			});
		}
	});
}

function following(following, callback) {
	var Following;
	var followingObj = {};
	if(following.collectionName == "challenge") {
		Following = Challenge;
		followingObj.challengeId =  following.collectionId;
	} else if(following.collectionName == "solution") {
		Following = Solution;
		followingObj.solutionId =  following.collectionId;
	} else if(following.collectionName == "collaborate") {
		Following = Collaborate;
		followingObj.collaborateId =  following.collectionId;
	} else if(following.collectionName == "research") {
		Following = Research;
		followingObj.researchId =  following.collectionId;
	} else if(following.collectionName == "project") {
		Following = Project;
		followingObj.projectId =  following.collectionId;
	} else if(following.collectionName == "discussion") {
		Following = Discussion;
		followingObj.discussionId =  following.collectionId;
	} else {
		Following = Challenge;
		followingObj.challengeId =  following.collectionId;
	}
	Following.getDetails(following.collectionId, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var followingRecord = response.result[0];
			var message = "";
			if(following.follow) {
				followingRecord.following.push({userId: following.userId, userName: following.userName});
				followingObj.following = _.unique(followingRecord.following)
				message = "following";
			} else {
				followingObj.following = followingRecord.following.filter((user) => user.userId != following.userId);
				message = "follow";
			}
			followingObj.userId = following.userId;
			followingObj.uId = following.uId;
			followingObj.userName = following.userName;
			followingObj.userEmail = following.userEmail;
			followingObj.url = following.url;
			followingObj.method = following.method;
			followingObj.apiRequest = {
				"module": "Following-Update",
				"name": followingRecord
			};
			followingObj.apiResponse = "SUCCESS";
			Following.update(followingObj, function(response) {
				if(response.error) {
					callback(response);
				} else {
					userFollowing(following, function(response) {
						if(response.error) {
							callback(response);
						} else {
							Following.getList({}, function(response) {
								if(response.error) {
									callback(response);
								} else {
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: response.result
									});
								}
							});
						}
					});
				}
			})
		}
	});
}

function userFollowing(follow, callback) {
	getDetails(follow.userId, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var userRecord = response.result[0];
			if(!userRecord) {
				callback({
					status: REQUEST_CODES.FAIL,
					error: "User no Found"
				});
			} else {
				var user = {
					"userId": userRecord.userId,
					"uId": follow.uId,
					"following": userRecord.following,
					"userName": userRecord.firstName+' '+userRecord.lastName,
					"userEmail": userRecord.email,
					"url": follow.url,
					"method": follow.method,
					"apiRequest": {
						"module": "User-Following",
						"name": userRecord.firstName+' '+userRecord.lastName,
						"id": userRecord.userId
					},
					"apiResponse": "SUCCESS"
				};
				var index = userRecord.following.findIndex(x => x.collectionName == follow.collectionName);
				if(follow.follow) {
					if(index < 0) {
						user.following.push({
							collectionName: follow.collectionName,
							collectionId: [follow.collectionId]
						});
					} else {
						user.following[index].collectionId.push(follow.collectionId);
					}
				} else {
					user.following[index].collectionId.remove(follow.collectionId);
				}
				update(user, function(response) {
					callback(response);
				});
			}
		}
	});
}

function userRating(rating, callback) {
	var collectionObj = {};
	var UserRating;
	if(rating.collectionName == "challenge") {
		UserRating = Challenge;
		collectionObj.challengeId =  rating.collectionId;
	} else if(rating.collectionName == "solution") {
		UserRating = Solution;
		collectionObj.solutionId =  rating.collectionId;
	} else if(rating.collectionName == "collaborate") {
		UserRating = Collaborate;
		collectionObj.collaborateId =  rating.collectionId;
	} else if(rating.collectionName == "research") {
		UserRating = Research;
		collectionObj.researchId =  rating.collectionId;
	} else if(rating.collectionName == "project") {
		UserRating = Project;
		collectionObj.projectId =  rating.collectionId;
	} else {
		UserRating = Challenge;
		collectionObj.challengeId =  rating.collectionId;
	}
	var query = {
		collectionName: rating.collectionName,
		collectionId: rating.collectionId,
		ratingById: rating.ratingById
	};
	Rating.getList(query, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var ratingRecord = response.result;
			if(ratingRecord.length < 1) {
				Rating.create(rating, function(response) {
					if(response.error) {
						callback(response);
					} else { 
						var ratingId = response.ratingId;
						UserRating.getDetails(rating.collectionId, function(response) {
							if(response.error) {
								callback(response);
							} else {
								var collectionRecord = response.result[0];
								var index = collectionRecord.rating.findIndex(x => x.rating == rating.rating);
								if(index < 0) {
									collectionRecord.rating.push({
										rating: rating.rating,
										ratingId: [ratingId],
										userId: [rating.ratingById]
									});
								} else {
									collectionRecord.rating[index].ratingId.push(ratingId);
									collectionRecord.rating[index].userId.push(rating.ratingById);
								}
								var avgRate = 0;
								var totalRating = 0;
								var avgRating = collectionRecord.rating.map(rate => {
									if(rate.rating == 5) {
										avgRate = avgRate + rate.ratingId.length * 5;
										totalRating = totalRating + rate.ratingId.length;
									} else if(rate.rating == 4) {
										avgRate = avgRate + rate.ratingId.length * 4;
										totalRating = totalRating + rate.ratingId.length;
									} else if(rate.rating == 3) {
										avgRate = avgRate + rate.ratingId.length * 3;
										totalRating = totalRating + rate.ratingId.length;
									} else if(rate.rating == 2) {
										avgRate = avgRate + rate.ratingId.length * 2;
										totalRating = totalRating + rate.ratingId.length;
									} else if(rate.rating == 1) {
										avgRate = avgRate + rate.ratingId.length * 1;
										totalRating = totalRating + rate.ratingId.length;
									} else {

									}
									return {totalRating: totalRating, avgRate: avgRate/totalRating};
								});
								var myRatingIndex = collectionRecord.myRating.findIndex(x => x.userId == rating.ratingById);
								if(myRatingIndex < 0) {
									collectionRecord.myRating.push({userId: rating.ratingById, rating: rating.rating});
									collectionObj.myRating = collectionRecord.myRating;
								
								} else {
									collectionRecord.myRating[myRatingIndex].rating = rating.rating
									collectionObj.myRating = collectionRecord.myRating;
								}
								collectionObj.avgRating = avgRating[avgRating.length - 1].avgRate;
								collectionObj.totalRating = avgRating[avgRating.length - 1].totalRating;
								collectionObj.rating = collectionRecord.rating;
								collectionObj.userId = rating.uId;
								collectionObj.userName = rating.userName;
								collectionObj.userEmail = rating.userEmail;
								collectionObj.url = rating.url;
								collectionObj.method = rating.method;
								collectionObj.apiRequest = {
									"module": UserRating+"-UpdateRating",
									"name": ''
								};
								collectionObj.apiResponse = "SUCCESS";
								UserRating.update(collectionObj, function(response) {
									if(response.error) {
										callback(response);
									} else {
										callback({
											status: REQUEST_CODES.SUCCESS,
											result: collectionObj
										});
									}
								})
							}
						});
					}
				});
			} else {
				var ratingObj = {
					ratingId: ratingRecord[0].ratingId,
					rating: rating.rating
				};
				Rating.update(ratingObj, function(response) {
					if(response.error) {
						callback(response);
					} else {
						var ratingId = ratingRecord[0].ratingId;
						UserRating.getDetails(rating.collectionId, function(response) {
							if(response.error) {
								callback(response);
							} else {
								var collectionRecord = response.result[0];
								var index = collectionRecord.rating.findIndex(x => x.rating == rating.rating);
								var index1 = collectionRecord.rating.findIndex(x => x.rating == ratingRecord[0].rating);
								if(index1 > -1) {
									collectionRecord.rating[index1].ratingId = _.filter(collectionRecord.rating[index1].ratingId, function(rate){ return rate != ratingId });
									collectionRecord.rating[index1].userId = _.filter(collectionRecord.rating[index1].ratingId, function(rate){ return rate != rating.ratingById });
									}
								if(index < 0) {
									collectionRecord.rating.push({
										rating: rating.rating,
										ratingId: [ratingId],
										userId: [rating.ratingById]
									});
								} else {
									collectionRecord.rating[index].ratingId.push(ratingId);
									collectionRecord.rating[index].userId.push(rating.ratingById);
								}
								var avgRate = 0;
								var totalRating = 0;
								var avgRating = collectionRecord.rating.map(rate => {
									if(rate.rating == 5) {
										avgRate = avgRate + rate.ratingId.length * 5;
										totalRating = totalRating + rate.ratingId.length;
									} else if(rate.rating == 4) {
										avgRate = avgRate + rate.ratingId.length * 4;
										totalRating = totalRating + rate.ratingId.length;
									} else if(rate.rating == 3) {
										avgRate = avgRate + rate.ratingId.length * 3;
										totalRating = totalRating + rate.ratingId.length;
									} else if(rate.rating == 2) {
										avgRate = avgRate + rate.ratingId.length * 2;
										totalRating = totalRating + rate.ratingId.length;
									} else if(rate.rating == 1) {
										avgRate = avgRate + rate.ratingId.length * 1;
										totalRating = totalRating + rate.ratingId.length;
									} else {

									}
									return {totalRating: totalRating, avgRate: avgRate/totalRating};
								});
								var myRatingIndex = collectionRecord.myRating.findIndex(x => x.userId == rating.ratingById);
								if(myRatingIndex < 0) {
									collectionRecord.myRating.push({userId: rating.ratingById, rating: rating.rating});
									collectionObj.myRating = collectionRecord.myRating;
								
								} else {
									collectionRecord.myRating[myRatingIndex].rating = rating.rating;
									collectionObj.myRating = collectionRecord.myRating;
								}
								collectionObj.avgRating = avgRating[avgRating.length - 1].avgRate;
								collectionObj.totalRating = avgRating[avgRating.length - 1].totalRating;
								collectionObj.rating = collectionRecord.rating;
								collectionObj.userId = rating.uId;
								collectionObj.userName = rating.userName;
								collectionObj.userEmail = rating.userEmail;
								collectionObj.url = rating.url;
								collectionObj.method = rating.method;
								collectionObj.apiRequest = {
									"module": UserRating+"-UpdateRating",
									"name": ''
								};
								collectionObj.apiResponse = "SUCCESS";
								UserRating.update(collectionObj, function(response) {
									if(response.error) {
										callback(response);
									} else {
										callback({
											status: REQUEST_CODES.SUCCESS,
											result: collectionObj
										});
									}
								})
							}
						});
					}
				});
			}
		}
	});
}
 
function iamFollowing(userId, callback) {
	getDetails(userId, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var userRecord = response.result;
			if(userRecord.length < 0) {
				callback({
					status: REQUEST_CODES.FAIL,
					error: "User no found"
				});
			} else {
				var result = {
					challenges: [],
					solutions: [],
					projects: [],
					collaborates: [],
					researchs: [],
					discussions: []
				};
				getChallenges(userRecord[0].following, function(response) {
					if(response.error) {
						callback(response);
					} else {
						result.challenges = response.result;
						getSolutions(userRecord[0].following, function(response) {
							if(response.error) {
								callback(response);
							} else {
								result.solutions = response.result;
								getProjects(userRecord[0].following, function(response) {
									if(response.error) {
										callback(response);
									} else {
										result.projects = response.result;
										getCollaborates(userRecord[0].following, function(response) {
											if(response.error) {
												callback(response);
											} else {
												result.collaborates = response.result;
												getResearchs(userRecord[0].following, function(response) {
													if(response.error) {
														callback(response);
													} else {
														result.researchs = response.result;
														getDiscussions(userRecord[0].following, function(response) {
															if(response.error) {
																callback(response);
															} else {
																result.discussions = response.result;
																callback({
																	status: REQUEST_CODES.SUCCESS,
																	result: result
																});
															}
														});
													}
												});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		}
	});
}

function getChallenges(challengeArray, callback) {
	var challengeIndex = challengeArray.findIndex(x => x.collectionName == "challenge");
	if(challengeIndex > -1) {
		var challengeIds = challengeArray[challengeIndex].collectionId;	
		Challenge.getList({challengeId: {$in: challengeIds}}, function(response) {
			if(response.error) {
				callback(response);
			} else {
				var challengeRecords = response.result;
				var challenge;
				if(challengeRecords.length > 0) {
					challengeRecords = challengeRecords.map(challenges => {
						challenge = {
							challengeId: challenges.challengeId,
							title: challenges.title,
							type: challenges.type,
							sector: challenges.sector,
							totalRating: challenges.totalRating,
							avgRating: challenges.avgRating,
							rating: challenges.rating,
							myRating: challenges.myRating
						};
						return challenge;
					});
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: challengeRecords
					});
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: []
					});
				}
			}
		});
	} else {
		callback({
			status: REQUEST_CODES.SUCCESS,
			result: []
		});
	}
}

function getSolutions(solutionArray, callback) {
	var solutionIndex = solutionArray.findIndex(x => x.collectionName == "solution");
	if(solutionIndex > -1) {
		var solutionIds = solutionArray[solutionIndex].collectionId;	
		Solution.getList({solutionId: {$in: solutionIds}}, function(response) {
			if(response.error) {
				callback(response);
			} else {
				var solutionRecords = response.result;
				var solution;
				if(solutionRecords.length > 0) {
					solutionRecords = solutionRecords.map(solutions => {
						solution = {
							solutionId: solutions.solutionId,
							name: solutions.name,
							affiliated: solutions.affiliated,
							sector: solutions.sector,
							logo: solutions.logo,
							totalRating: solutions.totalRating,
							avgRating: solutions.avgRating,
							rating: solutions.rating,
							myRating: solutions.myRating
						};
						return solution;
					});
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: solutionRecords
					});
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: []
					});
				}
			}
		});
	} else {
		callback({
			status: REQUEST_CODES.SUCCESS,
			result: []
		});
	}
}

function getProjects(projectArray, callback) {
	var projectIndex = projectArray.findIndex(x => x.collectionName == "project");
	if(projectIndex > -1) {
		var projectIds = projectArray[projectIndex].collectionId;	
		Project.getList({projectId: {$in: projectIds}}, function(response) {
			if(response.error) {
				callback(response);
			} else {
				var projectRecords = response.result;
				var project;
				if(projectRecords.length > 0) {
					projectRecords = projectRecords.map(projects => {
						project = {
							projectId: projects.projectId,
							name: projects.name,
							categories: projects.categories,
							sector: projects.sector,
							totalRating: projects.totalRating,
							avgRating: projects.avgRating,
							rating: projects.rating,
							myRating: projects.myRating
						};
						return project;
					});
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: projectRecords
					});
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: []
					});
				}
			}
		});
	} else {
		callback({
			status: REQUEST_CODES.SUCCESS,
			result: []
		});
	}
}

function getCollaborates(collaborateArray, callback) {
	var collaborateIndex = collaborateArray.findIndex(x => x.collectionName == "collaborate");
	if(collaborateIndex > -1) {
		var collaborateIds = collaborateArray[collaborateIndex].collectionId;	
		Collaborate.getList({collaborateId: {$in: collaborateIds}}, function(response) {
			if(response.error) {
				callback(response);
			} else {
				var collaborateRecords = response.result;
				var collaborate;
				if(collaborateRecords.length > 0) {
					collaborateRecords = collaborateRecords.map(collaborates => {
						collaborate = {
							collaborateId: collaborates.collaborateId,
							name: collaborates.name,
							type: collaborates.type,
							sector: collaborates.sector,
							logo: collaborates.logo,
							totalRating: collaborates.totalRating,
							avgRating: collaborates.avgRating,
							rating: collaborates.rating,
							myRating: collaborates.myRating
						};
						return collaborate;
					});
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: collaborateRecords
					});
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: []
					});
				}
			}
		});
	} else {
		callback({
			status: REQUEST_CODES.SUCCESS,
			result: []
		});
	}
}

function getResearchs(researchArray, callback) {
	var researchIndex = researchArray.findIndex(x => x.collectionName == "research");
	if(researchIndex > -1) {
		var researchIds = researchArray[researchIndex].collectionId;	
		Research.getList({researchId: {$in: researchIds}}, function(response) {
			if(response.error) {
				callback(response);
			} else {
				var researchRecords = response.result;
				var research;
				if(researchRecords.length > 0) {
					researchRecords = researchRecords.map(researchs => {
						research = {
							researchId: researchs.researchId,
							researchName: researchs.researchName,
							type: researchs.type,
							sector: researchs.sector,
							totalRating: researchs.totalRating,
							avgRating: researchs.avgRating,
							rating: researchs.rating,
							myRating: researchs.myRating
						};
						return research;
					});
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: researchRecords
					});
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: []
					});
				}
			}
		});
	} else {
		callback({
			status: REQUEST_CODES.SUCCESS,
			result: []
		});
	}
}

function getDiscussions(discussionArray, callback) {
	var discussionIndex = discussionArray.findIndex(x => x.collectionName == "discussion");
	if(discussionIndex > -1) {
		var discussionIds = discussionArray[discussionIndex].collectionId;	
		Discussion.getList({discussionId: {$in: discussionIds}}, function(response) {
			if(response.error) {
				callback(response);
			} else {
				var discussionRecords = response.result;
				var discussion;
				if(discussionRecords.length > 0) {
					discussionRecords = discussionRecords.map(discussions => {
						discussion = {
							discussionId: discussions.discussionId,
							discussionName: discussions.discussionName
							// type: discussions.type,
							// sector: discussions.sector,
							// totalRating: discussions.totalRating,
							// avgRating: discussions.avgRating,
							// rating: discussions.rating,
							// myRating: discussions.myRating
						};
						return discussion;
					});
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: discussionRecords
					});
				} else {
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: []
					});
				}
			}
		});
	} else {
		callback({
			status: REQUEST_CODES.SUCCESS,
			result: []
		});
	}
}

module.exports.create = create;
module.exports.getDetails = getDetails;
module.exports.getList = getList;
module.exports.update = update;
module.exports.userFollowing = userFollowing;
module.exports.iamFollowing = iamFollowing;
