module.exports.userSchema = { 
                               userId: {
                                    type: Number,
                                    unique: true,
                                    required: true,
                                    index: true
                                },
                                firstName: { 
                                    type: String,
                                    required: true
                                },
                                lastName: { 
                                    type: String,
                                    required: true
                                },
                                email: { 
                                    type: String,
                                    unique: true,
                                    required: true
                                },
                                password: {
                                    type: String,
                                    required: true
                                },
                                designation: {
                                    type: String
                                },
                                institution: {
                                    type: String
                                },
                                companyName: {
                                    type: String
                                },
                                about: {
                                    type: String
                                },
                                location: {
                                    type: String
                                },
                                following: [{
                                    collectionName: {type: String},
                                    collectionId: {type: [Number]}
                                }],
                                areaOfInterest: {
                                    type: [String]
                                },
                                profileId: {
                                    type: Number
                                },                               
                                resources: [{
                                    fileId: {type: [Number]},
                                    category: {type: String}
                                }],
                                gender: {
                                    type: String
                                },
                                type: {
                                    type: String
                                },
                                dob: {
                                    type: Number
                                },
                                logo: {
                                    type: String
                                },
                                website: {
                                    type: String
                                },
                                linkedin: {
                                    type: String
                                },
                                role: {
                                    type: Number
                                },
                                status: {
                                    type: String
                                },
                                state: {
                                    type: String,
                                    unique: false,
                                    required: false
                                },
                                isSuperUser: {
                                    type: Boolean
                                },
                                isAdmin: {
                                    type: Boolean
                                },
                                organisationId: {
                                    type: Number
                                },
                                phone: {
                                    type: Number
                                },
                                createdDate: { 
                                    type: Number,
                                    index: true
                                },
                                updatedDate: { 
                                    type: Number,
                                    index: true
                                }

};
