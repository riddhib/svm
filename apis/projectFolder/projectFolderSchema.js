module.exports.projectFolderSchema = { 
                                projectFolderId: {
                                    type: Number,
                                    unique: true,
                                    required: true,
                                    index: true
                                },
                                name: { 
                                    type: String
                                },
                                category: {
                                    type: String
                                },
                                users: [],
                                isActive: {
                                    type: Boolean
                                },
                                createdDate: { 
                                    type: Number
                                },
                                updatedDate: { 
                                    type: Number
                                }

};