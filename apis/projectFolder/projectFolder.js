module.exports = function(app){
	app.post('/ui/projectFolder',function(req, res){
        try{
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            create(req.body, function(response){
               res.json(response);
        	});
         
        }catch(e){
            res.json(e);
        }
    });
	app.put('/ui/projectFolder', function(req, res) {
		try {
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			update(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
    app.get('/ui/projectFolder/:projectFolderId', function(req, res) {
		try {
			getDetails(req.params.projectFolderId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/query/projectFolder', function(req, res) {
		try {
			getList(req.query, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});

	app.put('/ui/projectFolder/addUsers', function(req, res) {
		try {
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			addUser(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
}

var mongoose = require('mongoose')
	Schema = mongoose.Schema;
var ProjectFolderSchema = new Schema(require('./projectFolderSchema').projectFolderSchema, {collection: 'projectFolder'});
var ProjectFolderModel = mongoose.model('projectFolder', ProjectFolderSchema);
var ProjectFolderController = require('./projectFolderController');
var UserLog = require('../userLog/userLog');
var User = require('../user/user');
var utils = require('../../assets/utils').utils;
var MailHelper = require('../mailHelper/mailHelper');
var mongoUtils = utils.mongoUtils;
var CONSTANTS = utils.CONSTANTS;
var VALIDATE = CONSTANTS.VALIDATE;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var FOLDER_CODES = CONSTANTS.FOLDER_CODES;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var USER_CODES = CONSTANTS.USER_CODES;

function create(projectFolder, callback) {
	var projectFolderAPI = ProjectFolderController.ProjectFolderAPI(projectFolder);
    var errorList = [];
    if (!projectFolderAPI.getName()) {
       	var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'name')
		};
		errorList.push(e);
	}	
   	if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var projectFolderModel = new ProjectFolderModel(projectFolderAPI);
		projectFolderModel.isActive = true;
		mongoUtils.getNextSequence('projectFolderId', function(oSeq) {
			projectFolderModel.projectFolderId = oSeq;
			projectFolderModel.createdDate = new Date().getTime();
			projectFolderModel.save(function(error) {
				if (error) {
					callback({
						status: DB_CODES.FAIL,
						error: error
					});
					return;
				} else {
					let logObj = {
						"userId": projectFolder.userId,
						"userName": projectFolder.userName,
						"email": projectFolder.userEmail,
						"requestUrl": projectFolder.url,
						"requestMethod": projectFolder.method,
						"apiRequest": {
							"module": "ProjectFolder-Create",
							"name": projectFolderModel.name,
							"id": projectFolderModel.projectFolderId
						},
						"apiResponse": "SUCCESS"
					}
					UserLog.create(logObj, function(response){
						if(!response.error){
							callback({
								status: REQUEST_CODES.SUCCESS,
								result: utils.formatText(FOLDER_CODES.CREATE_SUCCESS, projectFolderModel.projectFolderId)
							});
							return;							
						}
					});
				}
			});
		});
	}
}

function getDetails(projectFolderId, callback) {
	ProjectFolderModel.find({"projectFolderId": projectFolderId}, function(error, projectFolderRecords) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			projectFolderRecords = projectFolderRecords.map(function(projectFolderRecord) {
				return new ProjectFolderController.ProjectFolderAPI(projectFolderRecord);
			});			
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: projectFolderRecords
			});
			return;		
		}
	});	
}

function getList(query, callback) {
	ProjectFolderModel.find(query, function(error, projectFolderRecords) {		
		if (error) {
			callback({
			    status: DB_CODES.FAIL,
			    error: error
			});
			return;
		} else {		
			projectFolderRecords = projectFolderRecords.map(function(projectFolderRecord) {
				return new ProjectFolderController.ProjectFolderAPI(projectFolderRecord);
			});
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: projectFolderRecords
			});
			return;	
		}
	}).sort({createdDate: -1});
}

function update(projectFolder, callback) {
	getDetails(projectFolder.projectFolderId, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result) {
				var folderDetails = response.result[0];
				projectFolder.updatedDate = new Date().getTime();
				ProjectFolderModel.updateOne({"projectFolderId": projectFolder.projectFolderId}, {$set: projectFolder}, function(error, effectedRows) {
					if (error) {
						callback({
							status: DB_CODES.FAIL,
							error: error
						});
						return;
					} else {
						if (!effectedRows.nModified) {
							callback({
								status: REQUEST_CODES.FAIL,
								error: utils.formatText(FOLDER_CODES.UPDATE_FAIL, projectFolder.projectFolderId)
							});
							return;
						} else {
							let logObj = {
								"userId": projectFolder.userId,
								"userName": projectFolder.userName,
								"email": projectFolder.userEmail,
								"requestUrl": projectFolder.url,
								"requestMethod": projectFolder.method,
								"apiRequest": {
									"module": "ProjectFolder-Update",
									"name": folderDetails.name,
									"id": folderDetails.projectFolderId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: utils.formatText(FOLDER_CODES.UPDATE_SUCCESS, projectFolder.projectFolderId)
									});
									return;
								}
							});
						}
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No ProjectFolder Found"
				});
				return;
			}
		}
	});	 
}

async function addUser(users, callback){
	let userIds = users.userIds.map(user=> parseInt(user)) || [],
		folderUsers = [];
		
	if (userIds.length > 0) {
		await new Promise(resolve => {
			User.getList({userId: {$in: userIds}}, function(response) {
				if (!response.error) {
					folderUsers = response.result.map(user => {
						return {
							"userId": user.userId,
							"userName": user.firstName+' '+user.lastName,
							"type": user.type
						};
					}) || [];
				}
				resolve();
			});
		});
		let query = {
			"projectFolderId": users.projectFolderId,
			"users": folderUsers,
			"userId": users.userId,
			"userName": users.userName,
			"userEmail": users.userEmail,
			"url": users.url,
			"method": users.method,
			"apiRequest": {
				"module": "ProjectFolder-Update",
				"name": '',
				"id": users.projectFolderId
			},
			"apiResponse": "SUCCESS"
		}
		update(query, function(response){
			if (response.error) {
				callback({
					status: REQUEST_CODES.FAIL,
					error: utils.formatText(FOLDER_CODES.UPDATE_FAIL, users.projectFolderId)
				});
				return;
			} else {
				callback({
					status: REQUEST_CODES.SUCCESS,
					result: [users.projectFolderId]
				});
				return;
			}
		});
	} else {
		callback({
			status: REQUEST_CODES.SUCCESS,
			response: "No users selected!"
		});
		return;
	}
}

module.exports.create = create;
module.exports.getDetails = getDetails;
module.exports.getList = getList;
module.exports.update = update;
module.exports.addUser = addUser;
