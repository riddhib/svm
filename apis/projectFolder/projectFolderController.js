var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var VALIDATE = utils.CONSTANTS.VALIDATE;
var validate = utils.validate;

var ProjectFolder = function() {
    return {
        projectFolderId: 0,
        name: null,
        users: [],
        category: null,
        isActive: true,
        createdDate: 0,
        updatedDate: 0
    };
};

function ProjectFolderAPI(projectFolderRecord) {
    var projectFolder = new ProjectFolder();
    projectFolder.getProjectFolderId = function() {
        return this.projectFolderId;
    };
    projectFolder.setProjectFolderId = function(projectFolderId) {
        if (projectFolderId) {
            if (validate.isInteger(projectFolderId + '')) {
                    this.projectFolderId = projectFolderId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, projectFolderId, 'projectFolderId')
                };
            }
        }
    };
    projectFolder.getName = function() {
        return this.name;
    };
    projectFolder.setName = function(name) {
         if (name) {
            if (name.length <= 250) {
                this.name = name;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, name, 'name')
                };
            }
        }
    };
    projectFolder.getCategory = function() {
        return this.category;
    };
    projectFolder.setCategory = function(category) {
        if (category) {
            this.category = category;
        }
    };
    projectFolder.getUsers = function() {
        return this.users;
    };
    projectFolder.setUsers = function(users) {
        if (users) {
            this.users = users;
        }
    };
    projectFolder.getIsActive = function() {
        return this.isActive;
    };
    projectFolder.setIsActive = function(isActive) {
        this.isActive = isActive;
    };
    projectFolder.getCreatedDate = function() {
        return this.createdDate;
    };
    projectFolder.setCreatedDate = function(createdDate) {
        if (createdDate) {
            if (validate.isInteger(createdDate + '')) {
                    this.createdDate = createdDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdDate, 'createdDate')
                };
            }
        }
    };
    projectFolder.getUpdatedDate = function() {
        return this.updatedDate;
    };
    projectFolder.setUpdatedDate = function(updatedDate) {
        if (updatedDate) {
            if (validate.isInteger(updatedDate + '')) {
                    this.updatedDate = updatedDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, updatedDate, 'updatedDate')
                };
            }
        }
    };
   
    if (projectFolderRecord) {
        var errorList = [];
        try {
            projectFolder.setProjectFolderId(projectFolderRecord.projectFolderId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            projectFolder.setName(projectFolderRecord.name);
        } catch (e) {
            errorList.push(e);
        }
        try {
            projectFolder.setUsers(projectFolderRecord.users);
        } catch (e) {
            errorList.push(e);
        }
        try {
            projectFolder.setCategory(projectFolderRecord.category);
        } catch (e) {
            errorList.push(e);
        }
        try {
            projectFolder.setCreatedDate(projectFolderRecord.createdDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            projectFolder.setUpdatedDate(projectFolderRecord.updatedDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            projectFolder.setIsActive(projectFolderRecord.isActive);
        } catch (e) {
            errorList.push(e);
        }
        
        if (errorList.length) {
            throw {
                status: REQUEST_CODES.FAIL,
                error: errorList
            };
        }
    }
    return projectFolder;
}

module.exports.ProjectFolderAPI = ProjectFolderAPI;