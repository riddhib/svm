module.exports.discussionSchema = {
                                    discussionId: {
                                          type: Number,
                                          unique: true,
                                          required: true,
                                          index: true
                                    },
                                    discussionName: {
                                          type: String,
                                          required: true,
                                    },
                                    body: {
                                          type: String
                                    },
                                    category: {
                                          type: String
                                    },
                                    parentId: {
                                          type: Number
                                    },
                                    discussionThreadId: {
                                          type: Number
                                    },
                                    location: {
                                          type: String
                                    },
                                    keyword: {
                                          type: String
                                    },
                                    participantIds: {
                                          type: [],
                                    },
                                    status: {
                                          type: String
                                    },
                                    following: [{
                                        userId: {type: Number},
                                        userName: {type: String}
                                    }],
                                    isActive: {
                                          type: Boolean
                                    },
                                    createdBy: {
                                          type: Number
                                    },
                                    createdDate: {
                                          type: Number,
                                          index: true
                                    },
                                    updatedDate: {
                                          type: Number,
                                          index: true
                                    }
} 