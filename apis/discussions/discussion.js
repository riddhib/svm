module.exports = function(app){
	app.post('/ui/discussion',function(req, res){
        try {
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            create(req.body, function(response){
               res.json(response);
        	});
         
        } catch(e){
            res.json(e);
        }
    });
	app.put('/ui/discussion', function(req, res) {
		try {
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			update(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
	app.put('/ui/discussion/discussionStatus', function(req, res) {
		try {
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			updateDiscussionStatus(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
    app.get('/ui/discussion/:discussionId', function(req, res) {
		try {
			getDetails(req.params.discussionId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/getSubCategories/:category', function(req, res) {
		try {
			getSubCategories(req.params.category, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/query/discussion', function(req, res) {
		try {
			getList(req.query, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/category/discussionList/:userId', function(req, res) {
		try {
			getCategoryDiscussionList(req.query, req.params.userId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	    
}

var mongoose = require('mongoose')
	Schema = mongoose.Schema;
var DiscussionSchema = new Schema(require('./discussionSchema').discussionSchema, {collection: 'discussion'});
var DiscussionModel = mongoose.model('discussion', DiscussionSchema);
var DiscussionController = require('./discussionController');
var utils = require('../../assets/utils').utils;
var Challenge = require('../challenge/challenge');
var UserLog = require('../userLog/userLog');
var Research = require('../research/research');
var Project = require('../project/project');
var mongoUtils = utils.mongoUtils;
var CONSTANTS = utils.CONSTANTS;
var VALIDATE = CONSTANTS.VALIDATE;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var DISCUSSION_CODES = CONSTANTS.DISCUSSION_CODES;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var _ = require('lodash');

function create(discussion, callback) {
	var discussionAPI = DiscussionController.DiscussionAPI(discussion);
    var errorList = [];
    if (!discussionAPI.getDiscussionName()) {
       	var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'Discussion Name')
		};
		errorList.push(e);
	}
    
   	if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var discussionModel = new DiscussionModel(discussionAPI);
		discussionModel.isActive = true;
		mongoUtils.getNextSequence('discussionId', function(oSeq) {
			discussionModel.discussionId = oSeq;
			discussionModel.status = "Active";
			discussionModel.createdDate = new Date().getTime();
			discussionModel.save(function(error) {
				if (error) {
					callback({
						status: DB_CODES.FAIL,
						error: error
					});
					return;
				} else {
					if(discussion.apiRequest) {
						var discussionLogName = discussion.apiRequest.name
					} else {
						var discussionLogName = discussion.discussionName
					}
					let logObj = {
						"userId": discussion.userId,
						"userName": discussion.userName,
						"email": discussion.userEmail,
						"requestUrl": discussion.url,
						"requestMethod": discussion.method,
						"apiRequest": {
							"module": "Discussion-Create",
							"name": discussionLogName,
							"id": discussionModel.discussionId
						},
						"apiResponse": "SUCCESS"
					}
					UserLog.create(logObj, function(response){
						if(!response.error){
							callback({
								status: REQUEST_CODES.SUCCESS,
								result: [discussionModel]
							});
							return;	
						}
					});						
				}
			});
		});
	}
}

function getDetails(discussionId, callback) {
	DiscussionModel.find({"discussionId": discussionId}, function(error, discussionRecords) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			discussionRecords = discussionRecords.map(function(discussionRecord) {
				return new DiscussionController.DiscussionAPI(discussionRecord);
			});			
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: discussionRecords
			});
			return;		
		}
	});	
}

function getList(query, callback) {
	DiscussionModel.find(query, function(error, discussionRecords) {		
		if (error) {
			callback({
			    status: DB_CODES.FAIL,
			    error: error
			});
			return;
		} else {					
			discussionRecords.map(function(discussionRecord) {
				return new DiscussionController.DiscussionAPI(discussionRecord);
			});
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: discussionRecords
			});
			return;	
		}
	}).sort({createdDate: -1});
}

function getCategoryDiscussionList(query, userId, callback) {
	getList(query, function(response) {
		if (response.error) {
			callback(response);
		} else {
			let discussions = JSON.parse(JSON.stringify(response.result));
			discussions = _.groupBy(discussions, 'category');
			Object.keys(discussions).forEach(key => {
				discussions[key] = discussions[key].map(discussion => {
					discussion.isFollowing = discussion.following ? (discussion.following.some(follow => follow.userId == userId) || false) : false;
					return discussion;
				});
			});
			delete discussions['null'];
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: discussions
			});	
		}
	});
}

function update(discussion, callback) {
	getDetails(discussion.discussionId, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result) {
				discussion.updatedDate = new Date().getTime();
				DiscussionModel.update({"discussionId": discussion.discussionId}, {$set: discussion}, function(error, effectedRows) {
					if (error) {
						callback({
							status: DB_CODES.FAIL,
							error: error
						});
						return;
					} else {
						if (!effectedRows.nModified) {
							callback({
								status: REQUEST_CODES.FAIL,
								error: utils.formatText(DISCUSSION_CODES.UPDATE_FAIL, discussion.discussionId)
							});
							return;
						} else {
							let logObj = {
								"userId": discussion.userId,
								"userName": discussion.userName,
								"email": discussion.userEmail,
								"requestUrl": discussion.url,
								"requestMethod": discussion.method,
								"apiRequest": {
									"module": "Discussion-Create",
									"name": discussion.title,
									"id": discussion.discussionId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: utils.formatText(DISCUSSION_CODES.UPDATE_SUCCESS, discussion.discussionId)
									});
									return;
								}
							});
						}
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No Discussion Found"
				});
				return;
			}
		}
	});	 
}

function updateDiscussionStatus(discussions, callback) {
	DiscussionModel.updateMany({"discussionId": { $in: discussions.discussionIds}}, {$set: {"isActive": discussions.isActive}}, function(error, effectedRows) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			if (!effectedRows.nModified) {
				callback({
					status: REQUEST_CODES.FAIL,
					error: error
				});
				return;
			} else {
				let logObj = {
					"userId": discussions.userId,
					"userName": discussions.userName,
					"email": discussions.userEmail,
					"requestUrl": discussions.url,
					"requestMethod": discussions.method,
					"apiRequest": {
						"module": "Discussion-Update",
						"name": '',
						"id": discussions.discussionIds,
					},
					"apiResponse": "SUCCESS"
				}
				UserLog.create(logObj, function(response){
					if(!response.error){
						callback({
							status: REQUEST_CODES.SUCCESS,
							result: "Success"
						});
						return;
					}
				});
			}
		}
	});
}

function getSubCategories(category, callback) {
	var projection;
	var SubCategory;
	if(category == "Challenges") {
		projection = {_id: 0, challengeId: 1, title: 1};
		Challenge.getChallenges({}, projection, function(response) {
			if(response.error) {
				callback(response);
			} else {
				callback(response);
			}
		});
	} else if(category == "Research") {
		projection = {_id: 0, researchId: 1, researchName: 1};
		Research.getResearches({}, projection, function(response) {
			if(response.error) {
				callback(response);
			} else {
				callback(response);
			}
		});
	} else if(category == "Projects") {
		projection = {_id: 0, projectId: 1, name: 1};
		Project.getProjects({}, projection, function(response) {
			if(response.error) {
				callback(response);
			} else {
				callback(response);
			}
		});
	} else {
		callback({
			status: REQUEST_CODES.SUCCESS,
			result: []
		})
	}
}

module.exports.create = create;
module.exports.getDetails = getDetails;
module.exports.getList = getList;
module.exports.update = update;
module.exports.updateDiscussionStatus = updateDiscussionStatus;