var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var VALIDATE = utils.CONSTANTS.VALIDATE;
var validate = utils.validate;

var Discussion = function() {
    return {
        discussionId : 0,
        discussionName : null,
        body: null,
        category: null,
        parentId : 0,
        discussionThreadId: 0,
        location: null,
        keyword: null,
        participantIds : [],
		status : null,
        following: [],
        isActive: true,
		createdBy : 0,
        createdDate : 0,
        updatedDate: 0
   }
};

function DiscussionAPI(discussionRecord) {
    var discussion = new Discussion();

    discussion.getDiscussionId = function() {
        return this.discussionId;
    };
    discussion.setDiscussionId = function(discussionId) {
        if (discussionId) {
            if (validate.isInteger(discussionId + '')) {
                this.discussionId = discussionId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, discussionId, 'discussionId')
                };
            }
        }
    };
    discussion.getDiscussionName = function() {
        return this.discussionName;
    };
    discussion.setDiscussionName = function(discussionName) {       
        if (discussionName) {
            if (discussionName.length <= 250) {
                this.discussionName = discussionName;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, discussionName, 'discussionName')
                };
            }
        }
    };   
    discussion.getDiscussionThreadId = function() {
        return this.discussionThreadId;
    };
    discussion.setDiscussionThreadId = function(discussionThreadId) {
        if (discussionThreadId) {
            if (validate.isInteger(discussionThreadId + '')) {
                this.discussionThreadId = discussionThreadId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, discussionThreadId, 'discussionThreadId')
                };
            }
        }
    };
    discussion.getBody = function() {
        return this.body;
    }
    discussion.setBody = function(body) {
        if (body) {
            this.body = body;
        }
    }
    discussion.getCategory = function() {
        return this.category;
    }
    discussion.setCategory = function(category) {
        if (category) {
            this.category = category;
        }
    }
    discussion.getParentId = function() {
        return this.parentId;
    };
    discussion.setParentId = function(parentId) {
        if (parentId) {
            if (validate.isInteger(parentId + '')) {
                this.parentId = parentId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, parentId, 'parentId')
                };
            }
        }
    };
    discussion.getLocation = function() {
        return this.location;
    }
    discussion.setLocation = function(location) {
        if (location) {
            this.location = location;
        }
    }
    discussion.getIsActive = function() {
        return this.isActive;
    }
    discussion.setIsActive = function(isActive) {
        this.isActive = isActive;
    }
    discussion.getKeyword = function() {
        return this.keyword;
    }
    discussion.setKeyword = function(keyword) {
        if (keyword) {
            this.keyword = keyword;
        }
    }
    discussion.getParticipantIds = function() {
        return this.participantIds;
    }
    discussion.setParticipantIds = function(participantIds) {
        if (participantIds) {
            this.participantIds = participantIds;
        }
    }
    discussion.getStatus = function() {
        return this.status;
    }
    discussion.setStatus = function(status) {
        if (status) {
            this.status = status;
        }
    }
    discussion.getFollowing = function() {
        return this.following;
    }
    discussion.setFollowing = function(following) {
        if (following) {
            this.following = following;
        }
    }
    discussion.getCreatedBy = function() {
        return this.createdBy;
    };
    discussion.setCreatedBy = function(createdBy) {
         if (createdBy) {
            if (validate.isInteger(createdBy + '')) {
                this.createdBy = createdBy;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdBy, 'createdBy')
                };
            }
        }
    };
    discussion.getCreatedDate = function() {
        return this.createdDate;
    };
    discussion.setCreatedDate = function(createdDate) {
        if (createdDate) {
            if (validate.isInteger(createdDate + '')) {
                this.createdDate = createdDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdDate, 'createdDate')
                };
            }
        }
    };
    discussion.getUpdatedDate = function() {
        return this.updatedDate;
    };
    discussion.setUpdatedDate = function(updatedDate) {
        if (updatedDate) {
            if (validate.isInteger(updatedDate + '')) {
                this.updatedDate = updatedDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, updatedDate, 'updatedDate')
                };
            }
        }
    };
    if (discussionRecord) {
        var errorList = [];
		try {
            discussion.setDiscussionId(discussionRecord.discussionId);
        } catch (e) {
			errorList.push(e);
        }
        try {
            discussion.setDiscussionName(discussionRecord.discussionName);
        } catch (e) {
			 errorList.push(e);
        }
        try {
            discussion.setDiscussionThreadId(discussionRecord.discussionThreadId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussion.setBody(discussionRecord.body);
        } catch (e) {
			 errorList.push(e);
        }
        try {
            discussion.setCategory(discussionRecord.category);
        } catch (e) {
			 errorList.push(e);
        }
        try {
            discussion.setParentId(discussionRecord.parentId);            
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussion.setLocation(discussionRecord.location);            
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussion.setKeyword(discussionRecord.keyword);            
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussion.setParticipantIds(discussionRecord.participantIds);
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussion.setStatus(discussionRecord.status);            
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussion.setFollowing(discussionRecord.following);
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussion.setCreatedBy(discussionRecord.createdBy);
        } catch (e) {
            errorList.push(e);
        }
        try {
            discussion.setCreatedDate(discussionRecord.createdDate);
        } catch (e) {
           errorList.push(e);
        }
        try {
            discussion.setIsActive(discussionRecord.isActive);
        } catch (e) {
           errorList.push(e);
        }
        if (errorList.length) {
            throw {
                status: REQUEST_CODES.FAIL,
                error: errorList
            };
        }
    }
	return discussion;
}
module.exports.DiscussionAPI = DiscussionAPI;