module.exports = function(app){
	app.post('/ui/challenge',function(req, res){
        try{
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            create(req.body, function(response){
               res.json(response);
        	});
        } catch(e){
            res.json(e);
        }
    });
	app.put('/ui/challenge', function(req, res) {
		try {
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			update(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
    app.get('/ui/challenge/:challengeId', function(req, res) {
		try {
			getDetails(req.params.challengeId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/editChallenge/:challengeId', function(req, res) {
		try {
			editChallenge(req.params.challengeId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/getDocuments/:challengeId/:name', function(req, res) {
		try {
			getDocuments(req.params.challengeId, req.params.name, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/query/challenge', function(req, res) {
		try {
			getList(req.query, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.post('/ui/uploadProposal',function(req, res) {
        try {
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            uploadProposal(req.body, function(response) {
        	    res.json(response);
            });
        } catch(e){
            res.json(e);
        }
	});
	app.post('/ui/challenge/uploadResources',function(req, res) {
        try {
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            uploadResources(req.body, function(response) {
        	    res.json(response);
            });
        } catch(e){
            res.json(e);
        }
    });
}

var mongoose = require('mongoose')
	Schema = mongoose.Schema;
var ChallengeSchema = new Schema(require('./challengeSchema').challengeSchema, {collection: 'challenge'});
var ChallengeModel = mongoose.model('challenge', ChallengeSchema);
var ChallengeController = require('./challengeController');
var Discussion = require('../discussions/discussion');
var UserLog = require('../userLog/userLog');
var utils = require('../../assets/utils').utils;
var File = require('../file/file');
var mongoUtils = utils.mongoUtils;
var mongoUtilsCounter = utils.mongoUtilsCounter;
var CONSTANTS = utils.CONSTANTS;
var VALIDATE = CONSTANTS.VALIDATE;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var CHALLENGE_CODES = CONSTANTS.CHALLENGE_CODES;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var _ = require('underscore');


function create(challenge, callback) {
	var challengeAPI = ChallengeController.ChallengeAPI(challenge);
	var errorList = [];
    if (!challengeAPI.getTitle()) {
       	var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'title')
		};
		errorList.push(e);
	}    
   	if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var challengeModel = new ChallengeModel(challengeAPI);
		challengeModel.isActive = true;
		mongoUtils.getNextSequence('challengeId', function(oSeq) {
			challengeModel.challengeId = oSeq;
			challengeModel.createdDate = new Date().getTime();	
			var newDiscussion = {
				"discussionName": challengeModel.title,
				"body": challengeModel.description || "",
				"category": "Challenges",
				"parentId": challengeModel.challengeId,
				"userId": challenge.userId,
				"userName": challenge.userName,
				"userEmail": challenge.userEmail,
				"url": challenge.url,
				"method": challenge.method,
                "isGovernmentFunded": challenge.isGovernmentFunded,
				"apiRequest": {
					"module": "Discussions-Create",
					"name": challenge.title,
					"id": challengeModel.challengeId
				},
				"apiResponse": "SUCCESS"
			};
			Discussion.create(newDiscussion, function(response){
				if(response.error){
					callback({
						status: REQUEST_CODES.ERROR,
						error: "Failed to create Challenge"
					});
				} else {
					var discussionId = response.result[0].discussionId;
					challengeModel.discussions.push(discussionId);
					challengeModel.save(function(error) {
						if (error) {
							callback({
								status: DB_CODES.FAIL,
								error: error
							});
							return;
						} else {
							let logObj = {
								"userId": challenge.userId,
								"userName": challenge.userName,
								"email": challenge.userEmail,
								"requestUrl": challenge.url,
								"requestMethod": challenge.method,
								"apiRequest": {
									"module": "Challenges-Create",
									"name": challenge.title,
									"id": challengeModel.challengeId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: utils.formatText(CHALLENGE_CODES.CREATE_SUCCESS, challengeModel.challengeId)
									});
									return;
								}
							});
						}
					});
				}
			});							
		});
	}
}

function getDetails(challengeId, callback) {
	ChallengeModel.find({"challengeId": challengeId}, function(error, challengeRecords) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			challengeRecords = challengeRecords.map(function(challengeRecord) {
				return new ChallengeController.ChallengeAPI(challengeRecord);
			});			
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: challengeRecords
			});
			return;		
		}
	});	
}

function getList(query, callback) {
	ChallengeModel.find(query, {generalDetails: 0, painPoints: 0, impact: 0}, function(error, challengeRecords) {	
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			challengeRecords = challengeRecords.map(function(challengeRecord) {
				return new ChallengeController.ChallengeAPI(challengeRecord);
			});
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: challengeRecords
			});
			return;		
		}
	}).sort({createdDate: -1});
}

function getChallenges(query, projection, callback) {
	ChallengeModel.find(query, projection, function(error, challengeRecords) {	
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: challengeRecords
			});
			return;		
		}
	});
}

function editChallenge(challengeId, callback) {
	getDetails(challengeId, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var challengeRecord = response.result[0];
			File.getList({fileId: {$in: challengeRecord.resources}}, function(response) {
				if(response.error) {
					callback(response);
				} else {
					var fileRecords = response.result;
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: {challenge: challengeRecord, files: fileRecords}
					});
				}
			});
		}
	});
}

function update(challenge, callback) {
	getDetails(challenge.challengeId, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result) {
				challengeDetails = response.result[0];
				challenge.updatedDate = new Date().getTime();
				ChallengeModel.updateOne({"challengeId": challenge.challengeId}, {$set: challenge}, function(error, effectedRows) {
					if (error) {
						callback({
							status: DB_CODES.FAIL,
							error: error
						});
						return;
					} else {
						if (!effectedRows.nModified) {
							callback({
								status: REQUEST_CODES.FAIL,
								error: utils.formatText(CHALLENGE_CODES.UPDATE_FAIL, challenge.challengeId)
							});
							return;
						} else {
							let logObj = {
								"userId": challenge.userId,
								"userName": challenge.userName,
								"email": challenge.userEmail,
								"requestUrl": challenge.url,
								"requestMethod": challenge.method,
								"apiRequest": {
									"module": "Challenges-Update",
									"name": challengeDetails.title,
									"id": challenge.challengeId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: utils.formatText(CHALLENGE_CODES.UPDATE_SUCCESS, challenge.challengeId)
									});
									return;
								}
							});
						}
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No Challenge Found"
				});
				return;
			}
		}
	});	 
}

function uploadProposal(file, callback) {
	File.create({fileName: file.advanceDetails.fileName, description: file.advanceDetails.description, uploadBy: file.advanceDetails.uploadBy, originalName: file.file.originalname, filePath: '/uploads/proposals/'+file.file.filename, userId: file.advanceDetails.userId, category: "proposals", mimeType: file.file.mimetype}, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var fileResponse = response.file;
			getDetails(file.advanceDetails.challengeId, function(response) {
				if(response.error) {
					callback(response);
				} else {
					var challengeRecord = response.result[0];
					var proposal = challengeRecord.proposal;
					proposal.push(fileResponse.fileId);
					var challenge = {
						"challengeId": file.advanceDetails.challengeId,
						"proposal": proposal,
						"userId": file.userId,
						"userName": file.userName,
						"userEmail": file.userEmail,
						"url": file.url,
						"method": file.method,
						"apiRequest": {
							"module": "Challenges-Update",
							"name": challengeRecord.title,
							"id": file.advanceDetails.challengeId
						},
						"apiResponse": "SUCCESS"
					}

					update(challenge, function(response){
						if(response.error){
							callback(response);
						} else {
							let logObj = {
								"userId": file.userId,
								"userName": file.userName,
								"email": file.userEmail,
								"requestUrl": file.url,
								"requestMethod": file.method,
								"apiRequest": {
									"module": "Challenges-Proposal",
									"name": challengeRecord.title,
									"id": file.advanceDetails.challengeId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: "Proposal uploaded"
									});
									return;
								}
							});
						}
					});
				}
			});
		}
	});
}

function uploadResources(file, callback) {
	File.create({fileName: file.advanceDetails.fileName, description: file.advanceDetails.description, uploadBy: file.advanceDetails.uploadBy, originalName: file.file.originalname, filePath: '/uploads/resources/'+file.file.filename, userId: file.advanceDetails.userId, category: "resources", mimeType: file.file.mimetype}, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var fileResponse = response.file;
			getDetails(file.advanceDetails.challengeId, function(response) {
				if(response.error) {
					callback(response);
				} else {
					var challengeRecord = response.result[0];
					var resources = challengeRecord.resources;
					resources.push(fileResponse.fileId);
					var challenge = {
						"challengeId": file.advanceDetails.challengeId,
						"resources": resources,
						"userId": file.userId,
						"userName": file.userName,
						"userEmail": file.userEmail,
						"url": file.url,
						"method": file.method,
						"apiRequest": {
							"module": "Challenges-Update",
							"name": challengeRecord.title,
							"id": file.advanceDetails.challengeId
						},
						"apiResponse": "SUCCESS"
					}
					update(challenge, function(response){
						if(response.error){
							callback(response);
						} else {
							let logObj = {
								"userId": file.userId,
								"userName": file.userName,
								"email": file.userEmail,
								"requestUrl": file.url,
								"requestMethod": file.method,
								"apiRequest": {
									"module": "Challenges-Resources",
									"name": challengeRecord.title,
									"id": file.advanceDetails.challengeId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: "Challenge Resource uploaded"
									});
									return;
								}
							});
						}
					});
				}
			});
		}
	});
}

function getDocuments(challengeId, name, callback) {
	getDetails(challengeId, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var challengeRecord = response.result[0];
			var fileIds = [];
			if(name == "resources") {
				fileIds = challengeRecord.resources;
			} else {
				fileIds = challengeRecord.proposal;
			}
			File.getList({fileId: {$in: fileIds}}, function(response) {
				if(response.error) {
					callback(response);
				} else {
					var fileRecords = response.result;
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: fileRecords
					});
				}
			})
		}
	});
}

module.exports.create = create;
module.exports.getDetails = getDetails;
module.exports.getList = getList;
module.exports.update = update;
module.exports.getChallenges = getChallenges;
