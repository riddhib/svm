var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var VALIDATE = utils.CONSTANTS.VALIDATE;
var validate = utils.validate;

var Challenge = function() {
    return {
        challengeId: 0,
        title: null,
        type: null,
        sector: null,
        description: null,
        generalDetails: null,
        logo: null,
        lattitude: null,
        longitude: null,
        location: null,
        impact: null,
        proposal: [],
        avgRating: 0,
        totalRating: 0,
        myRating: [],
        rating: [],
        following: [],
        discussions: [],
        name: null,
        poulation: null,
        distictHeadquarters: null,
        MajorLivelihoodOpportunities: null,
        pictures: null, 
        keyword: [],
        painPoints: null,    
        resources: [],
        feedback: null,
        startDate: 0,
        endDate: 0,
        lastDateForSubmissions: 0,
        isActive: true,
        //I made this change
        isGovernmentFunded: false,
        createdDate: 0,
        updatedDate: 0
    };
};

function ChallengeAPI(challengeRecord) {
    var challenge = new Challenge();
    challenge.getChallengeId = function() {
        return this.challengeId;
    };
    challenge.setChallengeId = function(challengeId) {
        if (challengeId) {
            if (validate.isInteger(challengeId + '')) {
                    this.challengeId = challengeId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, challengeId, 'challengeId')
                };
            }
        }
    };
    challenge.getTitle = function() {
        return this.title;
    };
    challenge.setTitle = function(title) {
         if (title) {
            if (title.length <= 250) {
                this.title = title;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, title, 'title')
                };
            }
        }
    };
    challenge.getType = function() {
        return this.type;
    };
    challenge.setType = function(type) {
         if (type) {
            if (type.length <= 50) {
                this.type = type;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, type, 'type')
                };
            }
        }
    };
    challenge.getDescription = function() {
        return this.description;
    };
    challenge.setDescription = function(description) {
        if (description) {
            this.description = description;
        }
    };
    challenge.getGeneralDetails = function() {
        return this.generalDetails;
    };
    challenge.setGeneralDetails = function(generalDetails) {
        if (generalDetails) {
            this.generalDetails = generalDetails;
        }
    };
    challenge.getDiscussions = function() {
        return this.discussions;
    };
    challenge.setDiscussions = function(discussions) {
        if (discussions) {
            this.discussions = discussions;
        }
    };
    challenge.getSector = function() {
        return this.sector;
    };
    challenge.setSector = function(sector) {
        if (sector) {
            this.sector = sector;
        }
    };
    challenge.getLogo = function() {
        return this.logo;
    };
    challenge.setLogo = function(logo) {
        if (logo) {
            this.logo = logo;
        }
    };
    challenge.getLattitude = function() {
        return this.lattitude;
    };
    challenge.setLattitude = function(lattitude) {
        if (lattitude) {
            this.lattitude = lattitude;
        }
    };
    challenge.getLongitude = function() {
        return this.longitude;
    };
    challenge.setLongitude = function(longitude) {
        if (longitude) {
            this.longitude = longitude;
        }
    };
    challenge.getLocation = function() {
        return this.location;
    };
    challenge.setLocation = function(location) {
        if (location) {
            this.location = location;
        }
    };
    challenge.getImpact = function() {
        return this.impact;
    };
    challenge.setImpact = function(impact) {
        if (impact) {
            this.impact = impact;
        }
    };
    challenge.getProposal = function() {
        return this.proposal;
    };
    challenge.setProposal = function(proposal) {
        if (proposal) {
            this.proposal = proposal;
        }
    };
    challenge.getRating = function() {
        return this.rating;
    };
    challenge.setRating = function(rating) {
        if (rating) {
            this.rating = rating;
        }
    };
    challenge.getAvgRating = function() {
        return this.avgRating;
    };
    challenge.setAvgRating = function(avgRating) {
        if (avgRating) {
            this.avgRating = avgRating;
        }
    };
    challenge.getTotalRating = function() {
        return this.totalRating;
    };
    challenge.setTotalRating = function(totalRating) {
        if (totalRating) {
            this.totalRating = totalRating;
        }
    };
    challenge.getMyRating = function() {
        return this.myRating;
    };
    challenge.setMyRating = function(myRating) {
        if (myRating) {
            this.myRating = myRating;
        }
    };
    challenge.getFollowing = function() {
        return this.following;
    };
    challenge.setFollowing = function(following) {
        if (following) {
            this.following = following;
        }
    };
    challenge.getName = function() {
        return this.name;
    };
    challenge.setName = function(name) {
        if (name) {
            this.name = name;
        }
    };
    challenge.getPoulation = function() {
        return this.poulation;
    };
    challenge.setPoulation = function(poulation) {
        if (poulation) {
            this.poulation = poulation;
        }
    };
    challenge.getDistictHeadquarters = function() {
        return this.distictHeadquarters;
    };
    challenge.setDistictHeadquarters = function(distictHeadquarters) {
        if (distictHeadquarters) {
            this.distictHeadquarters = distictHeadquarters;
        }
    };
    challenge.getMajorLivelihoodOpportunities = function() {
        return this.majorLivelihoodOpportunities;
    };
    challenge.setMajorLivelihoodOpportunities = function(majorLivelihoodOpportunities) {
        if (majorLivelihoodOpportunities) {
            this.majorLivelihoodOpportunities = majorLivelihoodOpportunities;
        }
    };
    challenge.getPictures = function() {
        return this.pictures;
    };
    challenge.setPictures = function(pictures) {
        if (pictures) {
            this.pictures = pictures;
        }
    };
    challenge.getKeyword = function() {
        return this.keyword;
    };
    challenge.setKeyword = function(keyword) {
        if (keyword) {
            if (keyword.length <= 100) {
                this.keyword = keyword;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, keyword, 'keyword')
                };
            }
        }
    };
    challenge.getPainPoints = function() {
        return this.painPoints;
    };
    challenge.setPainPoints = function(painPoints) {
        if (painPoints) {
            this.painPoints = painPoints;
        }
    };
    challenge.getResources = function() {
        return this.resources;
    };
    challenge.setResources = function(resources) {
        if (resources) {
            this.resources = resources;
        }
    };
    challenge.getFeedback = function() {
        return this.feedback;
    };
    challenge.setFeedback = function(feedback) {
        if (feedback) {
            this.feedback = feedback;
        }
    };
    challenge.getStartDate = function() {
        return this.startDate;
    };
    challenge.setStartDate = function(startDate) {
        if (startDate) {
            if (validate.isInteger(startDate + '')) {
                    this.startDate = startDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, startDate, 'startDate')
                };
            }
        }
    };
    challenge.getEndDate = function() {
        return this.endDate;
    };
    challenge.setEndDate = function(endDate) {
        if (endDate) {
            if (validate.isInteger(endDate + '')) {
                    this.endDate = endDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, endDate, 'endDate')
                };
            }
        }
    };
    challenge.getLastDateForSubmissions = function() {
        return this.lastDateForSubmissions;
    };
    challenge.setLastDateForSubmissions = function(lastDateForSubmissions) {
        if (lastDateForSubmissions) {
            if (validate.isInteger(lastDateForSubmissions + '')) {
                    this.lastDateForSubmissions = lastDateForSubmissions;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, lastDateForSubmissions, 'lastDateForSubmissions')
                };
            }
        }
    };
    challenge.getCreatedDate = function() {
        return this.createdDate;
    };
    challenge.setCreatedDate = function(createdDate) {
        if (createdDate) {
            if (validate.isInteger(createdDate + '')) {
                    this.createdDate = createdDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdDate, 'createdDate')
                };
            }
        }
    };
    challenge.getUpdatedDate = function() {
        return this.updatedDate;
    };
    challenge.setUpdatedDate = function(updatedDate) {
        if (updatedDate) {
            if (validate.isInteger(updatedDate + '')) {
                    this.updatedDate = updatedDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, updatedDate, 'updatedDate')
                };
            }
        }
    };

    challenge.getIsActive = function() {
        return this.isActive;
    };
    challenge.setIsActive = function(isActive) {
        this.isActive = isActive;
    };

    //I made this change
    challenge.getIsGovernmentFunded = function() {
        return this.isGovernmentFunded;
    };
    challenge.setIsGovernmentFunded = function(isGovernmentFunded) {
        this.isGovernmentFunded = isGovernmentFunded;
    };
   
    if (challengeRecord) {
        var errorList = [];
        try {
            challenge.setChallengeId(challengeRecord.challengeId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setTitle(challengeRecord.title);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setType(challengeRecord.type);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setDescription(challengeRecord.description);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setGeneralDetails(challengeRecord.generalDetails);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setDiscussions(challengeRecord.discussions);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setSector(challengeRecord.sector);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setLogo(challengeRecord.logo);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setLattitude(challengeRecord.lattitude);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setLongitude(challengeRecord.longitude);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setLocation(challengeRecord.location);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setImpact(challengeRecord.impact);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setProposal(challengeRecord.proposal);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setRating(challengeRecord.rating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setAvgRating(challengeRecord.avgRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setTotalRating(challengeRecord.totalRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setMyRating(challengeRecord.myRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setFollowing(challengeRecord.following);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setName(challengeRecord.name);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setPoulation(challengeRecord.poulation);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setDistictHeadquarters(challengeRecord.distictHeadquarters);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setMajorLivelihoodOpportunities(challengeRecord.majorLivelihoodOpportunities);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setPictures(challengeRecord.pictures);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setKeyword(challengeRecord.keyword);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setPainPoints(challengeRecord.painPoints);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setResources(challengeRecord.resources);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setFeedback(challengeRecord.feedback);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setStartDate(challengeRecord.startDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setEndDate(challengeRecord.endDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setLastDateForSubmissions(challengeRecord.lastDateForSubmissions);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setCreatedDate(challengeRecord.createdDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setUpdatedDate(challengeRecord.updatedDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            challenge.setIsActive(challengeRecord.isActive);
        } catch (e) {
            errorList.push(e);
        }
        //I made this change
        try {
            challenge.setIsGovernmentFunded(challengeRecord.isGovernmentFunded);
        } catch (e) {
            errorList.push(e);
        }
        if (errorList.length) {
            throw {
                status: REQUEST_CODES.FAIL,
                error: errorList
            };
        }
    }
    return challenge;
}

module.exports.ChallengeAPI = ChallengeAPI;
module.exports.ChallengeTest = Challenge;
