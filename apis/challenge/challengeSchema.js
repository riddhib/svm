module.exports.challengeSchema = { 
                                challengeId: {
                                    type: Number,
                                    unique: true,
                                    required: true,
                                    index: true
                                },
                                title: { 
                                    type: String,
                                    required: true
                                },
                                description: { 
                                    type: String
                                },
                                generalDetails: {
                                    type: String
                                },
                                type: {
                                    type: String
                                },
                                sector: {
                                    type: String
                                },
                                logo: {
                                    type: String
                                },
                                lattitude: {
                                    type: String
                                },
                                longitude: {
                                    type: String
                                },
                                location: {
                                    type: String
                                },
                                impact: {
                                    type: String
                                },
                                proposal: {
                                    type: [Number]
                                },
                                rating: [{
                                    rating: {type: Number},
                                    ratingId: {type: [Number]},
                                    userId: {type: [Number]}
                                }],
                                avgRating: {
                                    type: Number
                                },
                                totalRating: {
                                    type: Number
                                },
                                myRating: [{
                                    userId: {type: Number},
                                    rating: {type: Number}
                                }],
                                following: [{
                                    userId: {type: Number},
                                    userName: {type: String}
                                }],
                                name: {
                                    type: String
                                },
                                poulation: {
                                    type: String
                                },
                                distictHeadquarters: {
                                    type: String
                                },
                                majorLivelihoodOpportunities: {
                                    type: String
                                },
                                pictures: {
                                    type: String
                                }, 
                                keyword: {
                                    type: [String]
                                },
                                painPoints: {
                                    type: String
                                }, 
                                resources: {
                                    type: [Number]
                                },   
                                feedback: {
                                    type: String
                                },
                                discussions: [],
                                startDate: {
                                    type: Number
                                },
                                endDate: {
                                    type: Number
                                },
                                lastDateForSubmissions: {
                                    type: Number
                                },
                                isActive : {
                                    type: Boolean
                                },
                                //I made this change
                                isGovernmentFunded : {
                                    type: Boolean
                                },
                                createdDate: { 
                                    type: Number,
                                    index: true
                                },
                                updatedDate: { 
                                    type: Number,
                                    index: true
                                }

};