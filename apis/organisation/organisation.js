module.exports = function(app){
    app.post('/ui/organisation',function(req, res) {
		try {
			create(req.body, function(response){
				res.json(response);
			});
		} catch(e){
			res.json(e);
		}
    });
    app.get('/ui/organisation/:organisationId', function(req, res) {
		try {
			getDetails(req.params.organisationId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/query/organisation', function(req, res) {
		try {
			getList(req.query, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.put('/ui/organisation', function(req, res) {
		try {
			update(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.post('/ui/organisation/resetpwd',function(req, res) {
		try{
		    resetPassword(req.body, function(response){
				res.json(response);
			});
		} catch(e){
		 	res.json(e);
		}
	});
	app.get('/ui/clients/:organisationId', function(req, res) {
		try {
			getOrganisationClients(req.params.organisationId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.post('/ui/organisation/createClient',function(req, res) {
		console.log();
		try{
		   	createClient(req.session.user.organisationId, req.body, function(response){
		   		res.json(response);
			});
		} catch(e){
		 	res.json(e);
		}
	});
	app.put('/ui/organisation/addClient', function(req, res) {
		try {
			addClientToOrganisation(req.body.organisationId, req.body.clientId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.put('/ui/organisation/client', function(req, res) {
		try {
			removeClient(req.body.organisationId, req.body.clientId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.put('/ui/projectcategories', function(req, res) {
		try {
			insertOrUpdatePojectCategory(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
			console.log(e);
			res.json(e);
		}
	});
	app.put('/ui/taskcategories', function(req, res) {
		try {
			insertOrUpdateTaskCategory(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
			console.log(e);
			res.json(e);
		}
	});
	app.put('/ui/leavecategories', function(req, res) {
		console.log('t');
		try {
			insertOrUpdateLeaveCategory(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
			console.log(e);
			res.json(e);
		}
	});
	app.put('/ui/worklocations', function(req, res) {
		try {
			insertOrUpdateWorkLocation(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
			console.log(e);
			res.json(e);
		}
	});
	  app.put('/ui/customers', function (req, res) {
        try {
            insertOrUpdateCustomer(req.body, function (response) {
                res.json(response);
            });
        } catch (e) {
            console.log(e);
            res.json(e);
        }
    });
	app.put('/ui/designation', function(req, res) {
		try {
			insertOrUpdateDesignation(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
			console.log(e);
			res.json(e);
		}
	});
}

var mongoose = require('mongoose'),
Schema = mongoose.Schema;
var OrganisationSchema = new Schema(require('./organisationSchema').organisationSchema, {collection: 'organisation'});
var OrganisationModel = mongoose.model('organisation', OrganisationSchema);
var OrganisationController = require('./organisationController');
var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var ORGANISATION_CODES = CONSTANTS.ORGANISATION;
var VALIDATE = CONSTANTS.VALIDATE;
var validate = utils.validate;
var mongoUtils = utils.mongoUtils;
var User = require('../user/user');
var MailHelper = require('../mailHelper/mailHelper');

function create(organisation, callback) {
	var organisationAPI = OrganisationController.OrganisationAPI(organisation);
	organisationAPI.setCreatedDate(utils.getSystemTime());
	organisationAPI.setStatus('Inactive');
	var randomNumber = utils.getRandomNumber();
	organisationAPI.setPassword(utils.encryptText(randomNumber));
    var errorList = [];
    if (!organisationAPI.getName()) {
        var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'Name')
		};
		errorList.push(e);
    }
    if (!organisationAPI.getEmail()) {
        var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'Email')
		};
		errorList.push(e);
    }
    if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var organisationModel = new OrganisationModel(organisationAPI);
		User.getList({workEmail: organisationModel.email}, function(response) {
			if (response.error) {
				callback(response);
			} else {
				if (response.result && response.result.length) {
					callback({
						status: REQUEST_CODES.FAIL,
						error: 'Organisation already exists with these details.'
					});
				} else {
				    mongoUtils.getNextSequence('organisationId', function(oSeq) {
						organisationModel.organisationId = oSeq;
						organisationModel.save(function(error) {
							if (error) {
								callback({
									status: DB_CODES.FAIL,
									error: error
								});
								return;
							} else {
								var user = {
									isAdmin: true,
									status: 'Active',
									firstName: organisation.firstName,
									lastName: organisation.lastName,
									email: organisationAPI.getEmail(),
									phone: organisationAPI.getPhone(),
									organisationId: organisationModel.organisationId
								};
								User.create(user, function(response) {
									callback(response);
								});
							}
				   		});
				   	});
				}
			}
		});	    
	}
}

function getDetails(organisationId, callback) {
	var organisationRecordsObj = [];
	OrganisationModel.find({organisationId: organisationId}, function(error, organisationRecords) {		
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {		
			organisationRecords.forEach(function(organisationRecord) {
				var organisationRecord = new OrganisationController.OrganisationAPI(organisationRecord);
				delete organisationRecord.password;
				User.getList({organisationId: organisationRecord.organisationId, isAdmin: true}, function(response) {
					if (response.error) {
						callback(response);
					} else {
						var user = response.result[0];
						if (user) {
							organisationRecord['email'] = user.email;
							organisationRecord['firstName'] = user.firstName;
							organisationRecord['lastName'] = user.lastName;
							organisationRecordsObj.push(organisationRecord);
						}
			            callback({
							status: REQUEST_CODES.SUCCESS,
							result: organisationRecordsObj
						});
						return;
					}
				});
			});	
		}
	});
}

function getList(query, callback) {
	OrganisationModel.find(query, function(error, organisationRecords) {		
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			var lastIndex = organisationRecords.length;
			var organisationRecordsObj = [];
			organisationRecords.forEach(function(organisationRecord) {
				var organisationRecord = new OrganisationController.OrganisationAPI(organisationRecord);
				delete organisationRecord.password;
				User.getList({organisationId: organisationRecord.organisationId, isAdmin: true}, function(response) {
					if (response.error) {
						callback(response);
					} else {
						var user = response.result[0];
						if (user) {
							organisationRecord['email'] = user.email;
							organisationRecord['firstName'] = user.firstName;
							organisationRecord['lastName'] = user.lastName;
							organisationRecordsObj.push(organisationRecord);
						} else {
							organisationRecordsObj.push(organisationRecord);
						}					
						lastIndex --;
						if (lastIndex <= 0) {
				            callback({
								status: REQUEST_CODES.SUCCESS,
								result: organisationRecordsObj
							});
							return;
						}
					}
				});				
			});            		
		}
	});
}

function update(organisation, callback) {
	OrganisationModel.update({"organisationId": organisation.organisationId}, {$set: organisation}, function(error, effectedRows) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			if (!effectedRows.nModified) {
				callback({
					status: REQUEST_CODES.FAIL,
					error: utils.formatText(ORGANISATION_CODES.UPDATE_FAIL, organisation.organisationId)
				});
				return;
			} else {
				callback({
					status: REQUEST_CODES.SUCCESS,
					result: [utils.formatText(ORGANISATION_CODES.UPDATE_SUCCESS, organisation.organisationId)]
				});
				return;
			}
		}
	}); 
}

function resetPassword(organisation, callback) {
	var organisationAPI = OrganisationController.OrganisationAPI(organisation);
	organisationAPI.setUpdatedDate(utils.getSystemTime());
	var randomNumber = utils.getRandomNumber();
	organisationAPI.setPassword(utils.encryptText(randomNumber));
	OrganisationModel.update({"organisationId": organisation.organisationId}, {$set: organisation}, function(error, effectedRows) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			getDetails(organisation.organisationId, function(response) {
				if (response.error) {
					callback(response);
				} else {
					organisation = response.result[0];
					var template =  {
						body: "<p> Hello <b>" + organisation.name + "</b> Your account password has been updated successfully in SVM. Please use the following details to login into the application.</p><p>Email : "+ organisation.email + "<br/> Password : "+ randomNumber + "</p>",
						recipients: [organisation.email],
						from: 'SVM',
						subject: "Account Password Updated"
					};
					MailHelper.sendMail(template, function(response) {
						if (response.result && response.result.accepted && response.result.accepted.length) {
							callback({
								status: REQUEST_CODES.SUCCESS,
								result: [utils.formatText(ORGANISATION_CODES.PASSWORD_RESET_FAIL, organisation.organisationId)]
							});
							return;
						} else {
							callback({
								status: REQUEST_CODES.SUCCESS,
								result: [utils.formatText(ORGANISATION_CODES.PASSWORD_RESET_SUCCESS, organisation.organisationId)]
							});
							return;
						}						
					});
				}
			});			
		}
	});
}

module.exports.getDetails=getDetails;
module.exports.create = create;
module.exports.update = update;
module.exports.getList = getList;
module.exports.resetPassword = resetPassword;