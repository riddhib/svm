var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var VALIDATE = CONSTANTS.VALIDATE;
var validate = utils.validate;

var Organisation = function() {
    return {
        organisationId: 0,
        name: null,
        type: null,
        phone: null,
        email: null,
        password: null,
        status: null,
        createdDate: 0,
        updatedDate: 0
    }
};
function OrganisationAPI(organisationRecord) {
    var organisation = new Organisation();
    organisation.getOrganisationId = function() {
        return this.organisationId;
    };
    organisation.setOrganisationId = function(organisationId) {
        if (organisationId) {
            if (validate.isInteger(organisationId + '')) {
                this.organisationId = organisationId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, organisationId, 'Organisation ID')
                };
            }
        }
    };
    organisation.getName = function() {
        return this.name;
    };
    organisation.setName = function(name) {
        if (name) {
            this.name = name;
        }
    };
    organisation.getType = function() {
        return this.type;
    };
    organisation.setType = function(type) {
        if (type) {
            this.type = type;
        }
    };
    organisation.getPhone = function() {
        return this.phone;
    };
    organisation.setPhone = function(phone) {
        if (phone) {
            this.phone = phone;
        }
    };
    organisation.getEmail = function() {
        return this.email;
    };
    organisation.setEmail = function(email) {
        if (email) {
            if (email.length <= 100) {
                this.email = email;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, email, 'Email')
                };
            }
        }
    };
    organisation.getPassword = function() {
        return this.password;
    };
    organisation.setPassword = function(password) {
        if (password) {
            this.password = password;
        }
    };
    organisation.getStatus = function() {
        return this.status;
    };
    organisation.setStatus = function(status) {
        if (status) {
            this.status = status;            
        }
    };
    organisation.getCreatedDate = function() {
        return this.createdDate;
    };
    organisation.setCreatedDate = function(createdDate) {
        if (createdDate) {
            if (validate.isInteger(createdDate + '')) {
                    this.createdDate = createdDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdDate, 'createdDate')
                };
            }
        }
    };
    organisation.getUpdatedDate = function() {
        return this.updatedDate;
    };
    organisation.setUpdatedDate = function(updatedDate) {
        if (updatedDate) {
            if (validate.isInteger(updatedDate + '')) {
                    this.updatedDate = updatedDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, updatedDate, 'updatedDate')
                };
            }
        }
    };
    if (organisationRecord) {
        var errorList = [];
        try {
            organisation.setOrganisationId(organisationRecord.organisationId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            organisation.setName(organisationRecord.name);
        } catch (e) {
            errorList.push(e);
        }
        try {
            organisation.setType(organisationRecord.type);
        } catch (e) {
            errorList.push(e);
        }
        try {
            organisation.setPhone(organisationRecord.phone);
        } catch (e) {
            errorList.push(e);
        }
        try {
            organisation.setEmail(organisationRecord.email);
        } catch (e) {
            errorList.push(e);
        }
        try {
            organisation.setPassword(organisationRecord.password);
        } catch (e) {
            errorList.push(e);
        }
        try {
            organisation.setStatus(organisationRecord.status);
        } catch (e) {
            errorList.push(e);
        }
        try {
            organisation.setCreatedDate(organisationRecord.createdDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            organisation.setUpdatedDate(organisationRecord.updatedDate);
        } catch (e) {
            errorList.push(e);
        }
        if (errorList.length) {
            throw {
                status: REQUEST_CODES.FAIL,
                error: errorList
            };
        }
    }
    return organisation;
}

module.exports.OrganisationAPI = OrganisationAPI;