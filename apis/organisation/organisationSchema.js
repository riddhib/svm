module.exports.organisationSchema = { 
                                organisationId: { 
                                    type: Number,
                                    required: true,
                                    unique: true,
                                    index: true
                                },
                                name: { 
                                    type: String,
                                    required: true
                                },
                                type: { 
                                    type: String
                                },
                                phone: {
                                    type: String,
                                },
                                status: {
                                    type: String,
                                    required: true
                                },
                                createdDate: { 
                                    type: Number
                                },
                                updatedDate: { 
                                    type: Number
                                }
};