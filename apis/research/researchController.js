var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var VALIDATE = utils.CONSTANTS.VALIDATE;
var validate = utils.validate;

var Research = function() {
    return {
        researchId: 0,
        researchName: null,
        description: null,
        type: null,
        sector: [],
        keyword: [],
        following: [],
        rating: [],
        projects: [],
        avgRating: 0,
        totalRating: 0,
        myRating: [],
        discussions: [],
        uploadBy: null,
        fileId: 0,
        isActive: true,
        researchUrl: null,
        author: null,
        createdDate: 0,
        updatedDate: 0
    };
};

function ResearchAPI(researchRecord) {
    var research = new Research();
    research.getResearchId = function() {
        return this.researchId;
    };
    research.setResearchId = function(researchId) {
        if (researchId) {
            if (validate.isInteger(researchId + '')) {
                    this.researchId = researchId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, researchId, 'researchId')
                };
            }
        }
    };
    research.getResearchName = function() {
        return this.researchName;
    };
    research.setResearchName = function(researchName) {
         if (researchName) {
            if (researchName.length <= 250) {
                this.researchName = researchName;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, researchName, 'researchName')
                };
            }
        }
    };
    research.getDescription = function() {
        return this.description;
    };
    research.setDescription = function(description) {
        if (description) {
            this.description = description;
        }
    };
    research.getProjects = function() {
        return this.projects;
    };
    research.setProjects = function(projects) {
        if (projects) {
            this.projects = projects;
        }
    };
    research.getKeyword = function() {
        return this.keyword;
    };
    research.setKeyword = function(keyword) {
        if (keyword) {
            this.keyword = keyword;
        }
    };
    research.getFollowing = function() {
        return this.following;
    };
    research.setFollowing = function(following) {
        if (following) {
            this.following = following;
        }
    };
    research.getDiscussions = function() {
        return this.discussions;
    };
    research.setDiscussions = function(discussions) {
        if (discussions) {
            this.discussions = discussions;
        }
    };
    research.getResearchUrl = function() {
        return this.researchUrl;
    };
    research.setResearchUrl = function(researchUrl) {
        this.researchUrl = researchUrl;
    };
    research.getAuthor = function() {
        return this.author;
    };
    research.setAuthor = function(author) {
        this.author = author;
    };
    research.getRating = function() {
        return this.rating;
    };
    research.setRating = function(rating) {
        if (rating) {
            this.rating = rating;
        }
    };
    research.getAvgRating = function() {
        return this.avgRating;
    };
    research.setAvgRating = function(avgRating) {
        if (avgRating) {
            this.avgRating = avgRating;
        }
    };
    research.getTotalRating = function() {
        return this.totalRating;
    };
    research.setTotalRating = function(totalRating) {
        if (totalRating) {
            this.totalRating = totalRating;
        }
    };
    research.getMyRating = function() {
        return this.myRating;
    };
    research.setMyRating = function(myRating) {
        if (myRating) {
            this.myRating = myRating;
        }
    };
    research.getType = function() {
        return this.type;
    };
    research.setType = function(type) {
        if (type) {
            this.type = type;
        }
    };
    research.getSector = function() {
        return this.sector;
    };
    research.setSector = function(sector) {
        if (sector) {
            this.sector = sector;
        }
    };
    research.getUploadBy = function() {
        return this.uploadBy;
    };
    research.setUploadBy = function(uploadBy) {
        if (uploadBy) {
            this.uploadBy = uploadBy;
        }
    };
    research.getIsActive = function() {
        return this.isActive;
    };
    research.setIsActive = function(isActive) {
        this.isActive = isActive;
    };
    research.getFileId = function() {
        return this.fileId;
    };
    research.setFileId = function(fileId) {
        if (fileId) {
            if (validate.isInteger(fileId + '')) {
                    this.fileId = fileId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, fileId, 'fileId')
                };
            }
        }
    };
    research.getCreatedDate = function() {
        return this.createdDate;
    };
    research.setCreatedDate = function(createdDate) {
        if (createdDate) {
            if (validate.isInteger(createdDate + '')) {
                    this.createdDate = createdDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdDate, 'createdDate')
                };
            }
        }
    };
    research.getUpdatedDate = function() {
        return this.updatedDate;
    };
    research.setUpdatedDate = function(updatedDate) {
        if (updatedDate) {
            if (validate.isInteger(updatedDate + '')) {
                    this.updatedDate = updatedDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, updatedDate, 'updatedDate')
                };
            }
        }
    };
   
    if (researchRecord) {
        var errorList = [];
        try {
            research.setResearchId(researchRecord.researchId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setResearchName(researchRecord.researchName);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setDescription(researchRecord.description);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setKeyword(researchRecord.keyword);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setDiscussions(researchRecord.discussions);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setFollowing(researchRecord.following);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setRating(researchRecord.rating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setAvgRating(researchRecord.avgRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setTotalRating(researchRecord.totalRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setMyRating(researchRecord.myRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setType(researchRecord.type);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setSector(researchRecord.sector);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setProjects(researchRecord.projects);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setUploadBy(researchRecord.uploadBy);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setFileId(researchRecord.fileId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setCreatedDate(researchRecord.createdDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setUpdatedDate(researchRecord.updatedDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setIsActive(researchRecord.isActive);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setResearchUrl(researchRecord.researchUrl);
        } catch (e) {
            errorList.push(e);
        }
        try {
            research.setAuthor(researchRecord.author);
        } catch (e) {
            errorList.push(e);
        }
        if (errorList.length) {
            throw {
                status: REQUEST_CODES.FAIL,
                error: errorList
            };
        }
    }
    return research;
}

module.exports.ResearchAPI = ResearchAPI;