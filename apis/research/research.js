module.exports = function(app){
	app.post('/ui/research',function(req, res){
        try {
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            create(req.body, function(response){
               res.json(response);
        	});
         
        } catch(e){
            res.json(e);
        }
    });
	app.put('/ui/research', function(req, res) {
		try {
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			update(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
	app.put('/ui/researchDelete', function(req, res) {
		try {
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			researchDelete(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
    app.get('/ui/research/:researchId', function(req, res) {
		try {
			getDetails(req.params.researchId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/query/research', function(req, res) {
		try {
			getListWithFiles(req.query, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});

	app.put('/ui/research/addProjects', function(req, res) {
		try {
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			addProjects(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
}

var mongoose = require('mongoose')
	Schema = mongoose.Schema;
var ResearchSchema = new Schema(require('./researchSchema').researchSchema, {collection: 'research'});
var ResearchModel = mongoose.model('research', ResearchSchema);
var ResearchController = require('./researchController');
var Discussion = require('../discussions/discussion');
var UserLog = require('../userLog/userLog');
var Projects = require('../project/project');
var utils = require('../../assets/utils').utils;
var MailHelper = require('../mailHelper/mailHelper');
var File = require('../file/file');
var mongoUtils = utils.mongoUtils;
var CONSTANTS = utils.CONSTANTS;
var VALIDATE = CONSTANTS.VALIDATE;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var RESEARCH_CODES = CONSTANTS.RESEARCH_CODES;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var _ = require('underscore');

function create(research, callback) {
	var researchAPI = ResearchController.ResearchAPI(research.advanceDetails);
    var errorList = [];
    if (!researchAPI.getResearchName()) {
       	var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'researchName')
		};
		errorList.push(e);
	}	
   	if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var researchModel = new ResearchModel(researchAPI);
		var query = {
			"fileName": research.advanceDetails.fileName,
			"originalName": research.file.originalname, 
			"uploadBy": research.advanceDetails.uploadBy, 
			"description": research.advanceDetails.description, 
			"filePath": '/uploads/research/'+research.file.filename, 
			"userId": research.advanceDetails.userId, 
			"category": "research", 
			"mimeType": research.file.mimetype,
			"userId": research.userId,
			"userName": research.userName,
			"userEmail": research.userEmail,
			"url": research.url,
			"method": research.method,
			"apiRequest": {
				"module": "File-Create",
				"name": researchModel.researchName,
				"id": researchModel.researchId
			},
			"apiResponse": "SUCCESS"
		};
		File.create(query, function(response) {
			if(response.error) {
				callback(response);
			} else {
				var fileResponse = response.file;
				researchModel.isActive = true;
				researchModel.fileId = fileResponse.fileId;
				mongoUtils.getNextSequence('researchId', function(oSeq) {
					researchModel.researchId = oSeq;
					researchModel.createdDate = new Date().getTime();
					var newDiscussion = {
						"discussionName": researchModel.researchName,
						"body": researchModel.description || "",
						"category": "Research",
						"parentId": researchModel.researchId,
						"userId": research.userId,
						"userName": research.userName,
						"userEmail": research.userEmail,
						"url": research.url,
						"method": research.method,
						"apiRequest": {
							"module": "Discussions-Create",
							"name": researchModel.researchName,
							"id": researchModel.researchId
						},
						"apiResponse": "SUCCESS"
					};
					Discussion.create(newDiscussion, function(response){
						if(response.error){
							callback({
								status: REQUEST_CODES.ERROR,
								error: "Failed to create Research"
							});
						} else {
							var discussionId = response.result[0].discussionId;
							researchModel.discussions.push(discussionId);
							researchModel.save(function(error) {
								if (error) {
									callback({
										status: DB_CODES.FAIL,
										error: error
									});
									return;
								} else {
									let logObj = {
										"userId": research.userId,
										"userName": research.userName,
										"email": research.userEmail,
										"requestUrl": research.url,
										"requestMethod": research.method,
										"apiRequest": {
											"module": "Research-Create",
											"name": researchModel.researchName,
											"id": researchModel.researchId
										},
										"apiResponse": "SUCCESS"
									}
									UserLog.create(logObj, function(response){
										if(!response.error){
											callback({
												status: REQUEST_CODES.SUCCESS,
												result: utils.formatText(RESEARCH_CODES.CREATE_SUCCESS, researchModel.researchId)
											});
											return;
										}
									});					
								}
							});
						}
					});
				});
			}
		});
	}
}

function createResearchFromProject(research, callback){
	var researchAPI = ResearchController.ResearchAPI(research);
    var errorList = [];
    if (!researchAPI.getResearchName()) {
       	var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'researchName')
		};
		errorList.push(e);
	}	
   	if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var researchModel = new ResearchModel(researchAPI);
		researchModel.isActive = true;
		researchModel.uploadBy = research.fileDetails.uploadBy;
		researchModel.fileId = research.fileDetails.fileId;
		mongoUtils.getNextSequence('researchId', function(oSeq) {
			researchModel.researchId = oSeq;
			researchModel.createdDate = new Date().getTime();
			var newDiscussion = {
				"discussionName": researchModel.researchName,
				"body": researchModel.description || "",
				"category": "Research",
				"parentId": researchModel.researchId,
				"userId": research.userId,
				"userName": research.userName,
				"userEmail": research.email,
				"url": research.requestUrl,
				"method": research.requestMethod,
				"apiRequest": {
					"module": "Discussions-Create",
					"name": researchModel.researchName,
					"id": researchModel.researchId
				},
				"apiResponse": "SUCCESS"
			};
			Discussion.create(newDiscussion, function(response){
				if(response.error){
					callback({
						status: REQUEST_CODES.ERROR,
						error: "Failed to create Research"
					});
				} else {
					var discussionId = response.result[0].discussionId;
					researchModel.discussions.push(discussionId);
					researchModel.save(function(error) {
						if (error) {
							callback({
								status: DB_CODES.FAIL,
								error: error
							});
							return;
						} else {
							let logObj = {
								"userId": research.userId,
								"userName": research.userName,
								"email": research.email,
								"requestUrl": research.requestUrl,
								"requestMethod": research.requestMethod,
								"apiRequest": {
									"module": "Solutions-Create",
									"name": researchModel.researchName,
									"id": researchModel.researchId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: utils.formatText(RESEARCH_CODES.CREATE_SUCCESS, researchModel.researchId)
									});
									return;	
								}
							});						
						}
					});
				}
			});
		});
	}
}

function getDetails(researchId, callback) {
	ResearchModel.find({"researchId": researchId}, function(error, researchRecords) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			researchRecords = researchRecords.map(function(researchRecord) {
				return new ResearchController.ResearchAPI(researchRecord);
			});			
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: researchRecords
			});
			return;		
		}
	});	
}

function getList(query, callback) {
	ResearchModel.find(query, function(error, researchRecords) {		
		if (error) {
			callback({
			    status: DB_CODES.FAIL,
			    error: error
			});
			return;
		} else {		
			researchRecords = researchRecords.map(function(researchRecord) {
				return new ResearchController.ResearchAPI(researchRecord);
			});
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: researchRecords
			});
			return;	
		}
	}).sort({createdDate: -1});
}

function getResearches(query, projection, callback) {
	ResearchModel.find(query, projection, function(error, researchRecords) {		
		if (error) {
			callback({
			    status: DB_CODES.FAIL,
			    error: error
			});
			return;
		} else {
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: researchRecords
			});
			return;	
		}
	});
}

function getListWithFiles(query, callback) {
	getList(query, function(response){
		if(response.error) {
			callback(response);
		} else {
			var researchRecords = response.result;
			var researchRecordsIndex = researchRecords.length;
			var fileIds = _.pluck(researchRecords, 'fileId');
			File.getList({fileId: {$in: fileIds}}, function(response) {
				if(response.error) {
					callback(response);
				} else {
					var fileResponse = response.result;
					var reserachFiles = _.filter(researchRecords, function(research){ 
						index = fileResponse.findIndex(x => x.fileId == research.fileId);
						if(index < 0) {
							research.filePath = '';
						} else {
							if(research.fileId == fileResponse[index].fileId) {
								research.filePath = fileResponse[index].filePath;
							}
						}
						return research; 
					});
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: reserachFiles
					})
				}
			});
		}
	})
}

async function addProjects(projects, callback){
	let projectIds = projects.projectIds.map(project=> parseInt(project)) || [],
		researchProjects = [];
		
	if (projectIds.length > 0) {
		await new Promise(resolve => {
			Projects.getList({projectId: {$in: projectIds}}, function(response) {
				if (!response.error) {
					researchProjects = response.result.map(project => {
						return {
							"projectId": project.projectId,
							"projectName": project.name
						};
					}) || [];
				}
				resolve();
			});
		});
		let query = {
			"researchId": projects.researchId, 
			"projects": researchProjects,
			"userId": projects.userId,
			"userName": projects.userName,
			"userEmail": projects.userEmail,
			"url": projects.url,
			"method": projects.method,
			"apiRequest": {
				"module": "Solutions-Update",
				"name": '',
				"id": projects.researchId
			},
			"apiResponse": "SUCCESS"
		}
		update(query, function(response){
			if (response.error) {
				callback({
					status: REQUEST_CODES.FAIL,
					error: utils.formatText(FOLDER_CODES.UPDATE_FAIL, projects.researchId)
				});
				return;
			} else {
				callback({
					status: REQUEST_CODES.SUCCESS,
					result: [projects.researchId]
				});
				return;
			}
		});
	} else {
		callback({
			status: REQUEST_CODES.SUCCESS,
			response: "No projects selected!"
		});
		return;
	}
}

function update(research, callback) {
	getDetails(research.researchId, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result) {
				var researchDetails = response.result[0];
				research.updatedDate = new Date().getTime();
				ResearchModel.updateOne({"researchId": research.researchId}, {$set: research}, function(error, effectedRows) {
					if (error) {
						callback({
							status: DB_CODES.FAIL,
							error: error
						});
						return;
					} else {
						if (!effectedRows.nModified) {
							callback({
								status: REQUEST_CODES.FAIL,
								error: utils.formatText(RESEARCH_CODES.UPDATE_FAIL, research.researchId)
							});
							return;
						} else {
							let logObj = {
								"userId": research.userId,
								"userName": research.userName,
								"email": research.userEmail,
								"requestUrl": research.url,
								"requestMethod": research.method,
								"apiRequest": {
									"module": "Research-Update",
									"name": researchDetails.researchName,
									"id": researchDetails.researchId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: utils.formatText(RESEARCH_CODES.UPDATE_SUCCESS, research.researchId)
									});
									return;
								}
							});
						}
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No Research Found"
				});
				return;
			}
		}
	});	 
}

function researchDelete(research, callback) {
	getList({"fileId": research.fileId}, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result.length > 0) {
				var researchData = response.result[0];
				var query1 = {
					"discussionIds": researchData.discussions, 
					"isActive": false,
					"userId": research.userId,
					"userName": research.userName,
					"userEmail": research.userEmail,
					"url": research.url,
					"method": research.method,
					"apiRequest": {
						"module": "Discussions-Create",
						"name": researchData.researchName,
						"id": researchData.researchId
					},
					"apiResponse": "SUCCESS"
				}
				Discussion.updateDiscussionStatus(query1, function(response) {
					if(!response.error){
						ResearchModel.updateOne({"researchId": researchData.researchId}, {$set: {"isActive": false}}, function(error, effectedRows) {
							if (error) {
								callback({
									status: DB_CODES.FAIL,
									error: error
								});
								return;
							} else {
								if (!effectedRows.nModified) {
									callback({
										status: REQUEST_CODES.FAIL,
										error: error
									});
									return;
								} else {
									let logObj = {
										"userId": research.userId,
										"userName": research.userName,
										"email": research.userEmail,
										"requestUrl": research.url,
										"requestMethod": research.method,
										"apiRequest": {
											"module": "Research-Update",
											"name": researchData.researchName,
											"id": researchData.researchId
										},
										"apiResponse": "SUCCESS"
									}
									UserLog.create(logObj, function(response){
										if(!response.error){
											callback({
												status: REQUEST_CODES.SUCCESS,
												result: "Success"
											});
											return;
										}
									});
								}
							}
						});
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No Research Found"
				});
				return;
			}
		}
	});	 
}

module.exports.create = create;
module.exports.getDetails = getDetails;
module.exports.getList = getList;
module.exports.update = update;
module.exports.getResearches = getResearches;
module.exports.createResearchFromProject = createResearchFromProject;
