module.exports.researchSchema = { 
                               researchId: {
                                    type: Number,
                                    unique: true,
                                    required: true,
                                    index: true
                                },
                                researchName: { 
                                    type: String
                                },
                                description: {
                                    type: String
                                },
                                type: {
                                    type: String
                                },
                                sector: {
                                    type: [String]
                                },
                                keyword: {
                                    type: [String]
                                },
                                following: [{
                                    userId: {type: Number},
                                    userName: {type: String}
                                }],
                                rating: [{
                                    rating: {type: Number},
                                    ratingId: {type: [Number]},
                                    userId: {type: [Number]}
                                }],
                                avgRating: {
                                    type: Number
                                },
                                researchUrl: {
                                    type: String
                                },
                                author: {
                                    type: String
                                },
                                totalRating: {
                                    type: Number
                                },
                                myRating: [{
                                    userId: {type: Number},
                                    rating: {type: Number}
                                }],
                                discussions:[],
                                projects: [],
                                uploadBy: {
                                    type: String
                                },
                                fileId: {
                                    type: Number
                                },
                                isActive: {
                                    type: Boolean
                                },
                                createdDate: { 
                                    type: Number,
                                    index: true
                                },
                                updatedDate: { 
                                    type: Number,
                                    index: true
                                }

};