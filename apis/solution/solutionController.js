var utils = require('../../assets/utils').utils;
var CONSTANTS = utils.CONSTANTS;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;
var VALIDATE = utils.CONSTANTS.VALIDATE;
var validate = utils.validate;

var Solution = function() {
    return {
        solutionId: 0,
        name: null,
        type: null,
        location: null,
        logo: null,
        affiliated: null,
        website: null,
        dueDate: 0,
        status: null,
        sector: [],
        following: [],
        rating: [],
        avgRating: 0,
        totalRating: 0,
        myRating: [],
        discussions: [],
        impact: null,
        description: null,
        article: [],
        addArticle: [],
        countryOfOps: null,
        descriptionOfSolution: null,
        referenceSites: null,
        website: null,
        collaboration: null,
        isActive: true,
        createdDate: 0,
        updatedDate: 0
    };
};

function SolutionAPI(solutionRecord) {
    var solution = new Solution();
    solution.getSolutionId = function() {
        return this.solutionId;
    };
    solution.setSolutionId = function(solutionId) {
        if (solutionId) {
            if (validate.isInteger(solutionId + '')) {
                    this.solutionId = solutionId;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, solutionId, 'solutionId')
                };
            }
        }
    };
    solution.getName = function() {
        return this.name;
    };
    solution.setName = function(name) {
         if (name) {
            if (name.length <= 250) {
                this.name = name;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, name, 'name')
                };
            }
        }
    };
    solution.getType = function() {
        return this.type;
    };
    solution.setType = function(type) {
        if (type) {
            this.type = type;
        }
    };
    solution.getLocation = function() {
        return this.location;
    };
    solution.setLocation = function(location) {
        if (location) {
            this.location = location;
        }
    };
    solution.getLogo = function() {
        return this.logo;
    };
    solution.setLogo = function(logo) {
        if (logo) {
            this.logo = logo;
        }
    };
    solution.getAffiliated = function() {
        return this.affiliated;
    };
    solution.setAffiliated = function(affiliated) {
        if (affiliated) {
            this.affiliated = affiliated;
        }
    };
    solution.getWebsite = function() {
        return this.website;
    };
    solution.setWebsite = function(website) {
        if (website) {
            this.website = website;
        }
    };
    solution.getDiscussions = function() {
        return this.discussions;
    };
    solution.setDiscussions = function(discussions) {
        if (discussions) {
            this.discussions = discussions;
        }
    };
    solution.getDueDate = function() {
        return this.dueDate;
    };
    solution.setDueDate = function(dueDate) {
        if (dueDate) {
            if (validate.isInteger(dueDate + '')) {
                    this.dueDate = dueDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, dueDate, 'dueDate')
                };
            }
        }
    };
    solution.getStatus = function() {
        return this.status;
    };
    solution.setStatus = function(status) {
         if (status) {
            if (status.length <= 50) {
                this.status = status;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.VALUE_TOO_BIG, status, 'status')
                };
            }
        }
    };
    solution.getSector = function() {
        return this.sector;
    };
    solution.setSector = function(sector) {
        if (sector) {
            this.sector = sector;
        }
    };
    solution.getFollowing = function() {
        return this.following;
    };
    solution.setFollowing = function(following) {
        if (following) {
            this.following = following;
        }
    };
    solution.getRating = function() {
        return this.rating;
    };
    solution.setRating = function(rating) {
        if (rating) {
            this.rating = rating;
        }
    };
    solution.getAvgRating = function() {
        return this.avgRating;
    };
    solution.setAvgRating = function(avgRating) {
        if (avgRating) {
            this.avgRating = avgRating;
        }
    };
    solution.getTotalRating = function() {
        return this.totalRating;
    };
    solution.setTotalRating = function(totalRating) {
        if (totalRating) {
            this.totalRating = totalRating;
        }
    };
    solution.getMyRating = function() {
        return this.myRating;
    };
    solution.setMyRating = function(myRating) {
        if (myRating) {
            this.myRating = myRating;
        }
    };
    solution.getImpact = function() {
        return this.impact;
    };
    solution.setImpact = function(impact) {
        if (impact) {
            this.impact = impact;
        }
    };
    solution.getDescription = function() {
        return this.description;
    };
    solution.setDescription = function(description) {
        if (description) {
            this.description = description;
        }
    };
    solution.getIsActive = function() {
        return this.isActive;
    };
    solution.setIsActive = function(isActive) {
        this.isActive = isActive;
    };
    solution.getArticle = function() {
        return this.article;
    };
    solution.setArticle = function(article) {
        if (article) {
            this.article = article;
        }
    };
    solution.getAddArticle = function() {
        return this.addArticle;
    };
    solution.setAddArticle = function(addArticle) {
        if (addArticle) {
            this.addArticle = addArticle;
        }
    };
    solution.getCountryOfOps = function() {
        return this.countryOfOps;
    };
    solution.setCountryOfOps = function(countryOfOps) {
        if (countryOfOps) {
            this.countryOfOps = countryOfOps;
        }
    };
    solution.getDescriptionOfSolution = function() {
        return this.descriptionOfSolution;
    };
    solution.setDescriptionOfSolution = function(descriptionOfSolution) {
        if (descriptionOfSolution) {
            this.descriptionOfSolution = descriptionOfSolution;
        }
    };
    solution.getReferenceSites = function() {
        return this.referenceSites;
    };
    solution.setReferenceSites = function(referenceSites) {
        if (referenceSites) {
            this.referenceSites = referenceSites;
        }
    };
    solution.getWebsite = function() {
        return this.website;
    };
    solution.setWebsite = function(website) {
        if (website) {
            this.website = website;
        }
    };
    solution.getCollaboration = function() {
        return this.collaboration;
    };
    solution.setCollaboration = function(collaboration) {
        if (collaboration) {
            this.collaboration = collaboration;
        }
    };
    solution.getCreatedDate = function() {
        return this.createdDate;
    };
    solution.setCreatedDate = function(createdDate) {
        if (createdDate) {
            if (validate.isInteger(createdDate + '')) {
                    this.createdDate = createdDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, createdDate, 'createdDate')
                };
            }
        }
    };
    solution.getUpdatedDate = function() {
        return this.updatedDate;
    };
    solution.setUpdatedDate = function(updatedDate) {
        if (updatedDate) {
            if (validate.isInteger(updatedDate + '')) {
                    this.updatedDate = updatedDate;
            } else {
                throw {
                    status: VALIDATE.FAIL,
                    error: utils.formatText(VALIDATE.NOT_A_INTEGER, updatedDate, 'updatedDate')
                };
            }
        }
    };
   
    if (solutionRecord) {
        var errorList = [];
        try {
            solution.setSolutionId(solutionRecord.solutionId);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setName(solutionRecord.name);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setType(solutionRecord.type);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setLocation(solutionRecord.location);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setLogo(solutionRecord.logo);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setAffiliated(solutionRecord.affiliated);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setWebsite(solutionRecord.website);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setDueDate(solutionRecord.dueDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setDiscussions(solutionRecord.discussions);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setStatus(solutionRecord.status);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setSector(solutionRecord.sector);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setFollowing(solutionRecord.following);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setRating(solutionRecord.rating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setAvgRating(solutionRecord.avgRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setTotalRating(solutionRecord.totalRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setMyRating(solutionRecord.myRating);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setImpact(solutionRecord.impact);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setDescription(solutionRecord.description);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setArticle(solutionRecord.article);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setAddArticle(solutionRecord.addArticle);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setCountryOfOps(solutionRecord.countryOfOps);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setDescriptionOfSolution(solutionRecord.descriptionOfSolution);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setReferenceSites(solutionRecord.referenceSites);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setWebsite(solutionRecord.website);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setCollaboration(solutionRecord.collaboration);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setCreatedDate(solutionRecord.createdDate);
        } catch (e) {
            errorList.push(e);
        }
        try {
            solution.setUpdatedDate(solutionRecord.updatedDate);
        } catch (e) {
            errorList.push(e);
        }  
        try {
            solution.setIsActive(solutionRecord.isActive);
        } catch (e) {
            errorList.push(e);
        }        
        if (errorList.length) {
            throw {
                status: REQUEST_CODES.FAIL,
                error: errorList
            };
        }
    }
    return solution;
}

module.exports.SolutionAPI = SolutionAPI;