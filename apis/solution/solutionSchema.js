module.exports.solutionSchema = { 
                               solutionId: {
                                    type: Number,
                                    unique: true,
                                    required: true,
                                    index: true
                                },
                                name: { 
                                    type: String
                                },
                                type: {
                                    type: String
                                },
                                location: {
                                    type: String
                                },
                                logo: {
                                    type: String
                                },
                                affiliated: {
                                    type: String
                                },
                                website: {
                                    type: String
                                },
                                dueDate: {
                                    type: Number
                                },
                                status: { 
                                    type: String
                                },
                                sector: {
                                    type: [String]
                                },
                                following: [{
                                    userId: {type: Number},
                                    userName: {type: String}
                                }],
                                rating: [{
                                    rating: {type: Number},
                                    ratingId: {type: [Number]},
                                    userId: {type: [Number]}
                                }],
                                avgRating: {
                                    type: Number
                                },
                                totalRating: {
                                    type: Number
                                },
                                myRating: [{
                                    userId: {type: Number},
                                    rating: {type: Number}
                                }],
                                impact: {
                                    type: String
                                },
                                description: {
                                    type: String
                                },
                                article: {
                                    type: [Number]
                                },
                                addArticle: {
                                    type: []
                                },
                                countryOfOps: {
                                    type: String
                                },
                                descriptionOfSolution: {
                                    type: String
                                },
                                referenceSites: {
                                    type: String
                                },
                                website: {
                                    type: String
                                },
                                discussions: [],
                                collaboration: {
                                    type: String
                                },
                                isActive: {
                                    type: Boolean
                                },
                                createdDate: { 
                                    type: Number,
                                    index: true
                                },
                                updatedDate: { 
                                    type: Number,
                                    index: true
                                }

};