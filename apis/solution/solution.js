module.exports = function(app){
	app.post('/ui/solution',function(req, res){
        try{
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            create(req.body, function(response){
               res.json(response);
        	});
         
        }catch(e){
            res.json(e);
        }
    });
	app.put('/ui/solution', function(req, res) {
		try {
			req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
			update(req.body, function(response) {
				res.json(response);
			});
		} catch(e) {
		 	res.json(e);
		}
	});
    app.get('/ui/solution/:solutionId', function(req, res) {
		try {
			getDetails(req.params.solutionId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	app.get('/ui/query/solution', function(req, res) {
		try {
			getList(req.query, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
	  
	app.post('/ui/solution/uploadResource',function(req, res) {
        try {
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            uploadResource(req.body, function(response) {
        	    res.json(response);
            });
        } catch(e){
            res.json(e);
        }
	});
	app.post('/ui/solution/addArticle',function(req, res){
        try{
        	req.body.userId = req.session.userId;
			req.body.userName = req.session.user.userName;
			req.body.userEmail = req.session.user.email;
			req.body.url = req.url;
			req.body.method = req.method;
            addArticle(req.body, function(response){
               res.json(response);
        	});
         
        }catch(e){
            res.json(e);
        }
    });
	app.get('/ui/getArticle/:solutionId/', function(req, res) {
		try {
			getArticle(req.params.solutionId, function(response) {
				res.json(response);
			});
		} catch(e) {
			res.json(e);
		}
	});
}

var mongoose = require('mongoose')
	Schema = mongoose.Schema;
var SolutionSchema = new Schema(require('./solutionSchema').solutionSchema, {collection: 'solution'});
var SolutionModel = mongoose.model('solution', SolutionSchema);
var SolutionController = require('./solutionController');
var Discussion = require('../discussions/discussion');
var UserLog = require('../userLog/userLog');
var utils = require('../../assets/utils').utils;
var MailHelper = require('../mailHelper/mailHelper');
var File = require('../file/file');
var mongoUtils = utils.mongoUtils;
var CONSTANTS = utils.CONSTANTS;
var VALIDATE = CONSTANTS.VALIDATE;
var DB_CODES = CONSTANTS.DATABASE_CODES;
var SOLUTION_CODES = CONSTANTS.SOLUTION_CODES;
var REQUEST_CODES = CONSTANTS.REQUEST_CODES;

function create(solution, callback) {
	var solutionAPI = SolutionController.SolutionAPI(solution);
    var errorList = [];
    if (!solutionAPI.getName()) {
       	var e = {
			status: VALIDATE.FAIL,
			error: utils.formatText(VALIDATE.REQUIRED, 'name')
		};
		errorList.push(e);
	}	
   	if (errorList.length) {
		throw {
		    status: REQUEST_CODES.FAIL,
		    error: errorList
		};
	}  else {
		var solutionModel = new SolutionModel(solutionAPI);
		solutionModel.isActive = true;
		mongoUtils.getNextSequence('solutionId', function(oSeq) {
			solutionModel.solutionId = oSeq;
			solutionModel.createdDate = new Date().getTime();
			var newDiscussion = {
				"discussionName": solutionModel.name,
				"body": solutionModel.descriptionOfSolution || "",
				"category": "Solutions",
				"parentId": solutionModel.solutionId,
				"userId": solution.userId,
				"userName": solution.userName,
				"userEmail": solution.userEmail,
				"url": solution.url,
				"method": solution.method,
				"apiRequest": {
					"module": "Discussions-Create",
					"name": solution.name,
					"id": solutionModel.solutionId
				},
				"apiResponse": "SUCCESS"
			};
			Discussion.create(newDiscussion, function(response){
				if(response.error){
					callback({
						status: REQUEST_CODES.FAIL,
						error: "Failed to create Solution"
					});
				} else {
					var discussionId = response.result[0].discussionId;
					solutionModel.discussions.push(discussionId);
					solutionModel.save(function(error) {
						if (error) {
							callback({
								status: DB_CODES.FAIL,
								error: error
							});
							return;
						} else {
							let logObj = {
								"userId": solution.userId,
								"userName": solution.userName,
								"email": solution.userEmail,
								"requestUrl": solution.url,
								"requestMethod": solution.method,
								"apiRequest": {
									"module": "Solutions-Create",
									"name": solution.name,
									"id": solutionModel.solutionId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: utils.formatText(SOLUTION_CODES.CREATE_SUCCESS, solutionModel.solutionId)
									});
									return;		
								}
							});					
						}
					});
				}
			});
		});
	}
}

function getDetails(solutionId, callback) {
	SolutionModel.find({"solutionId": solutionId}, function(error, solutionRecords) {
		if (error) {
			callback({
				status: DB_CODES.FAIL,
				error: error
			});
			return;
		} else {
			solutionRecords = solutionRecords.map(function(solutionRecord) {
				return new SolutionController.SolutionAPI(solutionRecord);
			});			
            callback({
				status: REQUEST_CODES.SUCCESS,
				result: solutionRecords
			});
			return;		
		}
	});	
}

function getList(query, callback) {
	SolutionModel.find(query, {impact: 0, description: 0}, function(error, solutionRecords) {		
		if (error) {
			callback({
			    status: DB_CODES.FAIL,
			    error: error
			});
			return;
		} else {
			solutionRecords = solutionRecords.map(function(solutionRecord) {
				return new SolutionController.SolutionAPI(solutionRecord);
			});
			callback({
				status: REQUEST_CODES.SUCCESS,
				result: solutionRecords
			});
			return;	
		}
	}).sort({createdDate: -1});
}

function update(solution, callback) {
	getDetails(solution.solutionId, function(response) {
		if (response.error) {
			callback(response);
			return;
		} else {
			if (response.result) {
				var solutionDetails = response.result[0];
				solution.updatedDate = new Date().getTime();
				SolutionModel.updateOne({"solutionId": solution.solutionId}, {$set: solution}, function(error, effectedRows) {
					if (error) {
						callback({
							status: DB_CODES.FAIL,
							error: error
						});
						return;
					} else {
						if (!effectedRows.nModified) {
							callback({
								status: REQUEST_CODES.FAIL,
								error: utils.formatText(SOLUTION_CODES.UPDATE_FAIL, solution.solutionId)
							});
							return;
						} else {
							let logObj = {
								"userId": solution.userId,
								"userName": solution.userName,
								"email": solution.userEmail,
								"requestUrl": solution.url,
								"requestMethod": solution.method,
								"apiRequest": {
									"module": "Solutions-Update",
									"name": solutionDetails.name,
									"id": solutionDetails.solutionId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: utils.formatText(SOLUTION_CODES.UPDATE_SUCCESS, solution.solutionId)
									});
									return;
								}
							});
						}
					}
				});
			} else {
				callback({
					status: REQUEST_CODES.FAIL,
					result: "No Solution Found"
				});
				return;
			}
		}
	});	 
}

function uploadResource(file, callback) {
	File.create({fileName: file.advanceDetails.fileName, description: file.advanceDetails.description, uploadBy: file.advanceDetails.uploadBy, originalName: file.file.originalname, filePath: '/uploads/resources/'+file.file.filename, userId: file.advanceDetails.userId, category: "article", mimeType: file.file.mimetype}, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var fileResponse = response.file;
			getDetails(file.advanceDetails.solutionId, function(response) {
				if(response.error) {
					callback(response);
				} else {
					var solutionRecord = response.result[0];
					var article = solutionRecord.article;
					article.push(fileResponse.fileId);
					var solution = {
						"solutionId": file.advanceDetails.solutionId,
						"article": article,
						"userId": file.userId,
						"userName": file.userName,
						"userEmail": file.userEmail,
						"url": file.url,
						"method": file.method,
						"apiRequest": {
							"module": "Solutions-Update",
							"name": solutionRecord.title,
							"id": file.advanceDetails.solutionId
						},
						"apiResponse": "SUCCESS"
					}
					update(solution, function(response){
						if(response.error){
							callback(response);
						} else {
							let logObj = {
								"userId": file.userId,
								"userName": file.userName,
								"email": file.userEmail,
								"requestUrl": file.url,
								"requestMethod": file.method,
								"apiRequest": {
									"module": "Solutions-UploadResource",
									"name": solutionRecord.name,
									"id": solutionRecord.solutionId
								},
								"apiResponse": "SUCCESS"
							}
							UserLog.create(logObj, function(response){
								if(!response.error){
									callback({
										status: REQUEST_CODES.SUCCESS,
										result: "Resource uploaded"
									});
									return;
								}
							});
						}
					});
				}
			});
		}
	});
}

function getArticle(solutionId, callback) {
	getDetails(solutionId, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var solutionRecord = response.result[0];
			var fileIds = solutionRecord.article;
			File.getList({fileId: {$in: fileIds}}, function(response) {
				if(response.error) {
					callback(response);
				} else {
					var fileRecords = response.result;
					callback({
						status: REQUEST_CODES.SUCCESS,
						result: fileRecords
					});
				}
			})
		}
	});
}

function addArticle(article, callback) {
	getDetails(article.solutionId, function(response) {
		if(response.error) {
			callback(response);
		} else {
			var solutionRecord = response.result[0];
			var articleRecord  = {
				"solutionId": article.solutionId,
				"addArticle": solutionRecord.addArticle,
				"userId": article.userId,
				"userName": article.userName,
				"userEmail": article.userEmail,
				"url": article.url,
				"method": article.method,
				"apiRequest": {
					"module": "Solutions-Update",
					"name": solutionRecord.title,
					"id": article.solutionId
				},
				"apiResponse": "SUCCESS"
			};
			articleRecord.addArticle.push(article.addArticle);
			update(articleRecord, function(response) {
				let logObj = {
					"userId": article.userId,
					"userName": article.userName,
					"email": article.userEmail,
					"requestUrl": article.url,
					"requestMethod": article.method,
					"apiRequest": {
						"module": "Solutions-AddArticle",
						"name": solutionRecord.name,
						"id": article.solutionId
					},
					"apiResponse": "SUCCESS"
				}
				UserLog.create(logObj, function(response){
					if(!response.error){
						callback(response);
					}
				});
			});
		}
	});
}

module.exports.create = create;
module.exports.getDetails = getDetails;
module.exports.getList = getList;
module.exports.update = update;
